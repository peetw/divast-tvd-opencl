// Class header
#include "Tests.h"

// System headers
#include <cmath>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

// Third-party headers
#include <CL/cl.hpp>

// Project headers
#include "utils.h"


// ---------------------------------------------------------------------------
// CONSTRUCTORS
// ---------------------------------------------------------------------------
Tests::Tests(const cl_device_type deviceType,
           const cl_uint numCores,
           cl_uint platformIdx,
           const unsigned int _numRows,
           const unsigned int _numCols)
{
    // Initialize OpenCL device and context
    initOpenCl(deviceType, numCores, platformIdx);

    // Array dimensions
    numRows = _numRows;
    numCols = _numCols;
    numElements = numRows * numCols;

    // Fortran array dimensions
    IMAX = numCols;
    JMAX = numRows;

    // Domain-sized kernel launch parameters
    globalWorkSz = cl::NDRange(numCols, numRows);
    localWorkSz = cl::NullRange;

    // Memory sizes for domain sized arrays
    domainClUIntSz = numElements * sizeof(cl_uint);
    domainClFloatSz = numElements * sizeof(cl_float);

    // Initialize constants
    initConstants();

    // Initialize arrays with random values
    initArrays();

    // Initialize Fortran arrays
    initArraysFortran();

    // Initialize OpenCL arrays
    initArraysOpenCl();

    // Build kernels and set args
    initKernels();
}


// ---------------------------------------------------------------------------
// MUTATORS
// ---------------------------------------------------------------------------
void Tests::initOpenCl(const cl_device_type deviceType,
                      const cl_uint numCores,
                      cl_uint platformIdx)
{
    try {
        // Select specified platform
        std::vector<cl::Platform> platforms;
        cl::Platform::get(&platforms);
        if (platformIdx >= platforms.size()) {
            utils::warning("Value for platform-idx too large, using 0");
            platformIdx = 0;
        }
        cl::Platform platform = platforms[platformIdx];

        // Find specified device type
        std::vector<cl::Device> devices;
        platform.getDevices(deviceType, &devices);
        device = devices[0];

        // Restrict to specified number of CPU cores
#ifdef CL_VERSION_1_2
        const unsigned int total_cores =
                device.getInfo<CL_DEVICE_MAX_COMPUTE_UNITS>();
        if (deviceType == CL_DEVICE_TYPE_CPU && numCores > 0
                && numCores < total_cores) {
            const std::string ext = device.getInfo<CL_DEVICE_EXTENSIONS>();
            if (ext.find("cl_ext_device_fission") != std::string::npos) {
                cl_device_partition_property partition_props[] =
                    {CL_DEVICE_PARTITION_EQUALLY, numCores, 0};
                device.createSubDevices(partition_props, &devices);
                device = devices[0];
            } else {
                utils::warning("Device does not support device_fission");
            }
        }
#endif

        // Create context and command queue
        cl_context_properties context_props[] = {CL_CONTEXT_PLATFORM,
            (cl_context_properties)(platform)(), 0};
        context = cl::Context(devices, context_props);
        queue = cl::CommandQueue(context, device);
    } catch(cl::Error &e) {
        utils::clError(e, __FUNCTION__);
    }
}

void Tests::initConstants()
{
    constants.roughnessScheme = CwTrans;
    constants.eddyScheme = MixingLength;
    constants.dx = 2.5;
    constants.dpMin = 0.2;
    constants.eddyCoef = 1.2 * sqrt(9.81);

    const float beta = 1.016;
    const float latitude_ang = 25.3;
    const float cori = M_PI * sin(latitude_ang * (M_PI/180.0)) / 21600.0;

    vars.dt = 0.1;
    vars.dtDx = vars.dt / constants.dx;
    vars.dtDxSq = vars.dtDx / constants.dx;
    vars.hDtDxG = 0.5 * vars.dtDx * 9.81;
    vars.dtDxBeta = vars.dtDx * beta;
    vars.dtG = vars.dt * 9.81;
    vars.dtCori = vars.dt * cori;

    try {
        constants_d = cl::Buffer(context, CL_MEM_READ_ONLY, sizeof(Constants));
        vars_d = cl::Buffer(context, CL_MEM_READ_ONLY, sizeof(Vars));

        queue.enqueueWriteBuffer(constants_d, CL_FALSE, 0, sizeof(Constants), &constants);
        queue.enqueueWriteBuffer(vars_d, CL_FALSE, 0, sizeof(Vars), &vars);
    } catch(cl::Error &e) {
        utils::clError(e, __FUNCTION__);
    }
}

void Tests::initArrays()
{

    // Initialize constant hydro arrays
    ZERO = std::vector<cl_float>(numElements, 0);
    H = ZERO;
    VARCHZ = std::vector<cl_float>(numElements, 0.01);
    IWET = std::vector<cl_uint>(numElements, 1);
    IALL = std::vector<cl_uint>(numElements, true);

    // Initialize variable hydro arrays
    iact = std::vector<cl_uint>(numElements, false);
    etu = ZERO;
    qxu = ZERO;
    qyu = ZERO;
    etl = ZERO;
    qxl = ZERO;
    qyl = ZERO;
    etm = ZERO;
    qxm = ZERO;
    qym = ZERO;
    taux = ZERO;
    tauy = ZERO;
    etmax = ZERO;
    uvmax = ZERO;
    taumax = ZERO;

    // Fill arrays with random numbers
    srand(time(NULL));
    for (unsigned int i = 0; i < numRows; i++) {
        for (unsigned int j = 0; j < numCols; j++) {
            const int idx = j + i * numCols;
            if (i == 0 || i == numRows-1 || j == 0 || j == numCols-1) {
                IWET[idx] = 0;
            } else {
                const unsigned int min = 0;
                const unsigned int max = 3;
                IWET[idx] = min + rand() % (max - min + 1);
            }

            H[idx] = rval(-5, 5);
            etu[idx] = rval(-5, 10);

            if (H[idx] + etu[idx] > constants.dpMin) {
                iact[idx] = true;
                qxu[idx] = rval(-2, 2);
                qyu[idx] = rval(-2, 2);
            }
        }
    }
}

void Tests::initArraysFortran()
{
    iact_host = iact;
    etu_host = etu;
    qxu_host = qxu;
    qyu_host = qyu;
    etl_host = etl;
    qxl_host = qxl;
    qyl_host = qyl;
    etm_host = etm;
    qxm_host = qxm;
    qym_host = qym;
    taux_host = taux;
    tauy_host = tauy;
    etmax_host = etmax;
    uvmax_host = uvmax;
    taumax_host = taumax;

    dp_host = ZERO;
    vegporos_host = ZERO;

    // WB Alnus EI25 and EI50 vegetation data
    vegnpoints_host[0] = 11;   vegnpoints_host[1] = 11;
    vegden_host[0] = 0.1667;   vegden_host[1] = 0.1667;
    vegheight_host[0] = 2.57;  vegheight_host[1] = 2.57;
    vegcd_host[0] = 0.835;     vegcd_host[1] = 0.888;
    vegei_host[0] = 140;       vegei_host[1] = 117;
    vegvogel_host[0] = -0.734; vegvogel_host[1] = -0.737;

    std::vector<float> tmpheights = {0, 0.26, 0.51, 0.77, 1.03, 1.29, 1.54,
            1.8, 2.06, 2.31, 2.57,
            0, 0.26, 0.51, 0.77, 1.03, 1.29, 1.54, 1.8, 2.06, 2.31, 2.57};
    vegheights_host = tmpheights;
    std::vector<float> tmpwidths = {0.34, 1.02, 1.72, 1.88, 1.91, 1.91,
            1.91, 1.91, 1.91, 1.91, 1.91,
            0.34, 1.02, 1.72, 1.88, 1.91, 1.91, 1.91, 1.91, 1.91, 1.91, 1.91};
    vegwidths_host = tmpwidths;
    std::vector<float> tmpareas = {0, 0.04, 0.1, 0.18, 0.26, 0.34, 0.39,
            0.42, 0.44, 0.45, 0.46,
            0, 0.04, 0.1, 0.18, 0.26, 0.34, 0.39, 0.42, 0.44, 0.45, 0.46};
    vegareas_host = tmpareas;
}

void Tests::initArraysOpenCl()
{
    iact_dev = iact;
    etu_dev = etu;
    qxu_dev = qxu;
    qyu_dev = qyu;
    etl_dev = etl;
    qxl_dev = qxl;
    qyl_dev = qyl;
    etm_dev = etm;
    qxm_dev = qxm;
    qym_dev = qym;
    taux_dev = taux;
    tauy_dev = tauy;
    etmax_dev = etmax;
    uvmax_dev = uvmax;
    taumax_dev = taumax;

    // WB Alnus EI25 and EI50 vegetation data
    std::vector<cl_float> vegData = {0.1667, 140, 0.835, -0.734*0.5, 0, 11,
            0.1667, 117, 0.888, -0.737*0.5, 11, 11};
    std::vector<cl_float> vegHeights = {0, 0.26, 0.51, 0.77, 1.03, 1.29, 1.54,
            1.8, 2.06, 2.31, 2.57,
            0, 0.26, 0.51, 0.77, 1.03, 1.29, 1.54, 1.8, 2.06, 2.31, 2.57};
    std::vector<cl_float> vegWidths = {0.34, 1.02, 1.72, 1.88, 1.91, 1.91,
            1.91, 1.91, 1.91, 1.91, 1.91,
            0.34, 1.02, 1.72, 1.88, 1.91, 1.91, 1.91, 1.91, 1.91, 1.91, 1.91};
    std::vector<cl_float> vegAreas = {0, 0.04, 0.1, 0.18, 0.26, 0.34, 0.39,
            0.42, 0.44, 0.45, 0.46,
            0, 0.04, 0.1, 0.18, 0.26, 0.34, 0.39, 0.42, 0.44, 0.45, 0.46};

    const size_t vegDataSz = sizeof(cl_float)*vegData.size();
    const size_t vegHeightsSz = sizeof(cl_float)*vegHeights.size();
    const size_t vegWidthsSz = sizeof(cl_float)*vegWidths.size();
    const size_t vegAreasSz = sizeof(cl_float)*vegAreas.size();

    try {
        // Initialize device buffers and transfer data to device
        H_d = cl::Buffer(context, CL_MEM_READ_ONLY, domainClFloatSz);
        VARCHZ_d = cl::Buffer(context, CL_MEM_READ_ONLY, domainClFloatSz);
        IWET_d = cl::Buffer(context, CL_MEM_READ_ONLY, domainClUIntSz);
        IALL_d = cl::Buffer(context, CL_MEM_READ_ONLY, domainClUIntSz);
        vegData_d = cl::Buffer(context, CL_MEM_READ_ONLY, vegDataSz);
        vegHeights_d = cl::Buffer(context, CL_MEM_READ_ONLY, vegHeightsSz);
        vegWidths_d = cl::Buffer(context, CL_MEM_READ_ONLY, vegWidthsSz);
        vegAreas_d = cl::Buffer(context, CL_MEM_READ_ONLY, vegAreasSz);
        iact_d = cl::Buffer(context, CL_MEM_READ_WRITE, domainClUIntSz);
        etu_d = cl::Buffer(context, CL_MEM_READ_WRITE, domainClFloatSz);
        qxu_d = cl::Buffer(context, CL_MEM_READ_WRITE, domainClFloatSz);
        qyu_d = cl::Buffer(context, CL_MEM_READ_WRITE, domainClFloatSz);
        etl_d = cl::Buffer(context, CL_MEM_READ_WRITE, domainClFloatSz);
        qxl_d = cl::Buffer(context, CL_MEM_READ_WRITE, domainClFloatSz);
        qyl_d = cl::Buffer(context, CL_MEM_READ_WRITE, domainClFloatSz);
        etm_d = cl::Buffer(context, CL_MEM_READ_WRITE, domainClFloatSz);
        qxm_d = cl::Buffer(context, CL_MEM_READ_WRITE, domainClFloatSz);
        qym_d = cl::Buffer(context, CL_MEM_READ_WRITE, domainClFloatSz);
        taux_d = cl::Buffer(context, CL_MEM_WRITE_ONLY, domainClFloatSz);
        tauy_d = cl::Buffer(context, CL_MEM_WRITE_ONLY, domainClFloatSz);
        etmax_d = cl::Buffer(context, CL_MEM_READ_WRITE, domainClFloatSz);
        uvmax_d = cl::Buffer(context, CL_MEM_READ_WRITE, domainClFloatSz);
        taumax_d = cl::Buffer(context, CL_MEM_READ_WRITE, domainClFloatSz);

        queue.enqueueWriteBuffer(H_d, CL_FALSE, 0, domainClFloatSz, &H[0]);
        queue.enqueueWriteBuffer(VARCHZ_d, CL_FALSE, 0, domainClFloatSz, &VARCHZ[0]);
        queue.enqueueWriteBuffer(IWET_d, CL_FALSE, 0, domainClUIntSz, &IWET[0]);
        queue.enqueueWriteBuffer(IALL_d, CL_FALSE, 0, domainClUIntSz, &IALL[0]);
        queue.enqueueWriteBuffer(vegData_d, CL_FALSE, 0, vegDataSz, &vegData[0]);
        queue.enqueueWriteBuffer(vegHeights_d, CL_FALSE, 0, vegHeightsSz, &vegHeights[0]);
        queue.enqueueWriteBuffer(vegWidths_d, CL_FALSE, 0, vegWidthsSz, &vegWidths[0]);
        queue.enqueueWriteBuffer(vegAreas_d, CL_FALSE, 0, vegAreasSz, &vegAreas[0]);
        queue.enqueueWriteBuffer(iact_d, CL_FALSE, 0, domainClUIntSz, &iact_dev[0]);
        queue.enqueueWriteBuffer(etu_d, CL_FALSE, 0, domainClFloatSz, &etu_dev[0]);
        queue.enqueueWriteBuffer(qxu_d, CL_FALSE, 0, domainClFloatSz, &qxu_dev[0]);
        queue.enqueueWriteBuffer(qyu_d, CL_FALSE, 0, domainClFloatSz, &qyu_dev[0]);
        queue.enqueueWriteBuffer(etl_d, CL_FALSE, 0, domainClFloatSz, &etl_dev[0]);
        queue.enqueueWriteBuffer(qxl_d, CL_FALSE, 0, domainClFloatSz, &qxl_dev[0]);
        queue.enqueueWriteBuffer(qyl_d, CL_FALSE, 0, domainClFloatSz, &qyl_dev[0]);
        queue.enqueueWriteBuffer(etm_d, CL_FALSE, 0, domainClFloatSz, &etm_dev[0]);
        queue.enqueueWriteBuffer(qxm_d, CL_FALSE, 0, domainClFloatSz, &qxm_dev[0]);
        queue.enqueueWriteBuffer(qym_d, CL_FALSE, 0, domainClFloatSz, &qym_dev[0]);
        queue.enqueueWriteBuffer(taux_d, CL_FALSE, 0, domainClFloatSz, &taux_dev[0]);
        queue.enqueueWriteBuffer(tauy_d, CL_FALSE, 0, domainClFloatSz, &tauy_dev[0]);
        queue.enqueueWriteBuffer(etmax_d, CL_FALSE, 0, domainClFloatSz, &etmax_dev[0]);
        queue.enqueueWriteBuffer(uvmax_d, CL_FALSE, 0, domainClFloatSz, &uvmax_dev[0]);
        queue.enqueueWriteBuffer(taumax_d, CL_TRUE, 0, domainClFloatSz, &taumax_dev[0]);
    } catch(cl::Error &e) {
        utils::clError(e, __FUNCTION__);
    }
}

void Tests::initKernels()
{
    try {
        // mac_xy.cl
        std::string file = "mac_xy.cl";
        macPredDrying = buildKernel(file, "macPredDrying");
        macPredDrying.setArg(0, constants_d);
        macPredDrying.setArg(1, IALL_d);
        macPredDrying.setArg(2, H_d);
        macPredDrying.setArg(3, etu_d);
        macPredDrying.setArg(4, qxu_d);
        macPredDrying.setArg(5, qyu_d);
        macPredDrying.setArg(6, iact_d);
        macPredDrying.setArg(7, etl_d);
        macPredDrying.setArg(8, qxl_d);
        macPredDrying.setArg(9, qyl_d);

        macCorrDrying = buildKernel(file, "macCorrDrying");
        macCorrDrying.setArg(0, constants_d);
        macCorrDrying.setArg(1, IWET_d);
        macCorrDrying.setArg(2, IALL_d);
        macCorrDrying.setArg(3, H_d);
        macCorrDrying.setArg(4, etl_d);
        macCorrDrying.setArg(5, qxl_d);
        macCorrDrying.setArg(6, qyl_d);
        macCorrDrying.setArg(7, iact_d);
        macCorrDrying.setArg(8, etm_d);
        macCorrDrying.setArg(9, qxm_d);
        macCorrDrying.setArg(10, qym_d);

        macWetDryInterface = buildKernel(file, "macWetDryInterface");
        macWetDryInterface.setArg(0, IWET_d);
        macWetDryInterface.setArg(1, iact_d);
        // setArg(2, offset) and setArg(3, q[xy][lm]) set before kernel launch

        // mac_x.cl
        file = "mac_x.cl";
        macPredX = buildKernel(file, "macPredX");
        macPredX.setArg(0, constants_d);
        macPredX.setArg(1, vars_d);
        macPredX.setArg(2, vegData_d);
        macPredX.setArg(3, vegHeights_d);
        macPredX.setArg(4, vegAreas_d);
        macPredX.setArg(5, vegWidths_d);
        macPredX.setArg(6, IWET_d);
        macPredX.setArg(7, iact_d);
        macPredX.setArg(8, VARCHZ_d);
        macPredX.setArg(9, H_d);
        macPredX.setArg(10, etl_d);
        macPredX.setArg(11, qxl_d);
        macPredX.setArg(12, qyl_d);
        macPredX.setArg(13, etm_d);
        macPredX.setArg(14, qxm_d);
        macPredX.setArg(15, qym_d);

        macCorrX = buildKernel(file, "macCorrX");
        macCorrX.setArg(0, constants_d);
        macCorrX.setArg(1, vars_d);
        macCorrX.setArg(2, vegData_d);
        macCorrX.setArg(3, vegHeights_d);
        macCorrX.setArg(4, vegAreas_d);
        macCorrX.setArg(5, vegWidths_d);
        macCorrX.setArg(6, IWET_d);
        macCorrX.setArg(7, iact_d);
        macCorrX.setArg(8, VARCHZ_d);
        macCorrX.setArg(9, H_d);
        macCorrX.setArg(10, etl_d);
        macCorrX.setArg(11, qxl_d);
        macCorrX.setArg(12, qyl_d);
        macCorrX.setArg(13, etm_d);
        macCorrX.setArg(14, qxm_d);
        macCorrX.setArg(15, qym_d);
        macCorrX.setArg(16, etu_d);
        macCorrX.setArg(17, qxu_d);
        macCorrX.setArg(18, qyu_d);

        // mac_y.cl
        file = "mac_y.cl";
        macPredY = buildKernel(file, "macPredY");
        macPredY.setArg(0, constants_d);
        macPredY.setArg(1, vars_d);
        macPredY.setArg(2, vegData_d);
        macPredY.setArg(3, vegHeights_d);
        macPredY.setArg(4, vegAreas_d);
        macPredY.setArg(5, vegWidths_d);
        macPredY.setArg(6, IWET_d);
        macPredY.setArg(7, iact_d);
        macPredY.setArg(8, VARCHZ_d);
        macPredY.setArg(9, H_d);
        macPredY.setArg(10, etl_d);
        macPredY.setArg(11, qxl_d);
        macPredY.setArg(12, qyl_d);
        macPredY.setArg(13, etm_d);
        macPredY.setArg(14, qxm_d);
        macPredY.setArg(15, qym_d);

        macCorrY = buildKernel(file, "macCorrY");
        macCorrY.setArg(0, constants_d);
        macCorrY.setArg(1, vars_d);
        macCorrY.setArg(2, vegData_d);
        macCorrY.setArg(3, vegHeights_d);
        macCorrY.setArg(4, vegAreas_d);
        macCorrY.setArg(5, vegWidths_d);
        macCorrY.setArg(6, IWET_d);
        macCorrY.setArg(7, iact_d);
        macCorrY.setArg(8, VARCHZ_d);
        macCorrY.setArg(9, H_d);
        macCorrY.setArg(10, etl_d);
        macCorrY.setArg(11, qxl_d);
        macCorrY.setArg(12, qyl_d);
        macCorrY.setArg(13, etm_d);
        macCorrY.setArg(14, qxm_d);
        macCorrY.setArg(15, qym_d);
        macCorrY.setArg(16, etu_d);
        macCorrY.setArg(17, qxu_d);
        macCorrY.setArg(18, qyu_d);

        // tvd_x.cl
        file = "tvd_x.cl";
        tvdX = buildKernel(file, "tvdX");
        tvdX.setArg(0, vars_d);
        tvdX.setArg(1, iact_d);
        tvdX.setArg(2, H_d);
        tvdX.setArg(3, etl_d);
        tvdX.setArg(4, qxl_d);
        tvdX.setArg(5, qyl_d);
        tvdX.setArg(6, etm_d);
        tvdX.setArg(7, qxm_d);
        tvdX.setArg(8, qym_d);

        // tvd_y.cl
        file = "tvd_y.cl";
        tvdY = buildKernel(file, "tvdY");
        tvdY.setArg(0, vars_d);
        tvdY.setArg(1, iact_d);
        tvdY.setArg(2, H_d);
        tvdY.setArg(3, etl_d);
        tvdY.setArg(4, qxl_d);
        tvdY.setArg(5, qyl_d);
        tvdY.setArg(6, etm_d);
        tvdY.setArg(7, qxm_d);
        tvdY.setArg(8, qym_d);

        // tvd_xy.cl
        file = "tvd_xy.cl";
        tvdPost = buildKernel(file, "tvdPost");
        tvdPost.setArg(0, IWET_d);
        tvdPost.setArg(1, iact_d);
        tvdPost.setArg(2, etm_d);
        tvdPost.setArg(3, qxm_d);
        tvdPost.setArg(4, qym_d);
        tvdPost.setArg(5, etu_d);
        tvdPost.setArg(6, qxu_d);
        tvdPost.setArg(7, qyu_d);
        // setArg(8, offset) done immediately before kernel launch

        // post_hydro.cl
        file = "post_hydro.cl";
        activeCellsMaxVals = buildKernel(file, "activeCellsMaxVals");
        activeCellsMaxVals.setArg(0, constants_d);
        activeCellsMaxVals.setArg(1, H_d);
        activeCellsMaxVals.setArg(2, etu_d);
        activeCellsMaxVals.setArg(3, qxu_d);
        activeCellsMaxVals.setArg(4, qyu_d);
        activeCellsMaxVals.setArg(5, iact_d);
        activeCellsMaxVals.setArg(6, etmax_d);
        activeCellsMaxVals.setArg(7, uvmax_d);

        bedShearStress = buildKernel(file, "bedShearStress");
        bedShearStress.setArg(0, constants_d);
        bedShearStress.setArg(1, IWET_d);
        bedShearStress.setArg(2, iact_d);
        bedShearStress.setArg(3, H_d);
        bedShearStress.setArg(4, etu_d);
        bedShearStress.setArg(5, qxu_d);
        bedShearStress.setArg(6, qyu_d);
        bedShearStress.setArg(7, VARCHZ_d);
        bedShearStress.setArg(8, vegData_d);
        bedShearStress.setArg(9, vegHeights_d);
        bedShearStress.setArg(10, vegWidths_d);
        bedShearStress.setArg(11, taux_d);
        bedShearStress.setArg(12, tauy_d);
        bedShearStress.setArg(13, taumax_d);
    } catch(cl::Error &e) {
        utils::clError(e, __FUNCTION__);
    }
}

void Tests::run()
{
    std::cout << "\n  Function                Variable   Errors"
            "     Min err   Avrg err    Max err\n";
    std::cout << " ----------------------------------------------------------"
            "-------------------\n";

    // Pre hydro
    wettingTest();

    // Hydro X
    macPredDryingTest();
    macPredInterfaceXTest();
    macPredXTest();
    macCorrDryingTest();
    macCorrInterfaceXTest();
    macCorrXTest();
    tvdXTest();
    tvdPostXTest();

    // Hydro Y
    macPredDryingTest();
    macPredInterfaceYTest();
    macPredYTest();
    macCorrDryingTest();
    macCorrInterfaceYTest();
    macCorrYTest();
    tvdYTest();
    tvdPostYTest();

    // Post hydro
    activeCellsMaxValsTest();
    bedShearStressTest();

    std::cout << " ----------------------------------------------------------"
            "-------------------\n";
    std::cout << "\n Tests complete\n\n";
}

void Tests::wettingTest()
{
    // -----------------------------------------------------------------------
    // HOST
    // -----------------------------------------------------------------------
    wetting_(&IMAX, &JMAX, &constants.dpMin, &IWET[0], &iact[0], &etu_host[0]);

    // -----------------------------------------------------------------------
    // DEVICE
    // -----------------------------------------------------------------------
    try {
        queue.enqueueReadBuffer(iact_d, CL_FALSE, 0, domainClUIntSz, &iact_dev[0]);
        queue.enqueueReadBuffer(etu_d, CL_TRUE, 0, domainClFloatSz, &etu_dev[0]);
    } catch(cl::Error &e) {
        utils::clError(e, __FUNCTION__);
    }

    for (int i = 1; i < JMAX-1; i++) {
        for (int j = 1; j < IMAX-1; j++) {
            const int idx = j + i * IMAX;
            if (IWET[idx] && !iact[idx]) {
                cl_float *et_tmp = &etu_dev[idx];

                uint idx_tmp = idx+1;
                if (iact[idx_tmp] && etu_dev[idx_tmp] > *et_tmp) {
                    et_tmp = &etu_dev[idx_tmp];
                }

                idx_tmp = idx-1;
                if (iact[idx_tmp] && etu_dev[idx_tmp] > *et_tmp) {
                    et_tmp = &etu_dev[idx_tmp];
                }

                idx_tmp = idx+IMAX;
                if (iact[idx_tmp] && etu_dev[idx_tmp] > *et_tmp) {
                    et_tmp = &etu_dev[idx_tmp];
                }

                idx_tmp = idx-IMAX;
                if (iact[idx_tmp] && etu_dev[idx_tmp] > *et_tmp) {
                    et_tmp = &etu_dev[idx_tmp];
                }

                // Increase water level in current cell and decrease in neighbour
                if ((*et_tmp - etu_dev[idx]) > (constants.dpMin * 2.f)) {
                    etu_dev[idx] = etu_dev[idx] + constants.dpMin;
                    *et_tmp = *et_tmp - constants.dpMin;
                }
            }
        }
    }

    try {
        queue.enqueueWriteBuffer(etu_d, CL_FALSE, 0, domainClFloatSz, &etu_dev[0]);
    } catch(cl::Error &e) {
        utils::clError(e, __FUNCTION__);
    }

    // -----------------------------------------------------------------------
    // CHECK
    // -----------------------------------------------------------------------
    check(__FUNCTION__, "etu", etu_host, etu_dev);
}

void Tests::macPredDryingTest()
{
    // -----------------------------------------------------------------------
    // HOST
    // -----------------------------------------------------------------------
    mac_pred_drying_(&IMAX, &JMAX, &constants.dpMin, &IALL[0], &H[0],
            &etu_host[0], &qxu_host[0], &qyu_host[0], &iact_host[0],
            &dp_host[0], &etl_host[0], &qxl_host[0], &qyl_host[0]);

    // -----------------------------------------------------------------------
    // DEVICE
    // -----------------------------------------------------------------------
    try {
        queue.enqueueNDRangeKernel(macPredDrying, cl::NullRange, globalWorkSz,
                        localWorkSz);
        queue.enqueueReadBuffer(iact_d, CL_FALSE, 0, domainClUIntSz, &iact_dev[0]);
        queue.enqueueReadBuffer(etl_d, CL_FALSE, 0, domainClFloatSz, &etl_dev[0]);
        queue.enqueueReadBuffer(qxl_d, CL_FALSE, 0, domainClFloatSz, &qxl_dev[0]);
        queue.enqueueReadBuffer(qyl_d, CL_TRUE, 0, domainClFloatSz, &qyl_dev[0]);
    } catch(cl::Error &e) {
        utils::clError(e, __FUNCTION__);
    }

    // -----------------------------------------------------------------------
    // CHECK
    // -----------------------------------------------------------------------
    check(__FUNCTION__, "iact", iact_host, iact_dev);
    check(__FUNCTION__, "etl", etl_host, etl_dev);
    check(__FUNCTION__, "qxl", qxl_host, qxl_dev);
    check(__FUNCTION__, "qyl", qyl_host, qyl_dev);
}

void Tests::macPredInterfaceXTest()
{
    // -----------------------------------------------------------------------
    // HOST
    // -----------------------------------------------------------------------
    mac_wet_dry_interface_x_(&IMAX, &JMAX, &IWET[0], &iact_host[0],
            &qxl_host[0]);

    // -----------------------------------------------------------------------
    // DEVICE
    // -----------------------------------------------------------------------
    try {
        macWetDryInterface.setArg(2, 1);
        macWetDryInterface.setArg(3, qxl_d);
        queue.enqueueNDRangeKernel(macWetDryInterface, cl::NullRange,
                globalWorkSz, localWorkSz);
        queue.enqueueReadBuffer(qxl_d, CL_TRUE, 0, domainClFloatSz, &qxl_dev[0]);
    } catch(cl::Error &e) {
        utils::clError(e, __FUNCTION__);
    }

    // -----------------------------------------------------------------------
    // CHECK
    // -----------------------------------------------------------------------
    check(__FUNCTION__, "qxl", qxl_host, qxl_dev);
}

void Tests::macPredXTest()
{
    // -----------------------------------------------------------------------
    // HOST
    // -----------------------------------------------------------------------
    mac_pred_x_(&IMAX, &JMAX, (int*)&constants.roughnessScheme,
            (int*)&constants.eddyScheme, &vars.dt, &constants.dx, &vars.dtDx,
            &vars.dtDxSq, &vars.hDtDxG, &vars.dtDxBeta, &vars.dtG, &vars.dtCori,
            &constants.eddyCoef, &IWET[0], &iact_host[0], &IWET[0],
            &vegnpoints_host[0], &vegden_host[0], &vegheight_host[0],
            &vegcd_host[0], &vegei_host[0], &vegvogel_host[0],
            &vegheights_host[0], &vegareas_host[0], &vegwidths_host[0],
            &VARCHZ[0], &dp_host[0], &etl_host[0], &qxl_host[0], &qyl_host[0],
            &vegporos_host[0], &etm_host[0], &qxm_host[0], &qym_host[0]);

    // -----------------------------------------------------------------------
    // DEVICE
    // -----------------------------------------------------------------------
    try {
        queue.enqueueNDRangeKernel(macPredX, cl::NullRange, globalWorkSz,
                                localWorkSz);
        queue.enqueueReadBuffer(etm_d, CL_FALSE, 0, domainClFloatSz, &etm_dev[0]);
        queue.enqueueReadBuffer(qxm_d, CL_FALSE, 0, domainClFloatSz, &qxm_dev[0]);
        queue.enqueueReadBuffer(qym_d, CL_TRUE, 0, domainClFloatSz, &qym_dev[0]);
    } catch(cl::Error &e) {
        utils::clError(e, __FUNCTION__);
    }

    // -----------------------------------------------------------------------
    // CHECK
    // -----------------------------------------------------------------------
    check(__FUNCTION__, "etm", etm_host, etm_dev);
    check(__FUNCTION__, "qxm", qxm_host, qxm_dev);
    check(__FUNCTION__, "qym", qym_host, qym_dev);
}

void Tests::macCorrDryingTest()
{
    // -----------------------------------------------------------------------
    // HOST
    // -----------------------------------------------------------------------
    mac_corr_drying_(&IMAX, &JMAX, &constants.dpMin, &IWET[0], &IALL[0], &H[0],
            &etl_host[0], &qxl_host[0], &qyl_host[0], &iact_host[0],
            &dp_host[0], &etm_host[0], &qxm_host[0], &qym_host[0]);

    // -----------------------------------------------------------------------
    // DEVICE
    // -----------------------------------------------------------------------
    try {
        queue.enqueueNDRangeKernel(macCorrDrying, cl::NullRange, globalWorkSz,
                        localWorkSz);
        queue.enqueueReadBuffer(iact_d, CL_FALSE, 0, domainClUIntSz, &iact_dev[0]);
        queue.enqueueReadBuffer(etm_d, CL_FALSE, 0, domainClFloatSz, &etm_dev[0]);
        queue.enqueueReadBuffer(qxm_d, CL_FALSE, 0, domainClFloatSz, &qxm_dev[0]);
        queue.enqueueReadBuffer(qym_d, CL_TRUE, 0, domainClFloatSz, &qym_dev[0]);
    } catch(cl::Error &e) {
        utils::clError(e, __FUNCTION__);
    }

    // -----------------------------------------------------------------------
    // CHECK
    // -----------------------------------------------------------------------
    check(__FUNCTION__, "iact", iact_host, iact_dev);
    check(__FUNCTION__, "etm", etm_host, etm_dev);
    check(__FUNCTION__, "qxm", qxm_host, qxm_dev);
    check(__FUNCTION__, "qym", qym_host, qym_dev);
}

void Tests::macCorrInterfaceXTest()
{
    // -----------------------------------------------------------------------
    // HOST
    // -----------------------------------------------------------------------
    mac_wet_dry_interface_x_(&IMAX, &JMAX, &IWET[0], &iact_host[0],
            &qxm_host[0]);

    // -----------------------------------------------------------------------
    // DEVICE
    // -----------------------------------------------------------------------
    try {
        macWetDryInterface.setArg(2, 1);
        macWetDryInterface.setArg(3, qxm_d);
        queue.enqueueNDRangeKernel(macWetDryInterface, cl::NullRange,
                globalWorkSz, localWorkSz);
        queue.enqueueReadBuffer(qxm_d, CL_TRUE, 0, domainClFloatSz, &qxm_dev[0]);
    } catch(cl::Error &e) {
        utils::clError(e, __FUNCTION__);
    }

    // -----------------------------------------------------------------------
    // CHECK
    // -----------------------------------------------------------------------
    check(__FUNCTION__, "qxm", qxm_host, qxm_dev);
}

void Tests::macCorrXTest()
{
    // -----------------------------------------------------------------------
    // HOST
    // -----------------------------------------------------------------------
    mac_corr_x_(&IMAX, &JMAX, (int*)&constants.roughnessScheme,
            (int*)&constants.eddyScheme, &vars.dt, &constants.dx, &vars.dtDx,
            &vars.dtDxSq, &vars.hDtDxG, &vars.dtDxBeta, &vars.dtG, &vars.dtCori,
            &constants.eddyCoef, &IWET[0], &iact_host[0], &IWET[0],
            &vegnpoints_host[0], &vegden_host[0], &vegheight_host[0],
            &vegcd_host[0], &vegei_host[0], &vegvogel_host[0],
            &vegheights_host[0], &vegareas_host[0], &vegwidths_host[0],
            &VARCHZ[0], &dp_host[0], &etl_host[0], &qxl_host[0], &qyl_host[0],
            &etm_host[0], &qxm_host[0], &qym_host[0], &vegporos_host[0],
            &etu_host[0], &qxu_host[0], &qyu_host[0]);

    // -----------------------------------------------------------------------
    // DEVICE
    // -----------------------------------------------------------------------
    try {
        queue.enqueueNDRangeKernel(macCorrX, cl::NullRange, globalWorkSz,
                                localWorkSz);
        queue.enqueueReadBuffer(etu_d, CL_FALSE, 0, domainClFloatSz, &etu_dev[0]);
        queue.enqueueReadBuffer(qxu_d, CL_FALSE, 0, domainClFloatSz, &qxu_dev[0]);
        queue.enqueueReadBuffer(qyu_d, CL_TRUE, 0, domainClFloatSz, &qyu_dev[0]);
    } catch(cl::Error &e) {
        utils::clError(e, __FUNCTION__);
    }

    // -----------------------------------------------------------------------
    // CHECK
    // -----------------------------------------------------------------------
    check(__FUNCTION__, "etu", etu_host, etu_dev);
    check(__FUNCTION__, "qxu", qxu_host, qxu_dev);
    check(__FUNCTION__, "qyu", qyu_host, qyu_dev);
}

void Tests::tvdXTest()
{
    // -----------------------------------------------------------------------
    // HOST
    // -----------------------------------------------------------------------
    tvd_x_(&IMAX, &JMAX, &vars.dtDx, &iact_host[0], &H[0], &etl_host[0],
            &qxl_host[0], &qyl_host[0]);

    // -----------------------------------------------------------------------
    // DEVICE
    // -----------------------------------------------------------------------
    try {
        queue.enqueueNDRangeKernel(tvdX, cl::NullRange, globalWorkSz,
                                    localWorkSz);
        queue.enqueueReadBuffer(etm_d, CL_FALSE, 0, domainClFloatSz, &etl_dev[0]);
        queue.enqueueReadBuffer(qxm_d, CL_FALSE, 0, domainClFloatSz, &qxl_dev[0]);
        queue.enqueueReadBuffer(qym_d, CL_TRUE, 0, domainClFloatSz, &qyl_dev[0]);
    } catch(cl::Error &e) {
        utils::clError(e, __FUNCTION__);
    }

    // -----------------------------------------------------------------------
    // CHECK
    // -----------------------------------------------------------------------
    check(__FUNCTION__, "etl", etl_host, etl_dev);
    check(__FUNCTION__, "qxl", qxl_host, qxl_dev);
    check(__FUNCTION__, "qyl", qyl_host, qyl_dev);
}

void Tests::tvdPostXTest()
{
    // -----------------------------------------------------------------------
    // HOST
    // -----------------------------------------------------------------------
    tvd_post_x_(&IMAX, &JMAX, &IWET[0], &iact_host[0], &etl_host[0],
            &qxl_host[0], &qyl_host[0], &etu_host[0], &qxu_host[0],
            &qyu_host[0]);

    // -----------------------------------------------------------------------
    // DEVICE
    // -----------------------------------------------------------------------
    try {
        tvdPost.setArg(8, 1);
        queue.enqueueNDRangeKernel(tvdPost, cl::NullRange, globalWorkSz,
                        localWorkSz);
        queue.enqueueReadBuffer(etu_d, CL_FALSE, 0, domainClFloatSz, &etu_dev[0]);
        queue.enqueueReadBuffer(qxu_d, CL_FALSE, 0, domainClFloatSz, &qxu_dev[0]);
        queue.enqueueReadBuffer(qyu_d, CL_TRUE, 0, domainClFloatSz, &qyu_dev[0]);
    } catch(cl::Error &e) {
        utils::clError(e, __FUNCTION__);
    }

    // -----------------------------------------------------------------------
    // CHECK
    // -----------------------------------------------------------------------
    check(__FUNCTION__, "etu", etu_host, etu_dev);
    check(__FUNCTION__, "qxu", qxu_host, qxu_dev);
    check(__FUNCTION__, "qyu", qyu_host, qyu_dev);
}

void Tests::macPredInterfaceYTest()
{
    // -----------------------------------------------------------------------
    // HOST
    // -----------------------------------------------------------------------
    mac_wet_dry_interface_y_(&IMAX, &JMAX, &IWET[0], &iact_host[0],
            &qyl_host[0]);

    // -----------------------------------------------------------------------
    // DEVICE
    // -----------------------------------------------------------------------
    try {
        macWetDryInterface.setArg(2, sizeof(cl_uint), const_cast<size_t*>(&globalWorkSz[0]));
        macWetDryInterface.setArg(3, qyl_d);
        queue.enqueueNDRangeKernel(macWetDryInterface, cl::NullRange,
                globalWorkSz, localWorkSz);
        queue.enqueueReadBuffer(qyl_d, CL_TRUE, 0, domainClFloatSz, &qyl_dev[0]);
    } catch(cl::Error &e) {
        utils::clError(e, __FUNCTION__);
    }

    // -----------------------------------------------------------------------
    // CHECK
    // -----------------------------------------------------------------------
    check(__FUNCTION__, "qyl", qyl_host, qyl_dev);
}

void Tests::macPredYTest()
{
    // -----------------------------------------------------------------------
    // HOST
    // -----------------------------------------------------------------------
    mac_pred_y_(&IMAX, &JMAX, (int*)&constants.roughnessScheme,
            (int*)&constants.eddyScheme, &vars.dt, &constants.dx, &vars.dtDx,
            &vars.dtDxSq, &vars.hDtDxG, &vars.dtDxBeta, &vars.dtG, &vars.dtCori,
            &constants.eddyCoef, &IWET[0], &iact_host[0], &IWET[0],
            &vegnpoints_host[0], &vegden_host[0], &vegheight_host[0],
            &vegcd_host[0], &vegei_host[0], &vegvogel_host[0],
            &vegheights_host[0], &vegareas_host[0], &vegwidths_host[0],
            &VARCHZ[0], &dp_host[0], &etl_host[0], &qxl_host[0], &qyl_host[0],
            &vegporos_host[0], &etm_host[0], &qxm_host[0], &qym_host[0]);

    // -----------------------------------------------------------------------
    // DEVICE
    // -----------------------------------------------------------------------
    try {
        queue.enqueueNDRangeKernel(macPredY, cl::NullRange, globalWorkSz,
                                localWorkSz);
        queue.enqueueReadBuffer(etm_d, CL_FALSE, 0, domainClFloatSz, &etm_dev[0]);
        queue.enqueueReadBuffer(qxm_d, CL_FALSE, 0, domainClFloatSz, &qxm_dev[0]);
        queue.enqueueReadBuffer(qym_d, CL_TRUE, 0, domainClFloatSz, &qym_dev[0]);
    } catch(cl::Error &e) {
        utils::clError(e, __FUNCTION__);
    }

    // -----------------------------------------------------------------------
    // CHECK
    // -----------------------------------------------------------------------
    check(__FUNCTION__, "etm", etm_host, etm_dev);
    check(__FUNCTION__, "qxm", qxm_host, qxm_dev);
    check(__FUNCTION__, "qym", qym_host, qym_dev);
}

void Tests::macCorrInterfaceYTest()
{
    // -----------------------------------------------------------------------
    // HOST
    // -----------------------------------------------------------------------
    mac_wet_dry_interface_y_(&IMAX, &JMAX, &IWET[0], &iact_host[0],
            &qym_host[0]);

    // -----------------------------------------------------------------------
    // DEVICE
    // -----------------------------------------------------------------------
    try {
        macWetDryInterface.setArg(2, sizeof(cl_uint), const_cast<size_t*>(&globalWorkSz[0]));
        macWetDryInterface.setArg(3, qym_d);
        queue.enqueueNDRangeKernel(macWetDryInterface, cl::NullRange,
                globalWorkSz, localWorkSz);
        queue.enqueueReadBuffer(qym_d, CL_TRUE, 0, domainClFloatSz, &qym_dev[0]);
    } catch(cl::Error &e) {
        utils::clError(e, __FUNCTION__);
    }

    // -----------------------------------------------------------------------
    // CHECK
    // -----------------------------------------------------------------------
    check(__FUNCTION__, "qym", qym_host, qym_dev);
}

void Tests::macCorrYTest()
{
    // -----------------------------------------------------------------------
    // HOST
    // -----------------------------------------------------------------------
    mac_corr_y_(&IMAX, &JMAX, (int*)&constants.roughnessScheme,
            (int*)&constants.eddyScheme, &vars.dt, &constants.dx, &vars.dtDx,
            &vars.dtDxSq, &vars.hDtDxG, &vars.dtDxBeta, &vars.dtG, &vars.dtCori,
            &constants.eddyCoef, &IWET[0], &iact_host[0], &IWET[0],
            &vegnpoints_host[0], &vegden_host[0], &vegheight_host[0],
            &vegcd_host[0], &vegei_host[0], &vegvogel_host[0],
            &vegheights_host[0], &vegareas_host[0], &vegwidths_host[0],
            &VARCHZ[0], &dp_host[0], &etl_host[0], &qxl_host[0], &qyl_host[0],
            &etm_host[0], &qxm_host[0], &qym_host[0], &vegporos_host[0],
            &etu_host[0], &qxu_host[0], &qyu_host[0]);

    // -----------------------------------------------------------------------
    // DEVICE
    // -----------------------------------------------------------------------
    try {
        queue.enqueueNDRangeKernel(macCorrY, cl::NullRange, globalWorkSz,
                                localWorkSz);
        queue.enqueueReadBuffer(etu_d, CL_FALSE, 0, domainClFloatSz, &etu_dev[0]);
        queue.enqueueReadBuffer(qxu_d, CL_FALSE, 0, domainClFloatSz, &qxu_dev[0]);
        queue.enqueueReadBuffer(qyu_d, CL_TRUE, 0, domainClFloatSz, &qyu_dev[0]);
    } catch(cl::Error &e) {
        utils::clError(e, __FUNCTION__);
    }

    // -----------------------------------------------------------------------
    // CHECK
    // -----------------------------------------------------------------------
    check(__FUNCTION__, "etu", etu_host, etu_dev);
    check(__FUNCTION__, "qxu", qxu_host, qxu_dev);
    check(__FUNCTION__, "qyu", qyu_host, qyu_dev);
}

void Tests::tvdYTest()
{
    // -----------------------------------------------------------------------
    // HOST
    // -----------------------------------------------------------------------
    tvd_y_(&IMAX, &JMAX, &vars.dtDx, &iact_host[0], &H[0], &etl_host[0],
            &qxl_host[0], &qyl_host[0]);

    // -----------------------------------------------------------------------
    // DEVICE
    // -----------------------------------------------------------------------
    try {
        queue.enqueueNDRangeKernel(tvdY, cl::NullRange, globalWorkSz,
                                    localWorkSz);
        queue.enqueueReadBuffer(etm_d, CL_FALSE, 0, domainClFloatSz, &etl_dev[0]);
        queue.enqueueReadBuffer(qxm_d, CL_FALSE, 0, domainClFloatSz, &qxl_dev[0]);
        queue.enqueueReadBuffer(qym_d, CL_TRUE, 0, domainClFloatSz, &qyl_dev[0]);
    } catch(cl::Error &e) {
        utils::clError(e, __FUNCTION__);
    }

    // -----------------------------------------------------------------------
    // CHECK
    // -----------------------------------------------------------------------
    check(__FUNCTION__, "etl", etl_host, etl_dev);
    check(__FUNCTION__, "qxl", qxl_host, qxl_dev);
    check(__FUNCTION__, "qyl", qyl_host, qyl_dev);
}

void Tests::tvdPostYTest()
{
    // -----------------------------------------------------------------------
    // HOST
    // -----------------------------------------------------------------------
    tvd_post_y_(&IMAX, &JMAX, &IWET[0], &iact_host[0], &etl_host[0],
            &qxl_host[0], &qyl_host[0], &etu_host[0], &qxu_host[0],
            &qyu_host[0]);

    // -----------------------------------------------------------------------
    // DEVICE
    // -----------------------------------------------------------------------
    try {
        tvdPost.setArg(8, sizeof(cl_uint), const_cast<size_t*>(&globalWorkSz[0]));
        queue.enqueueNDRangeKernel(tvdPost, cl::NullRange, globalWorkSz,
                        localWorkSz);
        queue.enqueueReadBuffer(etu_d, CL_FALSE, 0, domainClFloatSz, &etu_dev[0]);
        queue.enqueueReadBuffer(qxu_d, CL_FALSE, 0, domainClFloatSz, &qxu_dev[0]);
        queue.enqueueReadBuffer(qyu_d, CL_TRUE, 0, domainClFloatSz, &qyu_dev[0]);
    } catch(cl::Error &e) {
        utils::clError(e, __FUNCTION__);
    }

    // -----------------------------------------------------------------------
    // CHECK
    // -----------------------------------------------------------------------
    check(__FUNCTION__, "etu", etu_host, etu_dev);
    check(__FUNCTION__, "qxu", qxu_host, qxu_dev);
    check(__FUNCTION__, "qyu", qyu_host, qyu_dev);
}

void Tests::activeCellsMaxValsTest()
{
    // -----------------------------------------------------------------------
    // HOST
    // -----------------------------------------------------------------------
    active_cells_max_vals_(&IMAX, &JMAX, &constants.dpMin, &H[0], &etu_host[0],
            &qxu_host[0], &qyu_host[0], &iact_host[0], &etmax_host[0],
            &uvmax_host[0]);

    // -----------------------------------------------------------------------
    // DEVICE
    // -----------------------------------------------------------------------
    try {
        queue.enqueueNDRangeKernel(activeCellsMaxVals, cl::NullRange,
                globalWorkSz, localWorkSz);
        queue.enqueueReadBuffer(iact_d, CL_FALSE, 0, domainClUIntSz, &iact_dev[0]);
        queue.enqueueReadBuffer(etmax_d, CL_FALSE, 0, domainClFloatSz, &etmax_dev[0]);
        queue.enqueueReadBuffer(uvmax_d, CL_TRUE, 0, domainClFloatSz, &uvmax_dev[0]);
    } catch(cl::Error &e) {
        utils::clError(e, __FUNCTION__);
    }

    // -----------------------------------------------------------------------
    // CHECK
    // -----------------------------------------------------------------------
    check(__FUNCTION__, "iact", iact_host, iact_dev);
    check(__FUNCTION__, "etmax", etmax_host, etmax_dev);
    check(__FUNCTION__, "uvmax", uvmax_host, uvmax_dev);
}

void Tests::bedShearStressTest()
{
    // -----------------------------------------------------------------------
    // HOST
    // -----------------------------------------------------------------------
    bed_shear_stress_(&IMAX, &JMAX, (int*)&constants.roughnessScheme, &IWET[0],
            &iact_host[0], &IWET[0], &vegnpoints_host[0], &vegden_host[0],
            &vegheight_host[0], &vegcd_host[0], &vegei_host[0],
            &vegvogel_host[0], &vegheights_host[0], &vegareas_host[0],
            &vegwidths_host[0], &H[0], &VARCHZ[0], &etu_host[0], &qxu_host[0],
            &qyu_host[0], &vegporos_host[0], &taux_host[0], &tauy_host[0],
            &taumax_host[0]);

    // -----------------------------------------------------------------------
    // DEVICE
    // -----------------------------------------------------------------------
    try {
        queue.enqueueNDRangeKernel(bedShearStress, cl::NullRange,
                globalWorkSz, localWorkSz);
        queue.enqueueReadBuffer(taux_d, CL_FALSE, 0, domainClFloatSz, &taux_dev[0]);
        queue.enqueueReadBuffer(tauy_d, CL_FALSE, 0, domainClFloatSz, &tauy_dev[0]);
        queue.enqueueReadBuffer(taumax_d, CL_TRUE, 0, domainClFloatSz, &taumax_dev[0]);
    } catch(cl::Error &e) {
        utils::clError(e, __FUNCTION__);
    }

    // -----------------------------------------------------------------------
    // CHECK
    // -----------------------------------------------------------------------
    check(__FUNCTION__, "taux", taux_host, taux_dev);
    check(__FUNCTION__, "tauy", tauy_host, tauy_dev);
    check(__FUNCTION__, "taumax", taumax_host, taumax_dev);
}


// ---------------------------------------------------------------------------
// ACCESSORS
// ---------------------------------------------------------------------------
cl::Kernel Tests::buildKernel(const std::string &filename,
                             const std::string &funcName)
{
    const std::string directory = "src/kernels/";
    cl::Kernel kernel;
    try {
        // Read source file
        std::ifstream sourceFile(directory + filename);
        if (!sourceFile.is_open()) {
            utils::error("Unable to open file: " + directory + filename);
        }
        std::string sourceCode(std::istreambuf_iterator<char>(sourceFile),
                (std::istreambuf_iterator<char>()));
        cl::Program::Sources source(1, std::make_pair(sourceCode.c_str(),
                sourceCode.length()+1));

        // Make program of the source code in the context
        cl::Program program = cl::Program(context, source);

        // Build program for the specified device
        std::vector<cl::Device> devices;
        devices.push_back(device);
        try {
            program.build(devices, "-I src -I src/kernels");
        } catch(cl::Error &e) {
            utils::error("Failed to build kernel: " + funcName +
                    "\nBuild log:\n" +
                    program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(device));
        }

        // Make kernel
        kernel = cl::Kernel(program, funcName.c_str());
    } catch(cl::Error &e) {
        utils::clError(e, __FUNCTION__);
    }
    return kernel;
}


// ---------------------------------------------------------------------------
// ANONYMOUS NAMESPACE
// ---------------------------------------------------------------------------
namespace {
    // Generate random number between lo and hi
    float rval(const float lo, const float hi)
    {
        return lo + (float)(rand()) / (float)(RAND_MAX / (hi - lo));
    }
}
