// ---------------------------------------------------------------------------
// Total Variation Diminishing (TVD) post step
// ---------------------------------------------------------------------------
__kernel void tvdPost(__global const uint *IWET, __global const uint *iact,
         __global const float *etm, __global const float *qxm,
         __global const float *qym, __global float *etu, __global float *qxu,
         __global float *qyu, const uint offset)
{
    // Element index
    const uint idx = get_global_id(0) + get_global_id(1)*get_global_size(0);

    if (IWET[idx] && iact[idx]) {
        const uint idx_next = idx + offset;
        etu[idx] = etu[idx] + etm[idx_next] - etm[idx];
        qxu[idx] = qxu[idx] + qxm[idx_next] - qxm[idx];
        qyu[idx] = qyu[idx] + qym[idx_next] - qym[idx];
    }
}
