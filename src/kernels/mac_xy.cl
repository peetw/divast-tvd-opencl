#include "types.h"

// ---------------------------------------------------------------------------
// MacCormack predictor stage drying check
// ---------------------------------------------------------------------------
__kernel void macPredDrying(__constant const Constants *constants,
        __global const uint *IALL, __global const float *H,
        __global const float *etu, __global const float *qxu,
        __global const float *qyu, __global uint *iact, __global float *etl,
        __global float *qxl, __global float *qyl)
{
    // Element index
    const uint idx = get_global_id(0) + get_global_id(1)*get_global_size(0);

    // Determine if cell is active and reset variables
    if (IALL[idx]) {
        const float ettmp = etu[idx];

        bool active = (H[idx] + ettmp > constants->dpMin);
        iact[idx] = active;

        etl[idx] = ettmp;
        qxl[idx] = (active ? qxu[idx] : 0.f);
        qyl[idx] = (active ? qyu[idx] : 0.f);
    }
}

// ---------------------------------------------------------------------------
// MacCormack corrector stage drying check
// ---------------------------------------------------------------------------
__kernel void macCorrDrying(__constant const Constants *constants,
        __global const uint *IWET, __global const uint *IALL,
        __global const float *H,__global const float *etl,
        __global const float *qxl, __global const float *qyl,
        __global uint *iact, __global float *etm, __global float *qxm,
        __global float *qym)
{
    // Element index
    const uint idx = get_global_id(0) + get_global_id(1)*get_global_size(0);

    // Determine if cell is active and reset variables
    if (IALL[idx]) {
        if (!IWET[idx]) {
            etm[idx] = etl[idx];
            qxm[idx] = qxl[idx];
            qym[idx] = qyl[idx];
        }

        if (iact[idx] && (H[idx]+etm[idx]) < constants->dpMin) {
            iact[idx] = false;
            qxm[idx] = 0.0f;
            qym[idx] = 0.0f;
        }
    }
}

// ---------------------------------------------------------------------------
// Reset velocity at wet / dry interface
// ---------------------------------------------------------------------------
__kernel void macWetDryInterface(__global const uint *IWET,
        __global const uint *iact, const uint offset, __global float *q)
{
    // Element index
    const uint idx = get_global_id(0) + get_global_id(1)*get_global_size(0);

    // Velocity at the wet / dry interface should be zero
    if (IWET[idx] && iact[idx] && (!iact[idx-offset] || !iact[idx+offset])) {
        q[idx] = 0.f;
    }
}
