// Constants
__constant const float ALPHA_SQ = (0.41*0.41) / (6.0*6.0);

// ---------------------------------------------------------------------------
// Parabolic depth-averaged eddy viscosity
// ---------------------------------------------------------------------------
float parabolic(const float eddyCoef, const float qx, const float qy,
        const float chz)
{
    return eddyCoef * native_sqrt(qx*qx + qy*qy) / chz;
}

// ---------------------------------------------------------------------------
// Distance to nearest "wall" (dry cell)
// ---------------------------------------------------------------------------
float wallDistance(const uint numRows, const uint numCols, const uint row,
        const uint col, const float dpCoef, const float dx,
        __global const uint *iact)
{
    // Search area
    const uint num_cells = ceil(dpCoef / dx);
    const uint row_start = max(0, (int)row-(int)num_cells);
    const uint row_stop = min(numRows, row+num_cells);
    const uint col_start = max(0, (int)col-(int)num_cells);
    const uint col_stop = min(numCols, col+num_cells);

    // Return water depth if no nearby cells are dry
    float wall_distance = dpCoef;

    // Search for nearest dry cell
    for (uint i = row_start; i < row_stop; i++) {
        for (uint j = col_start; j < col_stop; j++) {
            if (!iact[j + i*numCols]) {
                const float x_sq = (col-j)*(col-j);
                const float y_sq = (row-i)*(row-i);
                const float wall_distance_tmp = dx * native_sqrt(x_sq + y_sq);
                if (wall_distance_tmp < wall_distance) {
                    wall_distance = wall_distance_tmp;
                }
            }
        }
    }

    return wall_distance;
}

// ---------------------------------------------------------------------------
// Mixing length eddy viscosity
// ---------------------------------------------------------------------------
float mixingLength(const uint numRows, const uint numCols, const uint row,
        const uint col, const uint idx, __global const uint *iact,
        __global const float *qx, __global const float *qy, const float dp,
        const float chz, const float dx)
{
    const float vel_shear_sq = 9.81f * (qx[idx]*qx[idx] + qy[idx]*qy[idx]) /
            (chz*chz);
    float dp_coef = 1.2f * dp;
    if (dp_coef > dx) {
        dp_coef = wallDistance(numRows, numCols, row, col, dp_coef, dx, iact);
    }

    const uint ip1 = idx + 1;
    const uint im1 = idx - 1;
    const uint jp1 = idx + numCols;
    const uint jm1 = idx - numCols;
    const float ssq = (0.5f * pown(qx[ip1]-qx[im1], 2)
            + 0.5f * pown(qy[jp1]-qy[jm1], 2)
            + pown(0.5f*(qx[jp1]-qx[jm1]+qy[ip1]-qy[im1]), 2)) / (dx*dx);

    return native_sqrt(ALPHA_SQ * vel_shear_sq + pown(0.41f*dp_coef, 4) * ssq);
}
