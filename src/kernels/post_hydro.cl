#include "types.h"
#include "bed_roughness.cl"
#include "vegetation.cl"

// ---------------------------------------------------------------------------
// Update active cells and max values
// ---------------------------------------------------------------------------
__kernel void activeCellsMaxVals(__constant const Constants *constants,
        __global const float *H, __global const float *etu,
        __global const float *qxu, __global const float *qyu,
        __global uint *iact, __global float *etmax, __global float *uvmax)
{
    // Element index
    const uint idx = get_global_id(0) + get_global_id(1)*get_global_size(0);

    // Load values into registers
    const float et = etu[idx];
    const float dp = H[idx] + et;

    // Active cells
    const bool active = (dp > constants->dpMin);
    iact[idx] = active;

    // Max water elevation
    etmax[idx] = (et > etmax[idx] ? et : etmax[idx]);

    // Max velocity
    if (active) {
        const float qx = qxu[idx];
        const float qy = qyu[idx];
        const float vel = native_sqrt(qx*qx + qy*qy) / dp;
        uvmax[idx] = (vel > uvmax[idx] ? vel : uvmax[idx]);
    }
}

// ---------------------------------------------------------------------------
// Calculate bed shear stress
// ---------------------------------------------------------------------------
__kernel void bedShearStress(__constant const Constants *constants,
        __global const uint *IWET, __global const uint *iact,
        __global const float *H, __global const float *etu,
        __global const float *qxu, __global const float *qyu,
        __global const float *VARCHZ, __constant const float *vegData,
        __constant const float *vegHeights, __constant const float *vegWidths,
        __global float *taux, __global float *tauy, __global float *taumax)
{
    // Element index
    const uint idx = get_global_id(0) + get_global_id(1)*get_global_size(0);

    // Check if cell is active
    const bool active = iact[idx];
    float taux_tmp = 0.f;
    float tauy_tmp = 0.f;
    if (active) {
        // Load values into registers
        const float dp = H[idx] + etu[idx];
        const float qx = qxu[idx];
        const float qy = qyu[idx];

        // Calculate bed roughness
        float chz;
        if (constants->roughnessScheme == Constant) {
            chz = VARCHZ[idx];
        } else if (constants->roughnessScheme == Manning) {
            chz = chzManning(dp, VARCHZ[idx]);
        } else if (constants->roughnessScheme == CwTrans) {
            chz = chzCwTrans(dp, qx, qy, VARCHZ[idx]);
        } else if (constants->roughnessScheme == CwRough) {
            chz = chzCwRough(dp, VARCHZ[idx]);
        }

        // Calculate bed shear stress
        const float veg_poros = vegPorosity(active, IWET[idx], vegData,
                vegHeights, vegWidths, dp);
        const float tmp = veg_poros * 9810.f * native_sqrt(qx*qx + qy*qy)
                / (dp*dp*chz*chz);
        taux_tmp = tmp * qx;
        tauy_tmp = tmp * qy;
        const float tau = native_sqrt(taux_tmp*taux_tmp + tauy_tmp*tauy_tmp);
        const float tau_max = taumax[idx];
        taumax[idx] = (tau > tau_max ? tau : tau_max);
    }
    taux[idx] = taux_tmp;
    tauy[idx] = tauy_tmp;
}
