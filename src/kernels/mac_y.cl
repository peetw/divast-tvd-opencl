#include "types.h"
#include "advection.cl"
#include "bed_roughness.cl"
#include "turbulence.cl"
#include "vegetation.cl"

// ---------------------------------------------------------------------------
// MacCormack predictor step - y direction
// ---------------------------------------------------------------------------
__kernel void macPredY(__constant const Constants *constants,
        __constant const Vars *vars, __constant const float *vegData,
        __constant const float *vegHeights, __constant const float *vegAreas,
        __constant const float *vegWidths, __global const uint *IWET,
        __global const uint *iact, __global const float *VARCHZ,
        __global const float *H, __global const float *etl,
        __global const float *qxl, __global const float *qyl,
        __global float *etm, __global float *qxm, __global float *qym)
{
    // Element index
    const uint idx = get_global_id(0) + get_global_id(1)*get_global_size(0);

    const uint iwet = IWET[idx];
    if (iwet) {
        // Load values into registers
        const bool active = iact[idx];
        const uint idx_next = idx + get_global_size(0);
        const uint idx_prev = idx - get_global_size(0);
        const float etl_tmp = etl[idx];
        const float qxl_tmp = qxl[idx];
        const float qyl_tmp = qyl[idx];
        float qxm_tmp = qxl_tmp;
        float qym_tmp = qyl_tmp;

        // Get vegetation solid volume fraction
        const float dp = H[idx] + etl_tmp;
        const float veg_poros = vegPorosity(active, iwet, vegData, vegHeights,
                vegWidths, dp);

        // Continuity equation - y direction
        etm[idx] = etl_tmp + vars->dtDx * (qyl_tmp - qyl[idx_next]) / veg_poros;

        if (active) {
            // Calculate advective terms - y direction
            const float2 advec = advectionY(dp, qxl_tmp, qyl_tmp);
            float2 advec_next = (float2)(0.f, 0.f);
            float dp_next;
            if (iact[idx_next]) {
                dp_next = H[idx_next] + etl[idx_next];
                advec_next = advectionY(dp_next, qxl[idx_next], qyl[idx_next]);
            }

            // Calculate bed friction coefficient
            float chz;
            if (constants->roughnessScheme == Constant) {
                chz = VARCHZ[idx];
            } else if (constants->roughnessScheme == Manning) {
                chz = chzManning(dp, VARCHZ[idx]);
            } else if (constants->roughnessScheme == CwTrans) {
                chz = chzCwTrans(dp, qxl_tmp, qyl_tmp, VARCHZ[idx]);
            } else if (constants->roughnessScheme == CwRough) {
                chz = chzCwRough(dp, VARCHZ[idx]);
            }

            // Calculate eddy viscosity
            float eddy_visc;
            if (constants->eddyScheme == None) {
                eddy_visc = 0.f;
            } else if (constants->eddyScheme == Parabolic) {
                eddy_visc = parabolic(constants->eddyCoef, qxl_tmp, qyl_tmp,
                        chz);
            } else if (constants->eddyScheme == MixingLength) {
                eddy_visc = mixingLength(get_global_size(1), get_global_size(0),
                        get_global_id(1), get_global_id(0), idx, iact, qxl, qyl,
                        dp, chz, constants->dx);
            }

            if (iact[idx_next] && iact[idx_prev]) {
                // Advective acceleration - y direction
                qym_tmp = qyl_tmp - vars->dtDxBeta * (advec_next.y - advec.y);

                // Coriolis force - y direction
                qym_tmp = qym_tmp - vars->dtCori * qxl_tmp;

                // Vegetative drag force - y direction
                const float vel = native_sqrt(qxl_tmp*qxl_tmp + qyl_tmp*qyl_tmp)
                        / dp;
                const float veg_drag = vegDrag(vegData, vegHeights, vegAreas,
                        vegWidths, iwet, dp, vel, qyl_tmp);
                qym_tmp = qym_tmp - vars->dt * veg_drag;

                // Pressure gradient (water slope) - y direction
                qym_tmp = qym_tmp - vars->hDtDxG * (dp + dp_next)
                        * (etl[idx_next] - etl_tmp);

                // Bed friction - y direction
                const float tmp = vars->dtG * vel / (dp*chz*chz);
                if (tmp < 0.3f) {
                    qym_tmp = qym_tmp - veg_poros * tmp * qyl_tmp;
                } else {
                    qym_tmp = qym_tmp / (1.f + veg_poros * tmp);
                }

                // Turbulence - y direction
                qym_tmp = qym_tmp + 2.f * vars->dtDxSq * eddy_visc
                        * (qyl[idx_next] - 2.f*qyl_tmp + qyl[idx_prev]);
            } else {
                qym_tmp = 0.f;
            }

            // Advective acceleration - x direction
            qxm_tmp = qxl_tmp - vars->dtDxBeta * (advec_next.x - advec.x);

            // Turbulence - x direction
            qxm_tmp = qxm_tmp + vars->dtDxSq * eddy_visc * (qxl[idx_next] -
                    2.f*qxl_tmp + qxl[idx_prev]);
        }

        // Store updated values
        qxm[idx] = qxm_tmp;
        qym[idx] = qym_tmp;
    }
}

// ---------------------------------------------------------------------------
// MacCormack corrector step - y direction
// ---------------------------------------------------------------------------
__kernel void macCorrY(__constant const Constants *constants,
        __constant const Vars *vars, __constant const float *vegData,
        __constant const float *vegHeights, __constant const float *vegAreas,
        __constant const float *vegWidths, __global const uint *IWET,
        __global const uint *iact, __global const float *VARCHZ,
        __global const float *H, __global const float *etl,
        __global const float *qxl, __global const float *qyl,
        __global const float *etm, __global const float *qxm,
        __global const float *qym, __global float *etu, __global float *qxu,
        __global float *qyu)
{
    // Element index
    const uint idx = get_global_id(0) + get_global_id(1)*get_global_size(0);

    const uint iwet = IWET[idx];
    if (iwet) {
        // Load values into registers
        const bool active = iact[idx];
        const uint idx_next = idx + get_global_size(0);
        const uint idx_prev = idx - get_global_size(0);
        const float etm_tmp = etm[idx];
        const float qxm_tmp = qxm[idx];
        const float qym_tmp = qym[idx];
        float qxu_tmp = qxl[idx];
        float qyu_tmp = qyl[idx];

        // Get vegetation solid volume fraction
        const float dp = H[idx] + etm_tmp;
        const float veg_poros = vegPorosity(active, iwet, vegData, vegHeights,
                vegWidths, dp);

        // Continuity equation - y direction
        float etu_tmp = etl[idx] + vars->dtDx * (qym[idx_prev] - qym_tmp)
                / veg_poros;

        if (active) {
            // Calculate advective terms - y direction
            const float2 advec = advectionY(dp, qxm_tmp, qym_tmp);
            float2 advec_prev = (float2)(0.f, 0.f);
            float dp_prev;
            if (iact[idx_prev]) {
                dp_prev = H[idx_prev] + etm[idx_prev];
                advec_prev = advectionY(dp_prev, qxm[idx_prev], qym[idx_prev]);
            }

            // Calculate bed friction coefficient
            float chz;
            if (constants->roughnessScheme == Constant) {
                chz = VARCHZ[idx];
            } else if (constants->roughnessScheme == Manning) {
                chz = chzManning(dp, VARCHZ[idx]);
            } else if (constants->roughnessScheme == CwTrans) {
                chz = chzCwTrans(dp, qxm_tmp, qym_tmp, VARCHZ[idx]);
            } else if (constants->roughnessScheme == CwRough) {
                chz = chzCwRough(dp, VARCHZ[idx]);
            }

            // Calculate eddy viscosity
            float eddy_visc;
            if (constants->eddyScheme == None) {
                eddy_visc = 0.f;
            } else if (constants->eddyScheme == Parabolic) {
                eddy_visc = parabolic(constants->eddyCoef, qxm_tmp, qym_tmp,
                        chz);
            } else if (constants->eddyScheme == MixingLength) {
                eddy_visc = mixingLength(get_global_size(1), get_global_size(0),
                        get_global_id(1), get_global_id(0), idx, iact, qxm, qym,
                        dp, chz, constants->dx);
            }

            if (iact[idx_next] && iact[idx_prev]) {
                // Advective acceleration - y direction
                qyu_tmp = qyu_tmp - vars->dtDxBeta * (advec.y - advec_prev.y);

                // Coriolis force - y direction
                qyu_tmp = qyu_tmp - vars->dtCori * qxm_tmp;

                // Vegetative drag force - y direction
                const float vel = native_sqrt(qxm_tmp*qxm_tmp + qym_tmp*qym_tmp)
                        / dp;
                const float veg_drag = vegDrag(vegData, vegHeights, vegAreas,
                        vegWidths, iwet, dp, vel, qym_tmp);
                qyu_tmp = qyu_tmp - vars->dt * veg_drag;

                // Pressure gradient (water slope) - y direction
                qyu_tmp = qyu_tmp - vars->hDtDxG * (dp + dp_prev)
                        * (etm_tmp - etm[idx_prev]);

                // Bed friction - y direction
                const float tmp = vars->dtG * vel / (dp*chz*chz);
                if (tmp < 0.3f) {
                    qyu_tmp = qyu_tmp - veg_poros * tmp * qym_tmp;
                } else {
                    qyu_tmp = qyu_tmp / (1.f + veg_poros * tmp);
                }

                // Turbulence - y direction
                qyu_tmp = qyu_tmp + 2.f * vars->dtDxSq * eddy_visc
                        * (qym[idx_next] - 2.f*qym_tmp + qym[idx_prev]);
            } else {
                qyu_tmp = 0.f;
            }

            // Advective acceleration - x direction
            qxu_tmp = qxu_tmp - vars->dtDxBeta * (advec.x - advec_prev.x);

            // Turbulence - x direction
            qxu_tmp = qxu_tmp + vars->dtDxSq * eddy_visc * (qxm[idx_next] -
                    2.f*qxm_tmp + qxm[idx_prev]);
        }

        // Store updated values
        etu[idx] = 0.5f * (etm_tmp + etu_tmp);
        qxu[idx] = 0.5f * (qxm_tmp + qxu_tmp);
        qyu[idx] = 0.5f * (qym_tmp + qyu_tmp);
    }
}
