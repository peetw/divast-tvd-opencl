#include "types.h"

// ---------------------------------------------------------------------------
// Find max Courant number across domain
// ---------------------------------------------------------------------------
__kernel void maxCourantNumber(const uint input_elements, __local float *scratch,
        __global const uint *iact, __global const float *H,
        __global const float *etu, __global const float *qxu,
        __global const float *qyu, __global float *courant)
{
    // Initialize max Courant number
    uint idx = get_global_id(0);
    float courant_max = 0.f;

    // Loop sequentially over chunks of input vector
    while (idx < input_elements) {
        if (iact[idx]) {
            const float dp = H[idx] + etu[idx];
            const float qx = qxu[idx];
            const float qy = qyu[idx];
            const float courant_tmp = native_sqrt(9.81f*dp) +
                    native_sqrt(qx*qx + qy*qy) / dp;
            courant_max = (courant_tmp > courant_max ? courant_tmp : courant_max);
        }
        idx += get_global_size(0);
    }

    // Store result in local memory
    const uint local_idx = get_local_id(0);
    scratch[local_idx] = courant_max;
    barrier(CLK_LOCAL_MEM_FENCE);

    // Perform parallel reduction
    for (uint offset = get_local_size(0) / 2; offset > 0; offset /= 2) {
        if (local_idx < offset) {
            const float courant_tmp = scratch[local_idx + offset];
            courant_max = scratch[local_idx];
            scratch[local_idx] = (courant_tmp > courant_max ? courant_tmp : courant_max);
        }
        barrier(CLK_LOCAL_MEM_FENCE);
    }

    // Store result in global memory
    if (local_idx == 0) {
        courant[get_group_id(0)] = scratch[0];
    }
}
