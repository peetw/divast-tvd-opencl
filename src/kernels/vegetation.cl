// ---------------------------------------------------------------------------
// Linear interpolation of vegetation width or projected area at given height
// ---------------------------------------------------------------------------
float interp(const float dp, __constant const float *vegHeights,
        __constant const float *vegProp, const uint idx_first,
        const uint idx_last)
{
    // Find nearest value and interpolate
    for (uint idx = idx_first; idx < idx_last; idx++) {
        const uint idx_next = idx + 1;
        if (dp >= vegHeights[idx] && dp <= vegHeights[idx_next]) {
            return vegProp[idx] + (vegProp[idx_next] - vegProp[idx])
                    * (dp - vegHeights[idx])
                    / (vegHeights[idx_next] - vegHeights[idx]);
        }
    }

    // If dp > veg height (i.e. vegetation submerged)
    return vegProp[idx_last];
}

// ---------------------------------------------------------------------------
// Calculate vegetation solid volume fraction
// ---------------------------------------------------------------------------
float vegPorosity(const bool active, const uint IWET,
        __constant const float *vegData, __constant const float *vegHeights,
        __constant const float *vegWidths, const float dp)
{
    if (active && IWET > 1) {
        const uint veg_idx = (IWET - 2) * 6;
        const uint idx_first = vegData[veg_idx+4];
        const uint idx_last = idx_first + vegData[veg_idx+5] - 1;
        const float width = interp(dp, vegHeights, vegWidths, idx_first,
                idx_last);
        const float width_avrg = 0.5f * (vegWidths[idx_first] + width);
        return max(1e-15f, 1 - M_PI_4_F * width_avrg*width_avrg
                * vegData[veg_idx]);
    }
    return 1.f;
}

// ---------------------------------------------------------------------------
// Calculate vegetative drag force
// ---------------------------------------------------------------------------
float vegDrag(__constant const float *vegData,
        __constant const float *vegHeights,
        __constant const float *vegAreas, __constant const float *vegWidths,
        const uint IWET, const float dp, const float vel, const float q)
{
    // vegData[veg_idx] = density
    // vegData[veg_idx+1] = flex-rigid
    // vegData[veg_idx+2] = drag-coef
    // vegData[veg_idx+3] = vogel-exp * 0.5f
    // vegData[veg_idx+4] = offset
    // vegData[veg_idx+5] = number of height data points

    if (IWET > 1) {
        const uint veg_idx = (IWET - 2) * 6;
        const uint idx_first = vegData[veg_idx+4];
        const uint idx_last = idx_first + vegData[veg_idx+5] - 1;

        const float height = min(dp, vegHeights[idx_last]);
        const float ap = interp(dp, vegHeights, vegAreas, idx_first, idx_last);
        const float width = interp(dp, vegHeights, vegWidths, idx_first,
                idx_last);

        const float cauchy = max(1.f, 1000.f * vel*vel * ap * height * width
                / vegData[veg_idx+1]);

        return 0.5f * vegData[veg_idx+2] * ap
                * native_powr(cauchy, vegData[veg_idx+3])
                * vegData[veg_idx] * vel * q / dp;
    }
    return 0.f;
}
