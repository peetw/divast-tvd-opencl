#include "types.h"

// ---------------------------------------------------------------------------
// Total Variation Diminishing (TVD) main step - y direction
// ---------------------------------------------------------------------------
__kernel void tvdY(__constant const Vars *vars, __global const uint *iact,
        __global const float *H, __global const float *etl,
        __global const float *qxl, __global const float *qyl,
        __global float *etm, __global float *qxm, __global float *qym)
{
    // Element index
    const uint idx = get_global_id(0) + get_global_id(1)*get_global_size(0);
    const uint idx_prev = idx - get_global_size(0);

    // Ignore first and last columns and first row
    if (get_global_id(0) > 0 && get_global_id(0) < get_global_size(0)-1
            && get_global_id(1) > 0
            && iact[idx] && iact[idx_prev]) {
        // Load values into registers
        const uint idx_next = idx + get_global_size(0);
        const float etl_tmp = etl[idx];
        const float qxl_tmp = qxl[idx];
        const float qyl_tmp = qyl[idx];
        const float etl_prev = etl[idx_prev];
        const float qxl_prev = qxl[idx_prev];
        const float qyl_prev = qyl[idx_prev];

        // ----------------------------
        // Calculate deltas
        // ----------------------------
        const float detm1 = etl_tmp - etl_prev;
        const float dqxm1 = qxl_tmp - qxl_prev;
        const float dqym1 = qyl_tmp - qyl_prev;

        // ----------------------------
        // Calculate gradients
        // ----------------------------
        float gplus_prev = 0.f;
        float gminus = 0.f;

        // Ignore second and last rows
        if (get_global_id(1) > 1 && get_global_id(1) < get_global_size(1)-1) {
            // Element index two rows previous
            const uint idx_prev2 = idx - 2*get_global_size(0);
            if (iact[idx_prev2]) {
                // Rplus
                float tmp = (etl_prev - etl[idx_prev2]) * detm1
                        + (qxl_prev - qxl[idx_prev2]) * dqxm1
                        + (qyl_prev - qyl[idx_prev2]) * dqym1;

                const float r = tmp / (1.e-15f + detm1*detm1 + dqxm1*dqxm1
                        + dqym1*dqym1);

                // Local Courant number
                tmp = H[idx_prev] + etl_prev;
                tmp = vars->dtDx * (fabs(qyl_prev/tmp)
                        + native_sqrt(9.81f*tmp));
                tmp = (tmp < 0.5f) ? 0.5f * tmp * (1.f-tmp) : 0.125f;

                // Gplus
                gplus_prev = tmp * (1.f - max(0.f, min(2.f * r, 1.f)));
            }

            if (iact[idx_next]) {
                // Rminus
                float tmp = (etl[idx_next] - etl_tmp) * detm1
                        + (qxl[idx_next] - qxl_tmp) * dqxm1
                        + (qyl[idx_next] - qyl_tmp) * dqym1;

                const float r = tmp / (1.e-15f + detm1*detm1 + dqxm1*dqxm1
                        + dqym1*dqym1);

                // Local Courant number
                tmp = H[idx] + etl_tmp;
                tmp = vars->dtDx * (fabs(qyl_tmp/tmp)
                        + native_sqrt(9.81f*tmp));
                tmp = (tmp < 0.5f) ? 0.5f * tmp * (1.f-tmp) : 0.125f;

                // Gminus
                gminus = tmp * (1.f - max(0.f, min(2.f * r, 1.f)));
            }
        }

        // ----------------------------
        // Diffusion
        // ----------------------------
        const float tmp = gplus_prev + gminus;
        etm[idx] = tmp * detm1;
        qxm[idx] = tmp * dqxm1;
        qym[idx] = tmp * dqym1;
    } else {
        etm[idx] = 0.f;
        qxm[idx] = 0.f;
        qym[idx] = 0.f;
    }
}
