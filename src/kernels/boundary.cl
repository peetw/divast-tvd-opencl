// ---------------------------------------------------------------------------
// Interpolate water level or flow rate at current time based on time series
// ---------------------------------------------------------------------------
float interp(const float time, __constant const float *bcTsTimes,
        __constant const float *bcTsValues, const uint offset,
        const uint szTimes)
{
    // Index of last value for current boundary condition
    const uint idx_last = offset + szTimes - 1;

    // Find nearest value and interpolate
    for (uint idx = offset; idx < idx_last; idx++) {
        const uint idx_next = idx + 1;
        if (time >= bcTsTimes[idx] && time <= bcTsTimes[idx_next]) {
            return bcTsValues[idx] + (bcTsValues[idx_next] - bcTsValues[idx])
                    * (time - bcTsTimes[idx])
                    / (bcTsTimes[idx_next] - bcTsTimes[idx]);
        }
    }

    // If time > last time value
    return bcTsValues[idx_last];
}

// ---------------------------------------------------------------------------
// Time series water level boundary
// ---------------------------------------------------------------------------
__kernel void boundaryWlTime(__constant const uint *bcWlTime,
        __constant const float *bcTsTimes, __constant const float *bcTsValues,
        const float time, const uint numCols, __global float *etu)
{
    const uint tid = get_global_id(0) * 6;

    const uint row_min = bcWlTime[tid];
    const uint row_max = bcWlTime[tid+1];
    const uint col_min = bcWlTime[tid+2];
    const uint col_max = bcWlTime[tid+3];
    const uint offset = bcWlTime[tid+4];
    const uint sz_times = bcWlTime[tid+5];

    const float wl = interp(time, bcTsTimes, bcTsValues, offset, sz_times);

    for (uint i = row_min; i < row_max; i++) {
        for (uint j = col_min; j < col_max; j++) {
            etu[j + i*numCols] = wl;
        }
    }
}

// ---------------------------------------------------------------------------
// Time series x-direction flow rate boundary
// ---------------------------------------------------------------------------
__kernel void boundaryQxTime(__constant const uint *bcQxTime,
        __constant const float *bcTsTimes, __constant const float *bcTsValues,
        const float time, const uint numCols, __global float *qxu)
{
    const uint tid = get_global_id(0) * 6;

    const uint row_min = bcQxTime[tid];
    const uint row_max = bcQxTime[tid+1];
    const uint col_min = bcQxTime[tid+2];
    const uint col_max = bcQxTime[tid+3];
    const uint offset = bcQxTime[tid+4];
    const uint sz_times = bcQxTime[tid+5];

    const float qx = interp(time, bcTsTimes, bcTsValues, offset, sz_times);

    for (uint i = row_min; i < row_max; i++) {
        for (uint j = col_min; j < col_max; j++) {
            qxu[j + i*numCols] = qx;
        }
    }
}

// ---------------------------------------------------------------------------
// Time series y-direction flow rate boundary
// ---------------------------------------------------------------------------
__kernel void boundaryQyTime(__constant const uint *bcQyTime,
        __constant const float *bcTsTimes, __constant const float *bcTsValues,
        const float time, const uint numCols, __global float *qyu)
{
    const uint tid = get_global_id(0) * 6;

    const uint row_min = bcQyTime[tid];
    const uint row_max = bcQyTime[tid+1];
    const uint col_min = bcQyTime[tid+2];
    const uint col_max = bcQyTime[tid+3];
    const uint offset = bcQyTime[tid+4];
    const uint sz_times = bcQyTime[tid+5];

    const float qy = interp(time, bcTsTimes, bcTsValues, offset, sz_times);

    for (uint i = row_min; i < row_max; i++) {
        for (uint j = col_min; j < col_max; j++) {
            qyu[j + i*numCols] = qy;
        }
    }
}

// ---------------------------------------------------------------------------
// Free water level boundary
// --------------------------------------------------------------------------
__kernel void boundaryWlFree(__constant const int *bcWlFree, const uint numCols,
        __global float *etu)
{
    const uint tid = get_global_id(0) * 6;

    const uint row_min = bcWlFree[tid];
    const uint row_max = bcWlFree[tid+1];
    const uint col_min = bcWlFree[tid+2];
    const uint col_max = bcWlFree[tid+3];
    const int row_ref = bcWlFree[tid+4];
    const int col_ref = bcWlFree[tid+5];

    for (uint i = row_min; i < row_max; i++) {
        for (uint j = col_min; j < col_max; j++) {
            const uint idx = j + i*numCols;
            const uint ref_idx = idx + col_ref + row_ref*numCols;
            etu[idx] = etu[ref_idx];
        }
    }
}

// ---------------------------------------------------------------------------
// Free flow rate boundary
// --------------------------------------------------------------------------
__kernel void boundaryQxyFree(__constant const int *bcQxyFree,
        const uint numCols, __global float *qxu, __global float *qyu)
{
    const uint tid = get_global_id(0) * 6;

    const uint row_min = bcQxyFree[tid];
    const uint row_max = bcQxyFree[tid+1];
    const uint col_min = bcQxyFree[tid+2];
    const uint col_max = bcQxyFree[tid+3];
    const int row_ref = bcQxyFree[tid+4];
    const int col_ref = bcQxyFree[tid+5];

    for (uint i = row_min; i < row_max; i++) {
        for (uint j = col_min; j < col_max; j++) {
            const uint idx = j + i*numCols;
            const uint ref_idx = idx + col_ref + row_ref*numCols;
            qxu[idx] = qxu[ref_idx];
            qyu[idx] = qyu[ref_idx];
        }
    }
}

// ---------------------------------------------------------------------------
// Open boundary
// ---------------------------------------------------------------------------
__kernel void boundaryOpen(__constant const int *bcOpen, const uint numCols,
        __global float *etu, __global float *qxu, __global float *qyu)
{
    const uint tid = get_global_id(0) * 6;

    const uint row_min = bcOpen[tid];
    const uint row_max = bcOpen[tid+1];
    const uint col_min = bcOpen[tid+2];
    const uint col_max = bcOpen[tid+3];
    const int row_ref = bcOpen[tid+4];
    const int col_ref = bcOpen[tid+5];

    for (uint i = row_min; i < row_max; i++) {
        for (uint j = col_min; j < col_max; j++) {
            const uint idx = j + i*numCols;
            const uint ref_idx = idx + col_ref + row_ref*numCols;
            etu[idx] = etu[ref_idx];
            qxu[idx] = qxu[ref_idx];
            qyu[idx] = qyu[ref_idx];
        }
    }
}
