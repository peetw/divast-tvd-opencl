__constant float CHZ_PARAM = -17.717787672f; // -2.f * sqrt(8.f*9.81f);

// ---------------------------------------------------------------------------
// Manning formula
// ---------------------------------------------------------------------------
float chzManning(const float dp, const float varchz)
{
    return native_powr(dp, 0.166667f) / varchz;
}

// ---------------------------------------------------------------------------
// Colebrook-White formula (Transitional flow, i.e. Re ~ 500-2000)
// ---------------------------------------------------------------------------
float chzCwTrans(const float dp, const float qx, const float qy, const float varchz)
{
    float re = 4.f * native_sqrt(qx*qx + qy*qy) / 1.3e-6f;
    re = (re < 500 ? 500.f : re);

    const float tmp = varchz / (12.f*dp);
    float chz_old = CHZ_PARAM * native_log10(tmp);
    float chz = CHZ_PARAM * native_log10(tmp - 5.f*chz_old / (re*CHZ_PARAM));

    while (fabs(chz-chz_old) > 0.5f) {
        chz_old = chz;
        chz = CHZ_PARAM * native_log10(tmp - 5.f*chz_old / (re*CHZ_PARAM));
    }

    return chz;
}

// ---------------------------------------------------------------------------
// Colebrook-White formula (Fully rough flow, i.e. Re >> 1000)
// ---------------------------------------------------------------------------
float chzCwRough(const float dp, const float varchz)
{
    return CHZ_PARAM * native_log10(varchz/(12.f*dp));
}
