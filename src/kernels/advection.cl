// ---------------------------------------------------------------------------
// Calculate advective terms in x-direction
// ---------------------------------------------------------------------------
float2 advectionX(const float dp, const float qx, const float qy)
{
    const float vel = qx / dp;
    return (float2)(qx*vel, qy*vel);
}

// ---------------------------------------------------------------------------
// Calculate advective terms in y-direction
// ---------------------------------------------------------------------------
float2 advectionY(const float dp, const float qx, const float qy)
{
    const float vel = qy / dp;
    return (float2)(qx*vel, qy*vel);
}
