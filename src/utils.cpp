// Namespace header
#include "utils.h"

// System headers
#include <algorithm>
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <string>

// Third-party headers
#include <CL/cl.hpp>

namespace utils {
bool cmdLineArgExists(int argc, char **argv, const std::string &arg)
{
    char **end = argv + argc;
    return std::find(argv, end, arg) != end;
}

char* getCmdLineArg(int argc, char **argv, const std::string &arg)
{
    char **end = argv + argc;
    char **it = std::find(argv, end, arg);
    if (it != end && ++it != end)
    {
        return *it;
    }
    return 0;
}

void warning(const std::string &msg)
{
    std::cerr << "\nWARNING: " << msg << "\n";
}

void error(const std::string &msg)
{
    std::cerr << "\nERROR: " << msg << "\n\n";
    exit(EXIT_FAILURE);
}

void clError(const cl::Error &e, const char* func)
{
    char msg[128];
    sprintf(msg, "OpenCL error at: %s::%s (%s)", func, e.what(),
            openClErrorStr(e.err()).c_str());
    error(msg);
}

void printHeader(const std::string &msg)
{
    const char pat[64] = "# -------------------------------------------------------------";
    std::cout << "\n\n" << pat << "\n# " << msg << "\n" << pat << "\n";
}

void printProgressHeader()
{
    std::cout << "\n  Clock Time            Output No.   Time Step   Sim Time\n";
    std::cout << " -------------------------------------------------------------\n";
}

void printProgress(const unsigned int totalOut, const unsigned int numTimeStep,
        const float sim_time)
{
    // Increment output counter
    static unsigned int num_out;
    num_out++;

    // Get current datetime
    char date[128];
    time_t raw_time;
    time(&raw_time);
    const struct tm *timeinfo = localtime(&raw_time);
    strftime(date, 128, "%F %T", timeinfo);

    // Format model time
    const unsigned int time_msecs = sim_time * 1000;
    const unsigned int hours = time_msecs / (60*60*1000);
    const unsigned int mins = (time_msecs - 60*60*1000*hours) / (60*1000);
    const unsigned int secs = (time_msecs - 60*60*1000*hours - 60*1000*mins)
            / 1000;
    const unsigned int msecs = time_msecs - 60*60*1000*hours - 60*1000*mins
            - 1000*secs;

    char buf[128];
    sprintf(buf, "  %s   %.3u / %.3u    %-9u   %.2u:%.2u:%.2u.%.3u\n", date,
            num_out, totalOut, numTimeStep, hours, mins, secs, msecs);
    std::cout << buf;
}

std::string openClErrorStr(const cl_int err)
{
    switch (err) {
        case CL_SUCCESS: return "Success!";
        case CL_DEVICE_NOT_FOUND: return "Device not found";
        case CL_DEVICE_NOT_AVAILABLE: return "Device not available";
        case CL_COMPILER_NOT_AVAILABLE: return "Compiler not available";
        case CL_MEM_OBJECT_ALLOCATION_FAILURE: return "Memory object allocation failure";
        case CL_OUT_OF_RESOURCES: return "Out of resources";
        case CL_OUT_OF_HOST_MEMORY: return "Out of host memory";
        case CL_PROFILING_INFO_NOT_AVAILABLE: return "Profiling information not available";
        case CL_MEM_COPY_OVERLAP: return "Memory copy overlap";
        case CL_IMAGE_FORMAT_MISMATCH: return "Image format mismatch";
        case CL_IMAGE_FORMAT_NOT_SUPPORTED: return "Image format not supported";
        case CL_BUILD_PROGRAM_FAILURE: return "Program build failure";
        case CL_MAP_FAILURE: return "Map failure";
        case CL_MISALIGNED_SUB_BUFFER_OFFSET: return "Misaligned sub-buffer offset";
        case CL_EXEC_STATUS_ERROR_FOR_EVENTS_IN_WAIT_LIST: return "Error status for event in wait list";
#ifdef CL_VERSION_1_2
        case CL_COMPILE_PROGRAM_FAILURE: return "Program compile failure";
        case CL_LINKER_NOT_AVAILABLE: return "Linker not available";
        case CL_LINK_PROGRAM_FAILURE: return "Program link failure";
        case CL_DEVICE_PARTITION_FAILED: return "Failed to partition device";
        case CL_KERNEL_ARG_INFO_NOT_AVAILABLE: return "Kernel arg information not available";
#endif
        case CL_INVALID_VALUE: return "Invalid value";
        case CL_INVALID_DEVICE_TYPE: return "Invalid device type";
        case CL_INVALID_PLATFORM: return "Invalid platform";
        case CL_INVALID_DEVICE: return "Invalid device";
        case CL_INVALID_CONTEXT: return "Invalid context";
        case CL_INVALID_QUEUE_PROPERTIES: return "Invalid queue properties";
        case CL_INVALID_COMMAND_QUEUE: return "Invalid command queue";
        case CL_INVALID_HOST_PTR: return "Invalid host pointer";
        case CL_INVALID_MEM_OBJECT: return "Invalid memory object";
        case CL_INVALID_IMAGE_FORMAT_DESCRIPTOR: return "Invalid image format descriptor";
        case CL_INVALID_IMAGE_SIZE: return "Invalid image size";
        case CL_INVALID_SAMPLER: return "Invalid sampler";
        case CL_INVALID_BINARY: return "Invalid binary";
        case CL_INVALID_BUILD_OPTIONS: return "Invalid build options";
        case CL_INVALID_PROGRAM: return "Invalid program";
        case CL_INVALID_PROGRAM_EXECUTABLE: return "Invalid program executable";
        case CL_INVALID_KERNEL_NAME: return "Invalid kernel name";
        case CL_INVALID_KERNEL_DEFINITION: return "Invalid kernel definition";
        case CL_INVALID_KERNEL: return "Invalid kernel";
        case CL_INVALID_ARG_INDEX: return "Invalid argument index";
        case CL_INVALID_ARG_VALUE: return "Invalid argument value";
        case CL_INVALID_ARG_SIZE: return "Invalid argument size";
        case CL_INVALID_KERNEL_ARGS: return "Invalid kernel arguments";
        case CL_INVALID_WORK_DIMENSION: return "Invalid work dimension";
        case CL_INVALID_WORK_GROUP_SIZE: return "Invalid work group size";
        case CL_INVALID_WORK_ITEM_SIZE: return "Invalid work item size";
        case CL_INVALID_GLOBAL_OFFSET: return "Invalid global offset";
        case CL_INVALID_EVENT_WAIT_LIST: return "Invalid event wait list";
        case CL_INVALID_EVENT: return "Invalid event";
        case CL_INVALID_OPERATION: return "Invalid operation";
        case CL_INVALID_GL_OBJECT: return "Invalid OpenGL object";
        case CL_INVALID_BUFFER_SIZE: return "Invalid buffer size";
        case CL_INVALID_MIP_LEVEL: return "Invalid mip-map level";
        case CL_INVALID_GLOBAL_WORK_SIZE: return "Invalid global work size";
        case CL_INVALID_PROPERTY: return "Invalid property";
#ifdef CL_VERSION_1_2
        case CL_INVALID_IMAGE_DESCRIPTOR: return "Invalid image descriptor";
        case CL_INVALID_COMPILER_OPTIONS: return "Invalid compiler options";
        case CL_INVALID_LINKER_OPTIONS: return "Invalid linker options";
        case CL_INVALID_DEVICE_PARTITION_COUNT: return "Invalid device partition count";
#endif
        default: return "Unknown";
    }
}
}   // End namespace utils
