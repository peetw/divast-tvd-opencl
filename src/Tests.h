#pragma once
#define __CL_ENABLE_EXCEPTIONS

// System headers
#include <cmath>
#include <fstream>
#include <iostream>
#include <string>

// Third-party headers
#include <CL/cl.hpp>

// Project headers
#include "types.h"

// Fortran functions
extern "C" {
    // Pre hydro
    void wetting_(const int *imax,
                  const int *jmax,
                  const float *dpmin,
                  const unsigned int *iwet,
                  const unsigned int *iact,
                  float *etu);

    // Hydro XY
    void mac_pred_drying_(const int *imax,
                          const int *jmax,
                          const float *dpmin,
                          const unsigned int *iall,
                          const float *h,
                          const float *etu,
                          const float *qxu,
                          const float *qyu,
                          unsigned int *iact,
                          float *dp,
                          float *etl,
                          float *qxl,
                          float *qyl);
    void mac_corr_drying_(const int *imax,
                          const int *jmax,
                          const float *dpmin,
                          const unsigned int *iwet,
                          const unsigned int *iall,
                          const float *h,
                          const float *etl,
                          const float *qxl,
                          const float *qyl,
                          unsigned int *iact,
                          float *dp,
                          float *etm,
                          float *qxm,
                          float *qym);

    // Hydro X
    void mac_wet_dry_interface_x_(const int *imax,
                                  const int *jmax,
                                  const unsigned int *iwet,
                                  const unsigned int *iact,
                                  float *qx);
    void mac_pred_x_(const int *imax,
                     const int *jmax,
                     const int *nflchz,
                     const int *nfledv,
                     const float *dt,
                     const float *dx,
                     const float *dtdx,
                     const float *dtdxsq,
                     const float *hdtdxg,
                     const float *dtdxbeta,
                     const float *dtg,
                     const float *dtcori,
                     const float *edcoef,
                     const unsigned int *iwet,
                     const unsigned int *iact,
                     const unsigned int *iveg,
                     const unsigned int *npoints,
                     const float *vegden,
                     const float *vegheight,
                     const float *vegcd,
                     const float *vegei,
                     const float *vegvogel,
                     const float *vegdpvar,
                     const float *vegapvar,
                     const float *vegwdvar,
                     const float *varchz,
                     const float *dp,
                     const float *etl,
                     const float *qxl,
                     const float *qyl,
                     float *vegporos,
                     float *etm,
                     float *qxm,
                     float *qym);
    void mac_corr_x_(const int *imax,
                     const int *jmax,
                     const int *nflchz,
                     const int *nfledv,
                     const float *dt,
                     const float *dx,
                     const float *dtdx,
                     const float *dtdxsq,
                     const float *hdtdxg,
                     const float *dtdxbeta,
                     const float *dtg,
                     const float *dtcori,
                     const float *edcoef,
                     const unsigned int *iwet,
                     const unsigned int *iact,
                     const unsigned int *iveg,
                     const unsigned int *npoints,
                     const float *vegden,
                     const float *vegheight,
                     const float *vegcd,
                     const float *vegei,
                     const float *vegvogel,
                     const float *vegdpvar,
                     const float *vegapvar,
                     const float *vegwdvar,
                     const float *varchz,
                     const float *dp,
                     const float *etl,
                     const float *qxl,
                     const float *qyl,
                     const float *etm,
                     const float *qxm,
                     const float *qym,
                     float *vegporos,
                     float *etu,
                     float *qxu,
                     float *qyu);
    void tvd_x_(const int *imax,
                const int *jmax,
                const float *dtdx,
                const unsigned int *iact,
                const float *h,
                float *etl,
                float *qxl,
                float *qyl);
    void tvd_post_x_(const int *imax,
                     const int *jmax,
                     const unsigned int *iwet,
                     const unsigned int *iact,
                     const float *etl,
                     const float *qxl,
                     const float *qyl,
                     float *etu,
                     float *qxu,
                     float *qyu);

    // Hydro Y
    void mac_wet_dry_interface_y_(const int *imax,
                                  const int *jmax,
                                  const unsigned int *iwet,
                                  const unsigned int *iact,
                                  float *qy);
    void mac_pred_y_(const int *imax,
                     const int *jmax,
                     const int *nflchz,
                     const int *nfledv,
                     const float *dt,
                     const float *dx,
                     const float *dtdx,
                     const float *dtdxsq,
                     const float *hdtdxg,
                     const float *dtdxbeta,
                     const float *dtg,
                     const float *dtcori,
                     const float *edcoef,
                     const unsigned int *iwet,
                     const unsigned int *iact,
                     const unsigned int *iveg,
                     const unsigned int *npoints,
                     const float *vegden,
                     const float *vegheight,
                     const float *vegcd,
                     const float *vegei,
                     const float *vegvogel,
                     const float *vegdpvar,
                     const float *vegapvar,
                     const float *vegwdvar,
                     const float *varchz,
                     const float *dp,
                     const float *etl,
                     const float *qxl,
                     const float *qyl,
                     float *vegporos,
                     float *etm,
                     float *qxm,
                     float *qym);
    void mac_corr_y_(const int *imax,
                     const int *jmax,
                     const int *nflchz,
                     const int *nfledv,
                     const float *dt,
                     const float *dx,
                     const float *dtdx,
                     const float *dtdxsq,
                     const float *hdtdxg,
                     const float *dtdxbeta,
                     const float *dtg,
                     const float *dtcori,
                     const float *edcoef,
                     const unsigned int *iwet,
                     const unsigned int *iact,
                     const unsigned int *iveg,
                     const unsigned int *npoints,
                     const float *vegden,
                     const float *vegheight,
                     const float *vegcd,
                     const float *vegei,
                     const float *vegvogel,
                     const float *vegdpvar,
                     const float *vegapvar,
                     const float *vegwdvar,
                     const float *varchz,
                     const float *dp,
                     const float *etl,
                     const float *qxl,
                     const float *qyl,
                     const float *etm,
                     const float *qxm,
                     const float *qym,
                     float *vegporos,
                     float *etu,
                     float *qxu,
                     float *qyu);
    void tvd_y_(const int *imax,
                const int *jmax,
                const float *dtdx,
                const unsigned int *iact,
                const float *h,
                float *etl,
                float *qxl,
                float *qyl);
    void tvd_post_y_(const int *imax,
                     const int *jmax,
                     const unsigned int *iwet,
                     const unsigned int *iact,
                     const float *etl,
                     const float *qxl,
                     const float *qyl,
                     float *etu,
                     float *qxu,
                     float *qyu);

    // Post hydro
    void active_cells_max_vals_(const int *imax,
                                const int *jmax,
                                const float *dpmin,
                                const float *h,
                                const float *etu,
                                const float *qxu,
                                const float *qyu,
                                unsigned int *iact,
                                float *etmax,
                                float *uvmax);
    void bed_shear_stress_(const int *imax,
                           const int *jmax,
                           const int *nflchz,
                           const unsigned int *iwet,
                           const unsigned int *iact,
                           const unsigned int *iveg,
                           const unsigned int *npoints,
                           const float *vegden,
                           const float *vegheight,
                           const float *vegcd,
                           const float *vegei,
                           const float *vegvogel,
                           const float *vegdpvar,
                           const float *vegapvar,
                           const float *vegwdvar,
                           const float *h,
                           const float *varchz,
                           const float *etu,
                           const float *qxu,
                           const float *qyu,
                           const float *vegporos,
                           float *taux,
                           float *tauy,
                           float *taumax);
}

class Tests {
public:
    // Constuctors
    Tests(const cl_device_type deviceType,
         const cl_uint numCores,
         cl_uint platformIdx,
         const unsigned int _numRows,
         const unsigned int _numCols);

    // Tests
    void run();
private:
    // -----------------------------------------------------------------------
    // VARIABLES
    // -----------------------------------------------------------------------

    // --------------------------------
    // Device
    // --------------------------------

    // OpenCL device variables
    cl::Device device;
    cl::Context context;
    cl::CommandQueue queue;

    // Kernel launch parameters
    cl::NDRange globalWorkSz;
    cl::NDRange localWorkSz;

    // Memory sizes for domain sized arrays
    size_t domainClUIntSz;
    size_t domainClFloatSz;

    // Array lengths
    unsigned int numRows;
    unsigned int numCols;
    unsigned int numElements;
    int IMAX;
    int JMAX;

    // --------------------------------
    // Arrays (original)
    // --------------------------------

    // Constant
    Constants constants;
    Vars vars;
    std::vector<cl_float> ZERO;
    std::vector<cl_float> H;
    std::vector<cl_float> VARCHZ;
    std::vector<cl_uint> IWET;
    std::vector<cl_uint> IALL;  // vector<bool> is broken

    // Modifiable
    std::vector<cl_uint> iact;  // vector<bool> is broken
    std::vector<cl_float> etu;
    std::vector<cl_float> qxu;
    std::vector<cl_float> qyu;
    std::vector<cl_float> etl;
    std::vector<cl_float> qxl;
    std::vector<cl_float> qyl;
    std::vector<cl_float> etm;
    std::vector<cl_float> qxm;
    std::vector<cl_float> qym;
    std::vector<cl_float> taux;
    std::vector<cl_float> tauy;
    std::vector<cl_float> etmax;
    std::vector<cl_float> uvmax;
    std::vector<cl_float> taumax;

    // --------------------------------
    // Arrays (host)
    // --------------------------------
    std::vector<cl_uint> iact_host;
    std::vector<cl_float> etu_host;
    std::vector<cl_float> qxu_host;
    std::vector<cl_float> qyu_host;
    std::vector<cl_float> etl_host;
    std::vector<cl_float> qxl_host;
    std::vector<cl_float> qyl_host;
    std::vector<cl_float> etm_host;
    std::vector<cl_float> qxm_host;
    std::vector<cl_float> qym_host;
    std::vector<cl_float> taux_host;
    std::vector<cl_float> tauy_host;
    std::vector<cl_float> etmax_host;
    std::vector<cl_float> uvmax_host;
    std::vector<cl_float> taumax_host;

    std::vector<cl_float> dp_host;
    std::vector<cl_float> vegporos_host;

    // Vegetation
    unsigned int vegnpoints_host[2];
    float vegden_host[2];
    float vegheight_host[2];
    float vegcd_host[2];
    float vegei_host[2];
    float vegvogel_host[2];
    std::vector<float> vegheights_host;
    std::vector<float> vegwidths_host;
    std::vector<float> vegareas_host;

    // --------------------------------
    // Arrays (device)
    // --------------------------------
    std::vector<cl_uint> iact_dev;
    std::vector<cl_float> etu_dev;
    std::vector<cl_float> qxu_dev;
    std::vector<cl_float> qyu_dev;
    std::vector<cl_float> etl_dev;
    std::vector<cl_float> qxl_dev;
    std::vector<cl_float> qyl_dev;
    std::vector<cl_float> etm_dev;
    std::vector<cl_float> qxm_dev;
    std::vector<cl_float> qym_dev;
    std::vector<cl_float> taux_dev;
    std::vector<cl_float> tauy_dev;
    std::vector<cl_float> etmax_dev;
    std::vector<cl_float> uvmax_dev;
    std::vector<cl_float> taumax_dev;

    // --------------------------------
    // Buffers
    // --------------------------------

    // Constants
    cl::Buffer constants_d;
    cl::Buffer vars_d;

    // Vegetation data
    cl::Buffer vegData_d;
    cl::Buffer vegHeights_d;
    cl::Buffer vegWidths_d;
    cl::Buffer vegAreas_d;

    // Initial values
    cl::Buffer H_d;
    cl::Buffer etu_d;
    cl::Buffer qxu_d;
    cl::Buffer qyu_d;
    cl::Buffer VARCHZ_d;
    cl::Buffer IWET_d;
    cl::Buffer IALL_d;
    cl::Buffer iact_d;
    cl::Buffer etmax_d;
    cl::Buffer uvmax_d;

    // Bed shear stress
    cl::Buffer taux_d;
    cl::Buffer tauy_d;
    cl::Buffer taumax_d;

    // Temporary hydrodynamic arrays
    cl::Buffer etl_d;
    cl::Buffer qxl_d;
    cl::Buffer qyl_d;
    cl::Buffer etm_d;
    cl::Buffer qxm_d;
    cl::Buffer qym_d;

    // --------------------------------
    // Kernels
    // --------------------------------

    // mac_xy.cl
    cl::Kernel macPredDrying;
    cl::Kernel macCorrDrying;
    cl::Kernel macWetDryInterface;

    // mac_x.cl
    cl::Kernel macPredX;
    cl::Kernel macCorrX;

    // mac_y.cl
    cl::Kernel macPredY;
    cl::Kernel macCorrY;

    // tvd_x.cl
    cl::Kernel tvdX;

    // tvd_y.cl
    cl::Kernel tvdY;

    // tvd_xy.cl
    cl::Kernel tvdPost;

    // post_hydro.cl
    cl::Kernel activeCellsMaxVals;
    cl::Kernel bedShearStress;


    // -----------------------------------------------------------------------
    // MUTATORS
    // -----------------------------------------------------------------------

    // Setup
    void initOpenCl(const cl_device_type deviceType,
                    const cl_uint numCores,
                    cl_uint platformIdx);
    void initConstants();
    void initArrays();
    void initArraysFortran();
    void initArraysOpenCl();
    void initKernels();

    // Pre hydro
    void wettingTest();

    // Hydro XY
    void macPredDryingTest();
    void macCorrDryingTest();

    // Hydro X
    void macPredInterfaceXTest();
    void macPredXTest();
    void macCorrInterfaceXTest();
    void macCorrXTest();
    void tvdXTest();
    void tvdPostXTest();

    // Hydro Y
    void macPredInterfaceYTest();
    void macPredYTest();
    void macCorrInterfaceYTest();
    void macCorrYTest();
    void tvdYTest();
    void tvdPostYTest();

    // Post hydro
    void activeCellsMaxValsTest();
    void bedShearStressTest();

    // -----------------------------------------------------------------------
    // ACCESSORS
    // -----------------------------------------------------------------------
    cl::Kernel buildKernel(const std::string &filename,
                           const std::string &funcName);
    template<typename T>
    void check(const std::string &funcName,
               const std::string &varName,
               const std::vector<T> &host,
               const std::vector<T> &dev)
    {
        std::ofstream file("out/" + funcName + "_" + varName);
        file << "I    J     HOST        DEVICE      ERROR ABS   ERROR %\n";
        file << "--------------------------------------------------------\n";

        const unsigned int num_rows = globalWorkSz[1];
        const unsigned int num_cols = globalWorkSz[0];

        unsigned int error_count = 0;
        float error_min = NAN;
        float error_avrg = 0;
        float error_max = NAN;

        for (unsigned int i = 0; i < num_rows; i++) {
            for (unsigned int j = 0; j < num_cols; j++) {
                const int idx = j + i * num_cols;

                const float error_abs = fabs(host[idx]-dev[idx]);
                const float error_pcnt = 100.f * error_abs / host[idx];
                const bool error = (error_pcnt > 0.1);

                if (error) {
                    error_count++;
                    error_min = fmin(error_min, error_abs);
                    error_avrg += error_abs;
                    error_max = fmax(error_max, error_abs);

                    char buf[128];
                    std::string fmt = "%-4u %-4u %11.4e %11.4e %11.4e %9.2e\n";
                    if (std::is_same<T, cl_uint>::value) {
                        fmt = "%-4u %-4u  %-11u %-11u %-11i %-9.2e\n";
                    }
                    sprintf(buf, fmt.c_str(), i, j, host[idx], dev[idx],
                            host[idx]-dev[idx], error_pcnt);
                    file << buf;
                }
            }
        }
        file << "\n\nTOTAL ERROR COUNT: " << error_count << std::endl;

        printf("  %-22s  %-8s  %7u   %9.3e  %9.3e  %9.3e\n", funcName.c_str(),
                varName.c_str(), error_count, error_min,
                error_avrg / error_count, error_max);
    }
};


// Anonymous namespace
namespace {
    float rval(const float lo, const float hi);
}
