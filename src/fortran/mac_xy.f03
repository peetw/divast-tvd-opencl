subroutine mac_pred_drying(IMAX, JMAX, DPMIN, IALL, H, ETU, QXU, QYU, &
    &                      IACT, DP, ETL, QXL, QYL)

    implicit none

    INTEGER, INTENT(IN) :: IMAX, JMAX
    real, intent(in) :: DPMIN
    logical(4), DIMENSION(IMAX,JMAX), INTENT(IN) :: IALL
    real, DIMENSION(IMAX,JMAX), INTENT(IN) :: H, ETU, QXU, QYU
    logical(4), DIMENSION(IMAX,JMAX), INTENT(OUT) :: IACT
    real, DIMENSION(IMAX,JMAX), INTENT(OUT) :: DP, ETL, QXL, QYL

    integer :: i, j

    DO J = 1, JMAX
        DO I = 1, IMAX
            IF( IALL(I,J) ) THEN
                DP(I,J) = H(I,J) + ETU(I,J)
                IF( DP(I,J) .GT. DPMIN ) THEN
                    IACT(I,J) = .TRUE.
                    QXL(I,J) = QXU(I,J)
                    QYL(I,J) = QYU(I,J)
                ELSE
                    IACT(I,J) = .FALSE.
                    QXL(I,J) = 0.0
                    QYL(I,J) = 0.0
                END IF
                ETL(I,J) = ETU(I,J)
            END IF
        END DO
    END DO
end subroutine


subroutine mac_corr_drying(IMAX, JMAX, DPMIN, IWET, IALL, H, ETL, QXL, QYL, &
    &                      IACT, DP, ETM, QXM, QYM)

    implicit none

    INTEGER, INTENT(IN) :: IMAX, JMAX
    real, intent(in) :: DPMIN
    logical(4), DIMENSION(IMAX,JMAX), INTENT(IN) :: IWET, IALL
    real, DIMENSION(IMAX,JMAX), INTENT(IN) :: H, ETL, QXL, QYL
    logical(4), DIMENSION(IMAX,JMAX), INTENT(INOUT) :: IACT
    real, DIMENSION(IMAX,JMAX), INTENT(OUT) :: DP, ETM, QXM, QYM

    integer :: i, j

    DO J = 1, JMAX
        DO I = 1, IMAX
            IF( IALL(I,J) ) THEN
                IF( .NOT. IWET(I,J) ) THEN
                    ETM(I,J) = ETL(I,J)
                    QXM(I,J) = QXL(I,J)
                    QYM(I,J) = QYL(I,J)
                END IF

                DP(I,J) = H(I,J) + ETM(I,J)
                IF( IACT(I,J) .AND. DP(I,J).LT.DPMIN ) THEN
                    IACT(I,J) = .FALSE.
                    QXM(I,J) = 0.0
                    QYM(I,J) = 0.0
                END IF
            END IF
        END DO
    END DO
end subroutine


subroutine mac_wet_dry_interface_x(IMAX, JMAX, IWET, IACT, QX)

    implicit none

    INTEGER, INTENT(IN) :: IMAX, JMAX
    logical(4), DIMENSION(IMAX,JMAX), INTENT(IN) :: IWET, IACT
    real, DIMENSION(IMAX,JMAX), INTENT(out) :: QX

    integer :: i, j

    DO J = 1, JMAX
        DO I = 2, IMAX-1
            IF( IWET(I,J) .AND. IACT(I,J) ) THEN    !THE VELOCITY AT WET/DRY INTERFACE SHOULD BE 0
                IF( (.NOT.IACT(I-1,J)) .OR. (.NOT.IACT(I+1,J)) ) THEN
                    QX(I,J) = 0.0
                END IF
            END IF
        END DO
    END DO
end subroutine


subroutine mac_wet_dry_interface_y(IMAX, JMAX, IWET, IACT, QY)

    implicit none

    INTEGER, INTENT(IN) :: IMAX, JMAX
    logical(4), DIMENSION(IMAX,JMAX), INTENT(IN) :: IWET, IACT
    real, DIMENSION(IMAX,JMAX), INTENT(out) :: QY

    integer :: i, j

    DO J = 2, JMAX-1
        DO I = 1, IMAX
            IF( IWET(I,J) .AND. IACT(I,J) ) THEN    ! THE VELOCITY AT WET/DRY INTERFACE SHOULD BE 0
                IF( (.NOT.IACT(I,J-1)) .OR. (.NOT.IACT(I,J+1)) ) THEN
                    QY(I,J) = 0.0
                END IF
            END IF
        END DO
    END DO
end subroutine
