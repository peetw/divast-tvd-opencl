MODULE VEGETATION

    IMPLICIT NONE

    PRIVATE :: INTERP

CONTAINS

    REAL FUNCTION INTERP( N, NPOINTS, DP, DPVAR, VAR )

        IMPLICIT NONE

        INTEGER, INTENT(IN) :: N
        integer, dimension(2), intent(in) :: NPOINTS
        REAL, INTENT(IN) :: DP
        REAL, DIMENSION(11,2), INTENT(IN) :: DPVAR, VAR

        ! LOCAL VARIABLES
        INTEGER :: I, IP1

        DO I = 1, NPOINTS(N)-1
            IP1 = I + 1
            IF( DP.GE.DPVAR(I,N) .AND. DP.LE.DPVAR(IP1,N) ) THEN
                INTERP = VAR(I,N) + (VAR(IP1,N)-VAR(I,N)) * (DP-DPVAR(I,N)) &
&                        / (DPVAR(IP1,N)-DPVAR(I,N))
                RETURN
            END IF
        END DO

        WRITE(*,'(/1X,A/)') 'ERROR IN FUNCTION VEGETATION::INTERP'
        STOP 1

    END FUNCTION INTERP


    SUBROUTINE DRAG( IMAX, JMAX, IWET, IACT, IVEG, QX, QY, Q, DP, NPOINTS, &
&                    VEGDEN, HEIGHT, CD, EI, VOGEL, DPVAR, APVAR, WDVAR, &
&                    VEGDRAG, VEGPOROS )

        IMPLICIT NONE

        ! INPUT VARIABLES
        INTEGER, INTENT(IN) :: IMAX, JMAX
        LOGICAL(4), DIMENSION(IMAX,JMAX), INTENT(IN) :: IWET, IACT
        integer, DIMENSION(IMAX,JMAX), INTENT(IN) :: IVEG
        REAL, DIMENSION(IMAX,JMAX), INTENT(IN) :: QX, QY, Q, DP
        integer, DIMENSION(2), INTENT(IN) :: NPOINTS
        real, dimension(2), intent(in) :: VEGDEN, HEIGHT, CD, EI, VOGEL
        REAL, DIMENSION(11,2), intent(in) :: DPVAR, APVAR, WDVAR
        REAL, DIMENSION(IMAX,JMAX), INTENT(OUT) :: VEGDRAG, VEGPOROS

        ! LOCAL VARIABLES
        INTEGER :: I, J, N
        REAL :: AP, CDAP, CY, HT, VMAG, WIDTH, WDAVRG
        REAL, PARAMETER :: RHO = 1000.0, PI4 = 3.1415926 / 4.0

        ! CALCULATE DRAG FORCE
        DO J = 1, JMAX
            DO I = 1, IMAX
                IF( IACT(I,J) .AND. IWET(I,J) ) THEN
                    ! VEGETATION TYPE
                    N = IVEG(I,J) - 1

                    IF( N .GT. 0 ) THEN
                        ! CHECK IF VEGETATION IS SUBMERGED
                        IF( DP(I,J) .GT. HEIGHT(N) ) THEN
                            AP = APVAR(NPOINTS(N),N)
                            HT = HEIGHT(N)
                            WIDTH = WDVAR(NPOINTS(N),N)
                        ELSE
                            AP = INTERP(N, NPOINTS, DP(I,J), DPVAR, APVAR)
                            HT = DP(I,J)
                            WIDTH = INTERP(N, NPOINTS, DP(I,J), DPVAR, WDVAR)
                        END IF

                        ! CALCULATE BLOCKAGE EFFECT DUE TO VEGETATION
                        WDAVRG = 0.5 * (WDVAR(1,N) + WIDTH)
                        VEGPOROS(I,J) = MAX(1.0E-15, 1.0 - PI4 * WDAVRG**2 &
&                                       * VEGDEN(N))

                        ! CALCULATE CAUCHY NUMBER
                        VMAG = SQRT(QX(I,J)**2 + QY(I,J)**2) / DP(I,J)
                        CY = MAX(1.0, RHO * VMAG**2 * AP * HT * WIDTH / EI(N))

                        ! CALCULATE DEPTH AVERAGED DRAG FORCE
                        CDAP = CD(N) * AP * CY**(0.5*VOGEL(N))
                        VEGDRAG(I,J) = 0.5 * CDAP * VEGDEN(N) * VMAG * Q(I,J) &
&                                      / DP(I,J)
                    ELSE
                        VEGPOROS(I,J) = 1.0
                        VEGDRAG(I,J) = 0.0
                    END IF
                ELSE
                    VEGPOROS(I,J) = 1.0
                END IF
            END DO
        END DO
    END SUBROUTINE DRAG

END MODULE VEGETATION
