subroutine active_cells_max_vals(IMAX, JMAX, DPMIN, H, ETU, QXU, QYU, IACT, &
    &                            ETMAX, UVMAX)

    implicit none

    INTEGER, INTENT(IN) :: IMAX, JMAX
    real, intent(in) :: DPMIN
    real, DIMENSION(IMAX,JMAX), INTENT(IN) :: H, ETU, QXU, QYU
    logical(4), DIMENSION(IMAX,JMAX), INTENT(OUT) :: IACT
    real, DIMENSION(IMAX,JMAX), INTENT(INOUT) :: ETMAX, UVMAX

    integer :: i, j
    real :: ttmp, velmax

    DO J = 1, JMAX
        DO I = 1, IMAX
            TTMP = H(I,J) + ETU(I,J)
            IACT(I,J) = MERGE(.TRUE., .FALSE., TTMP .GT. DPMIN)
            ETMAX(I,J) = MERGE(ETU(I,J), ETMAX(I,J), ETU(I,J) .GT. ETMAX(I,J))
            IF( IACT(I,J) ) THEN
                VELMAX = SQRT(QXU(I,J)**2 + QYU(I,J)**2) / TTMP
                UVMAX(I,J) = MERGE(VELMAX, UVMAX(I,J), VELMAX .GT. UVMAX(I,J))
            END IF
        END DO
    END DO
end subroutine




subroutine bed_shear_stress(IMAX, JMAX, NFLCHZ, IWET, IACT, IVEG, NPOINTS, &
&                           VEGDEN, HEIGHT, CD, EI, VOGEL, DPVAR, APVAR, WDVAR, &
&                           H, VARCHZ, ETU, QXU, QYU, VEGPOROS, TAUX, TAUY, &
&                           TAUMAX)

    use roughness
    use vegetation

    implicit none

    INTEGER, INTENT(IN) :: IMAX, JMAX, NFLCHZ
    LOGICAL(4), DIMENSION(IMAX,JMAX), INTENT(IN) :: IWET, IACT
    integer, DIMENSION(IMAX,JMAX), INTENT(IN) :: IVEG
    integer, DIMENSION(2), INTENT(IN) :: NPOINTS
    real, dimension(2), intent(in) :: VEGDEN, HEIGHT, CD, EI, VOGEL
    REAL, DIMENSION(11,2), intent(in) :: DPVAR, APVAR, WDVAR
    REAL, DIMENSION(IMAX,JMAX), INTENT(IN) :: H, VARCHZ, ETU, QXU, QYU
    REAL, DIMENSION(IMAX,JMAX), INTENT(OUT) :: VEGPOROS, TAUX, TAUY
    REAL, DIMENSION(IMAX,JMAX), INTENT(INOUT) :: TAUMAX

    ! LOCAL VARIABLES
    INTEGER :: I, J
    REAL :: TMP, TAU
    REAL, PARAMETER :: RHOG = 1000.0 * 9.81
    REAL, DIMENSION(:,:), ALLOCATABLE :: CHZ, DP, VEGDRAG
    ALLOCATE( CHZ(IMAX,JMAX), DP(IMAX,JMAX), VEGDRAG(IMAX,JMAX) )

    ! CALCULATE WATER DEPTH
    DP = H + ETU

    ! CALCULATE CHEZY VALUES AND VEGETATION POROSITY
    CALL CHEZY( IMAX, JMAX, NFLCHZ, IACT, VARCHZ, DP, QXU, QYU, CHZ )
    CALL DRAG( IMAX, JMAX, IWET, IACT, IVEG, QXU, QYU, QXU, DP, &
&              NPOINTS, VEGDEN, HEIGHT, CD, EI, VOGEL, DPVAR, APVAR, WDVAR, &
&              VEGDRAG, VEGPOROS)

    ! CALCULATE BED SHEAR STRESS (SEE ENT722 NOTES SECTION 1, PAGE 44)
    DO J = 2, JMAX - 1
    DO I = 2, IMAX - 1
        IF( IACT(I,J) ) THEN

            TMP = VEGPOROS(I,J) * RHOG * SQRT(QXU(I,J)**2 + QYU(I,J)**2) / (DP(I,J)*CHZ(I,J))**2

            TAUX(I,J) = QXU(I,J) * TMP
            TAUY(I,J) = QYU(I,J) * TMP

            TAU = SQRT(TAUX(I,J)**2 + TAUY(I,J)**2)

            ! UPDATE MAX STRESS
            TAUMAX(I,J) = MERGE(TAU, TAUMAX(I,J), TAU .GT. TAUMAX(I,J))
        ELSE
            TAUX(I,J) = 0.0
            TAUY(I,J) = 0.0
        END IF
    END DO
    END DO

    DEALLOCATE( CHZ, DP )
end subroutine
