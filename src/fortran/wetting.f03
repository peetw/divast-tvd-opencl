subroutine wetting(IMAX, JMAX, DPMIN, IWET, IACT, ETU)

    implicit none

    INTEGER, INTENT(IN) :: IMAX, JMAX
    real, intent(in) :: dpmin
    logical(4), DIMENSION(IMAX,JMAX), INTENT(IN) :: IWET, IACT
    REAL, DIMENSION(IMAX,JMAX), INTENT(INOUT), TARGET :: ETU

    INTEGER :: I, J
    REAL, POINTER :: PTR

    ! WETTING JUDGEMENT
    DO J = 2, JMAX-1
        DO I = 2, IMAX-1
            IF( IWET(I,J) .AND. (.NOT.IACT(I,J)) )  THEN
                ! SEARCH FOR THE NEIGHBOURING WET CELL WITH HIGHEST WATER LEVEL
                PTR => ETU(I,J)
                IF( IACT(I+1,J) .AND. ETU(I+1,J).GT.PTR ) THEN
                    PTR => ETU(I+1,J)
                END IF
                IF( IACT(I-1,J) .AND. ETU(I-1,J).GT.PTR ) THEN
                    PTR => ETU(I-1,J)
                END IF
                IF( IACT(I,J+1) .AND. ETU(I,J+1).GT.PTR ) THEN
                    PTR => ETU(I,J+1)
                END IF
                IF( IACT(I,J-1) .AND. ETU(I,J-1).GT.PTR ) THEN
                    PTR => ETU(I,J-1)
                END IF

                IF( (PTR-ETU(I,J)) .GT. DPMIN*2.0 ) THEN
                    ETU(I,J) = ETU(I,J) + DPMIN
                    PTR = PTR - DPMIN
                END IF
            END IF
        END DO
    END DO
end subroutine
