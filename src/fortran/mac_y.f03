subroutine mac_pred_y(IMAX, JMAX, NFLCHZ, NFLEDV, DT, DX, DTDX, DTDXSQ, HDTDXG, &
&                     DTDXBETA, DTG, DTCORI, EDCOEF, IWET, IACT, IVEG, NPOINTS, &
&                     VEGDEN, HEIGHT, CD, EI, VOGEL, DPVAR, APVAR, WDVAR, &
&                     VARCHZ, DP, ETL, QXL, QYL, VEGPOROS, ETM, QXM, QYM)

    use advection
    use vegetation
    use roughness
    use turbulence

    implicit none

    INTEGER, INTENT(IN) :: IMAX, JMAX, NFLCHZ, NFLEDV
    REAL, INTENT(IN) :: DT, DX, DTDX, DTDXSQ, HDTDXG, DTDXBETA, DTG, DTCORI, EDCOEF
    LOGICAL(4), DIMENSION(IMAX,JMAX), INTENT(IN) :: IWET, IACT
    integer, DIMENSION(IMAX,JMAX), INTENT(IN) :: IVEG
    integer, DIMENSION(2), INTENT(IN) :: NPOINTS
    real, dimension(2), intent(in) :: VEGDEN, HEIGHT, CD, EI, VOGEL
    REAL, DIMENSION(11,2), intent(in) :: DPVAR, APVAR, WDVAR
    REAL, DIMENSION(IMAX,JMAX), INTENT(IN) :: VARCHZ, DP, ETL, QXL, QYL
    REAL, DIMENSION(IMAX,JMAX), INTENT(OUT) :: VEGPOROS, ETM, QXM, QYM

    INTEGER :: I, J, JM1, JP1
    REAL :: TTMP, QXTMP, QYTMP
    REAL, DIMENSION(:,:), ALLOCATABLE :: FX, FY, VEGDRAG, CHZ, EDV
    ALLOCATE(FX(IMAX,JMAX), FY(IMAX,JMAX), VEGDRAG(IMAX,JMAX), CHZ(IMAX,JMAX), &
&            EDV(IMAX,JMAX))

    CALL GUGV( IMAX, JMAX, IACT, QXL, QYL, DP, FX, FY )
    CALL DRAG( IMAX, JMAX, IWET, IACT, IVEG, QXL, QYL, QYL, DP, &
&              NPOINTS, VEGDEN, HEIGHT, CD, EI, VOGEL, DPVAR, APVAR, WDVAR, &
&              VEGDRAG, VEGPOROS)
    CALL CHEZY( IMAX, JMAX, NFLCHZ, IACT, VARCHZ, DP, QXL, QYL, CHZ )
    CALL EDDY( IMAX, JMAX, DX, NFLEDV, IACT, EDCOEF, QXL, QYL, CHZ, DP, EDV )

    DO J = 2, JMAX - 1
        JM1 = J - 1
        JP1 = J + 1
        DO I = 2, IMAX - 1
            IF( IWET(I,J) ) THEN
                ! CONTINUITY EQUATION - Y DIRECTION
                ETM(I,J) = ETL(I,J) + (-DTDX*(QYL(I,JP1)-QYL(I,J))) / VEGPOROS(I,J)

                ! ACTIVE POINTS
                IF( IACT(I,J) ) THEN
                    IF( IACT(I,JP1) .AND. IACT(I,JM1) )  THEN
                        ! ADVECTIVE ACCELERATION - Y DIRECTION
                        QYTMP = QYL(I,J) - DTDXBETA*(FY(I,JP1)-FY(I,J))

                        ! CORIOLIS FORCE - Y DIRECTION
                        QYTMP = QYTMP - DTCORI*QXL(I,J)

                        ! VEGETATIVE DRAG FORCE - Y DIRECTION
                        QYTMP = QYTMP - DT*VEGDRAG(I,J)

                        ! PRESSURE GRADIENT (WATER SLOPE) - Y DIRECTION
                        QYTMP = QYTMP - HDTDXG*(DP(I,J)+DP(I,JP1))*(ETL(I,JP1)-ETL(I,J))

                        ! BED FRICTION - Y DIRECTION
                        TTMP = DTG * SQRT(QXL(I,J)**2 + QYL(I,J)**2) / (DP(I,J) * CHZ(I,J))**2

                        IF(TTMP .LT. 0.3) THEN
                            QYTMP = QYTMP - VEGPOROS(I,J)*TTMP*QYL(I,J) ! EXPLICIT SCHEME FOR BED FRICTION TERM
                        ELSE
                            QYTMP = QYTMP / (1.0 + VEGPOROS(I,J)*TTMP)  ! SEMI-IMPLICIT SCHEME FOR BED FRICTION TERM
                        END IF

                        ! TURBULENCE - Y DIRECTION
                        QYM(I,J) = QYTMP + 2.0 * DTDXSQ * EDV(I,J) * (QYL(I,JP1)-2.0*QYL(I,J)+QYL(I,JM1))
                    ELSE
                        QYM(I,J) = 0.0
                    END IF

                    ! ADVECTIVE ACCELERATION - X DIRECTION
                    QXTMP = QXL(I,J) - DTDXBETA * (FX(I,JP1)-FX(I,J))

                    ! TURBULENCE - X DIRECTION
                    QXM(I,J) = QXTMP + DTDXSQ * EDV(I,J) * (QXL(I,JP1)-2.0*QXL(I,J)+QXL(I,JM1))
                ELSE
                    ! INACTIVE POINTS
                    QXM(I,J) = QXL(I,J)
                    QYM(I,J) = QYL(I,J)
                END IF
            END IF
        END DO
    END DO
end subroutine


subroutine mac_corr_y(IMAX, JMAX, NFLCHZ, NFLEDV, DT, DX, DTDX, DTDXSQ, HDTDXG, &
&                     DTDXBETA, DTG, DTCORI, EDCOEF, IWET, IACT, IVEG, NPOINTS, &
&                     VEGDEN, HEIGHT, CD, EI, VOGEL, DPVAR, APVAR, WDVAR, &
&                     VARCHZ, DP, ETL, QXL, QYL, ETM, QXM, QYM, &
&                     VEGPOROS, ETU, QXU, QYU)

    use advection
    use vegetation
    use roughness
    use turbulence

    implicit none

    INTEGER, INTENT(IN) :: IMAX, JMAX, NFLCHZ, NFLEDV
    REAL, INTENT(IN) :: DT, DX, DTDX, DTDXSQ, HDTDXG, DTDXBETA, DTG, DTCORI, EDCOEF
    LOGICAL(4), DIMENSION(IMAX,JMAX), INTENT(IN) :: IWET, IACT
    integer, DIMENSION(IMAX,JMAX), INTENT(IN) :: IVEG
    integer, DIMENSION(2), INTENT(IN) :: NPOINTS
    real, dimension(2), intent(in) :: VEGDEN, HEIGHT, CD, EI, VOGEL
    REAL, DIMENSION(11,2), intent(in) :: DPVAR, APVAR, WDVAR
    REAL, DIMENSION(IMAX,JMAX), INTENT(IN) :: VARCHZ, DP, ETL, QXL, QYL, ETM, QXM, QYM
    REAL, DIMENSION(IMAX,JMAX), INTENT(OUT) :: VEGPOROS, ETU, QXU, QYU

    INTEGER :: I, J, JM1, JP1
    REAL :: TTMP, QXTMP, QYTMP, ETTMP
    REAL, DIMENSION(:,:), ALLOCATABLE :: FX, FY, VEGDRAG, CHZ, EDV
    ALLOCATE(FX(IMAX,JMAX), FY(IMAX,JMAX), VEGDRAG(IMAX,JMAX), CHZ(IMAX,JMAX), &
&            EDV(IMAX,JMAX))

    CALL GUGV( IMAX, JMAX, IACT, QXM, QYM, DP, FX, FY )
    CALL DRAG( IMAX, JMAX, IWET, IACT, IVEG, QXM, QYM, QYM, DP, &
&              NPOINTS, VEGDEN, HEIGHT, CD, EI, VOGEL, DPVAR, APVAR, WDVAR, &
&              VEGDRAG, VEGPOROS)
    CALL CHEZY( IMAX, JMAX, NFLCHZ, IACT, VARCHZ, DP, QXM, QYM, CHZ )
    CALL EDDY( IMAX, JMAX, DX, NFLEDV, IACT, EDCOEF, QXM, QYM, CHZ, DP, EDV )

    DO J = 2, JMAX - 1
        JM1 = J - 1
        JP1 = J + 1
        DO I = 2, IMAX - 1
            IF( IWET(I,J) ) THEN
                ! CONTINUITY EQUATION - Y DIRECTION
                ETTMP = ETL(I,J) + (-DTDX*(QYM(I,J)-QYM(I,JM1))) / VEGPOROS(I,J)

                ! ACTIVE POINTS
                IF( IACT(I,J) ) THEN
                    IF( IACT(I,JM1) .AND. IACT(I,JP1) )  THEN
                        ! ADVECTIVE ACCELERATION - Y DIRECTION
                        QYTMP = QYL(I,J) - DTDXBETA*(FY(I,J)-FY(I,JM1))

                        ! CORIOLIS FORCE - Y DIRECTION
                        QYTMP = QYTMP - DTCORI*QXM(I,J)

                        ! VEGETATIVE DRAG FORCE - Y DIRECTION
                        QYTMP = QYTMP - DT*VEGDRAG(I,J)

                        ! PRESSURE GRADIENT (WATER SLOPE) - Y DIRECTION
                        QYTMP = QYTMP - HDTDXG*(DP(I,J)+DP(I,JM1))*(ETM(I,J)-ETM(I,JM1))

                        ! BED FRICTION - Y DIRECTION
                        TTMP = DTG * SQRT(QXM(I,J)**2 + QYM(I,J)**2) / (DP(I,J) * CHZ(I,J))**2

                        IF(TTMP .LT. 0.3) THEN
                            QYTMP = QYTMP - VEGPOROS(I,J)*TTMP*QYM(I,J) ! EXPLICIT SCHEME FOR BED FRICTION TERM
                        ELSE
                            QYTMP = QYTMP / (1.0 + VEGPOROS(I,J)*TTMP)  ! SEMI-IMPLICIT SCHEME FOR BED FRICTION TERM
                        END IF

                        ! TURBULENCE - Y DIRECTION
                        QYTMP = QYTMP + 2.0 * DTDXSQ * EDV(I,J) * (QYM(I,JP1)-2.0*QYM(I,J)+QYM(I,JM1))
                    ELSE
                        QYTMP = 0.0
                    END IF

                    ! ADVECTIVE ACCELERATION - X DIRECTION
                    QXTMP = QXL(I,J) - DTDXBETA * (FX(I,J)-FX(I,JM1))

                    ! TURBULENCE - X DIRECTION
                    QXTMP = QXTMP + DTDXSQ * EDV(I,J) * (QXM(I,JP1)-2.0*QXM(I,J)+QXM(I,JM1))

                    ! FINAL VALUE = 1/2 * (PREDICTOR + CORRECTOR)
                    ETU(I,J) = 0.5 * ( ETM(I,J) + ETTMP )
                    QXU(I,J) = 0.5 * ( QXM(I,J) + QXTMP )
                    QYU(I,J) = 0.5 * ( QYM(I,J) + QYTMP )
                ELSE
                    ! INACTIVE POINTS
                    ETU(I,J) = 0.5 * ( ETM(I,J) + ETTMP )
                    QXU(I,J) = 0.5 * ( QXM(I,J) + QXL(I,J) )
                    QYU(I,J) = 0.5 * ( QYM(I,J) + QYL(I,J) )
                END IF
            END IF
        END DO
    END DO
end subroutine
