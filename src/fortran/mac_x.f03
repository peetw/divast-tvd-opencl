subroutine mac_pred_x(IMAX, JMAX, NFLCHZ, NFLEDV, DT, DX, DTDX, DTDXSQ, HDTDXG, &
&                     DTDXBETA, DTG, DTCORI, EDCOEF, IWET, IACT, IVEG, NPOINTS, &
&                     VEGDEN, HEIGHT, CD, EI, VOGEL, DPVAR, APVAR, WDVAR, &
&                     VARCHZ, DP, ETL, QXL, QYL, VEGPOROS, ETM, QXM, QYM)

    use advection
    use vegetation
    use roughness
    use turbulence

    implicit none

    INTEGER, INTENT(IN) :: IMAX, JMAX, NFLCHZ, NFLEDV
    REAL, INTENT(IN) :: DT, DX, DTDX, DTDXSQ, HDTDXG, DTDXBETA, DTG, DTCORI, EDCOEF
    LOGICAL(4), DIMENSION(IMAX,JMAX), INTENT(IN) :: IWET, IACT
    integer, DIMENSION(IMAX,JMAX), INTENT(IN) :: IVEG
    integer, DIMENSION(2), INTENT(IN) :: NPOINTS
    real, dimension(2), intent(in) :: VEGDEN, HEIGHT, CD, EI, VOGEL
    REAL, DIMENSION(11,2), intent(in) :: DPVAR, APVAR, WDVAR
    REAL, DIMENSION(IMAX,JMAX), INTENT(IN) :: VARCHZ, DP, ETL, QXL, QYL
    REAL, DIMENSION(IMAX,JMAX), INTENT(OUT) :: VEGPOROS, ETM, QXM, QYM

    INTEGER :: I, J, IM1, IP1
    REAL :: TTMP, QXTMP, QYTMP
    REAL, DIMENSION(:,:), ALLOCATABLE :: FX, FY, VEGDRAG, CHZ, EDV
    ALLOCATE(FX(IMAX,JMAX), FY(IMAX,JMAX), VEGDRAG(IMAX,JMAX), CHZ(IMAX,JMAX), &
&            EDV(IMAX,JMAX))

    CALL FUFV( IMAX, JMAX, IACT, QXL, QYL, DP, FX, FY )
    CALL DRAG( IMAX, JMAX, IWET, IACT, IVEG, QXL, QYL, QXL, DP, &
&              NPOINTS, VEGDEN, HEIGHT, CD, EI, VOGEL, DPVAR, APVAR, WDVAR, &
&              VEGDRAG, VEGPOROS)
    CALL CHEZY( IMAX, JMAX, NFLCHZ, IACT, VARCHZ, DP, QXL, QYL, CHZ )
    CALL EDDY( IMAX, JMAX, DX, NFLEDV, IACT, EDCOEF, QXL, QYL, CHZ, DP, EDV )

    DO J = 2, JMAX - 1
        DO I = 2, IMAX - 1
            IF( IWET(I,J) ) THEN
                ! CONTINUITY EQUATION - X DIRECTION
                IP1 = I + 1
                ETM(I,J) = ETL(I,J) + (-DTDX*(QXL(IP1,J)-QXL(I,J))) / VEGPOROS(I,J)

                ! ACTIVE POINTS
                IF( IACT(I,J) ) THEN
                    IM1 = I - 1
                    IF( IACT(IP1,J) .AND. IACT(IM1,J) )  THEN
                        ! ADVECTIVE ACCELERATION - X DIRECTION
                        QXTMP = QXL(I,J) - DTDXBETA*(FX(IP1,J)-FX(I,J))

                        ! CORIOLIS FORCE - X DIRECTION
                        QXTMP = QXTMP + DTCORI*QYL(I,J)

                        ! VEGATATIVE DRAG FORCE - X DIRECTION
                        QXTMP = QXTMP - DT*VEGDRAG(I,J)

                        ! PRESSURE GRADIENT (WATER SLOPE) - X DIRECTION
                        QXTMP = QXTMP - HDTDXG*(DP(I,J)+DP(IP1,J))*(ETL(IP1,J)-ETL(I,J))

                        ! BED FRICTION - X DIRECTION
                        TTMP = DTG * SQRT(QXL(I,J)**2 + QYL(I,J)**2) / (DP(I,J) * CHZ(I,J))**2

                        IF(TTMP .LT. 0.3) THEN
                            QXTMP = QXTMP - VEGPOROS(I,J)*TTMP*QXL(I,J) ! EXPLICIT SCHEME FOR BED FRICTION TERM
                        ELSE
                            QXTMP = QXTMP / (1.0 + VEGPOROS(I,J)*TTMP)  ! SEMI-IMPLICIT SCHEME FOR BED FRICTION TERM
                        END IF

                        ! TURBULENCE - X DIRECTION
                        QXM(I,J) = QXTMP + 2.0 * DTDXSQ * EDV(I,J) * (QXL(IP1,J)-2.0*QXL(I,J)+QXL(IM1,J))
                    ELSE
                        QXM(I,J) = 0.0
                    END IF

                    ! ADVECTIVE ACCELERATION - Y DIRECTION
                    QYTMP = QYL(I,J) - DTDXBETA * (FY(IP1,J)-FY(I,J))

                    ! TURBULENCE - Y DIRECTION
                    QYM(I,J) = QYTMP + DTDXSQ * EDV(I,J) * (QYL(IP1,J)-2.0*QYL(I,J)+QYL(IM1,J))
                ELSE
                    ! INACTIVE POINTS
                    QXM(I,J) = QXL(I,J)
                    QYM(I,J) = QYL(I,J)
                END IF
            END IF
        END DO
    END DO
end subroutine


subroutine mac_corr_x(IMAX, JMAX, NFLCHZ, NFLEDV, DT, DX, DTDX, DTDXSQ, HDTDXG, &
&                     DTDXBETA, DTG, DTCORI, EDCOEF, IWET, IACT, IVEG, NPOINTS, &
&                     VEGDEN, HEIGHT, CD, EI, VOGEL, DPVAR, APVAR, WDVAR, &
&                     VARCHZ, DP, ETL, QXL, QYL, ETM, QXM, QYM, &
&                     VEGPOROS, ETU, QXU, QYU)

    use advection
    use vegetation
    use roughness
    use turbulence

    implicit none

    INTEGER, INTENT(IN) :: IMAX, JMAX, NFLCHZ, NFLEDV
    REAL, INTENT(IN) :: DT, DX, DTDX, DTDXSQ, HDTDXG, DTDXBETA, DTG, DTCORI, EDCOEF
    LOGICAL(4), DIMENSION(IMAX,JMAX), INTENT(IN) :: IWET, IACT
    integer, DIMENSION(IMAX,JMAX), INTENT(IN) :: IVEG
    integer, DIMENSION(2), INTENT(IN) :: NPOINTS
    real, dimension(2), intent(in) :: VEGDEN, HEIGHT, CD, EI, VOGEL
    REAL, DIMENSION(11,2), intent(in) :: DPVAR, APVAR, WDVAR
    REAL, DIMENSION(IMAX,JMAX), INTENT(IN) :: VARCHZ, DP, ETL, QXL, QYL, ETM, QXM, QYM
    REAL, DIMENSION(IMAX,JMAX), INTENT(OUT) :: VEGPOROS, ETU, QXU, QYU

    INTEGER :: I, J, IM1, IP1
    REAL :: TTMP, QXTMP, QYTMP, ETTMP
    REAL, DIMENSION(:,:), ALLOCATABLE :: FX, FY, VEGDRAG, CHZ, EDV
    ALLOCATE(FX(IMAX,JMAX), FY(IMAX,JMAX), VEGDRAG(IMAX,JMAX), CHZ(IMAX,JMAX), &
&            EDV(IMAX,JMAX))

    CALL FUFV( IMAX, JMAX, IACT, QXM, QYM, DP, FX, FY )
    CALL DRAG( IMAX, JMAX, IWET, IACT, IVEG, QXM, QYM, QXM, DP, &
&              NPOINTS, VEGDEN, HEIGHT, CD, EI, VOGEL, DPVAR, APVAR, WDVAR, &
&              VEGDRAG, VEGPOROS)
    CALL CHEZY( IMAX, JMAX, NFLCHZ, IACT, VARCHZ, DP, QXM, QYM, CHZ )
    CALL EDDY( IMAX, JMAX, DX, NFLEDV, IACT, EDCOEF, QXM, QYM, CHZ, DP, EDV )

    DO J = 2, JMAX - 1
        DO I = 2, IMAX - 1
            IF( IWET(I,J) ) THEN
                ! CONTINUITY EQUATION - X DIRECTION
                IM1 = I - 1
                ETTMP = ETL(I,J) + (-DTDX*(QXM(I,J)-QXM(IM1,J))) / VEGPOROS(I,J)

                ! ACTIVE POINTS
                IF( IACT(I,J) ) THEN
                    IP1 = I + 1
                    IF( IACT(IM1,J) .AND. IACT(IP1,J) )  THEN
                        ! ADVECTIVE ACCELERATION - X DIRECTION
                        QXTMP = QXL(I,J) - DTDXBETA*(FX(I,J)-FX(IM1,J))

                        ! CORIOLIS FORCE - X DIRECTION
                        QXTMP = QXTMP + DTCORI*QYM(I,J)

                        ! VEGETATIVE DRAG FORCE - X DIRECTION
                        QXTMP = QXTMP - DT*VEGDRAG(I,J)

                        ! PRESSURE GRADIENT (WATER SLOPE) - X DIRECTION
                        QXTMP = QXTMP - HDTDXG*(DP(I,J)+DP(IM1,J))*(ETM(I,J)-ETM(IM1,J))

                        ! BED FRICTION - X DIRECTION
                        TTMP = DTG * SQRT(QXM(I,J)**2 + QYM(I,J)**2) / (DP(I,J) * CHZ(I,J))**2

                        IF(TTMP .LT. 0.3) THEN
                            ! EXPLICIT SCHEME FOR BED FRICTION TERM
                            QXTMP = QXTMP - VEGPOROS(I,J)*TTMP*QXM(I,J)
                        ELSE
                            ! SEMI-IMPLICIT SCHEME FOR BED FRICTION TERM
                            QXTMP = QXTMP / (1.0 + VEGPOROS(I,J)*TTMP)
                        END IF

                        ! TURBULENCE - X DIRECTION
                        QXTMP = QXTMP + 2.0 * DTDXSQ * EDV(I,J) * (QXM(IP1,J)-2.0*QXM(I,J)+QXM(IM1,J))
                    ELSE
                        QXTMP = 0.0
                    END IF

                    ! ADVECTIVE ACCELERATION - Y DIRECTION
                    QYTMP = QYL(I,J) - DTDXBETA * (FY(I,J)-FY(IM1,J))

                    ! TURBULENCE - Y DIRECTION
                    QYTMP = QYTMP + DTDXSQ * EDV(I,J) * (QYM(IP1,J)-2.0*QYM(I,J)+QYM(IM1,J))

                    ! FINAL VALUE = 1/2 * (PREDICTOR + CORRECTOR)
                    ETU(I,J) = 0.5 * ( ETM(I,J) + ETTMP )
                    QXU(I,J) = 0.5 * ( QXM(I,J) + QXTMP )
                    QYU(I,J) = 0.5 * ( QYM(I,J) + QYTMP )
                ELSE
                    ! INACTIVE POINTS
                    ETU(I,J) = 0.5 * ( ETM(I,J) + ETTMP )
                    QXU(I,J) = 0.5 * ( QXM(I,J) + QXL(I,J) )
                    QYU(I,J) = 0.5 * ( QYM(I,J) + QYL(I,J) )
                END IF
            END IF
        END DO
    END DO
end subroutine
