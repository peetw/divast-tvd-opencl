MODULE roughness

    IMPLICIT NONE

CONTAINS

    ! CALCULATE BED ROUGHNESS VALUES
    SUBROUTINE CHEZY(IMAX, JMAX, NFLCHZ, IACT, VARCHZ, DP, QX, QY, CHZ)

        IMPLICIT NONE

        ! INPUT VARIABLES
        INTEGER, INTENT(IN) :: IMAX, JMAX, NFLCHZ
        LOGICAL(4), DIMENSION(IMAX,JMAX), INTENT(IN) :: IACT
        REAL, DIMENSION(IMAX,JMAX), INTENT(IN) :: VARCHZ, DP, QX, QY
        REAL, DIMENSION(IMAX,JMAX), INTENT(OUT) :: CHZ

        ! LOCAL VARIABLES
        INTEGER :: I, J
        REAL :: RE, TTMP, CHZTMP, CHZOLD
        REAL, PARAMETER :: CPARAM = 2.0 * SQRT(8.0 * 9.81)

        ! =========================================
        ! See pages 44-45 of ENT722 Section 1 notes
        ! =========================================

        ! NFLCHZ -1 due to ROUGHNESS_SCHEME enum starting from zero

        IF( NFLCHZ .EQ. 1-1 ) THEN
            ! CONSTANT VALUE
            DO J = 2, JMAX - 1
            DO I = 2, IMAX - 1
                IF( IACT(I,J) ) THEN
                    CHZ(I,J) = VARCHZ(I,J)     ! C has units m^(1/2)/s
                END IF
            END DO
            END DO
        ELSE IF( NFLCHZ .EQ. 2-1 ) THEN
            ! MANNING FORMULA
            DO J = 2, JMAX - 1
            DO I = 2, IMAX - 1
                IF( IACT(I,J) ) THEN
                    CHZ(I,J) = DP(I,J)**0.166667 / VARCHZ(I,J) ! VARCHZ = Manning's n
                END IF
            END DO
            END DO
        ELSE IF( NFLCHZ .EQ. 3-1 ) THEN
            ! COLEBROOK-WHITE FORMULA (Transitional flow, i.e. Re ~ 500-2000)
            DO J = 2, JMAX - 1
            DO I = 2, IMAX - 1
                IF( IACT(I,J) ) THEN
                    ! Re = U*L/v = U*Dh/v = (U*4A/P)/v = (U*4*dx*DP/dx)/v = 4q/v
                    RE = 4.0 * SQRT(QX(I,J)**2 + QY(I,J)**2) / 1.30E-6
                    RE = MERGE(500.0, RE, RE .LT. 500.0)

                    TTMP = VARCHZ(I,J) / (12.0*DP(I,J))                     ! VARCHZ = k = roughness height
                    CHZOLD = -CPARAM * LOG10(TTMP)                          ! C = -18*log10(k/12DP)
                    CHZTMP = -CPARAM * LOG10(TTMP + 5.0*CHZOLD/(RE*CPARAM)) ! C = -18*log10(k/12DP + 5C/18Re)

                    DO WHILE( ABS(CHZTMP-CHZOLD) .GT. 0.5 )
                        CHZOLD = CHZTMP
                        CHZTMP = -CPARAM * LOG10(TTMP + 5.0*CHZOLD/(RE*CPARAM))
                    END DO
                    CHZ(I,J) = CHZTMP
                END IF
            END DO
            END DO
        ! NOTE: Chezy is always greater when using NFL4 vs NFL3
        ELSE IF( NFLCHZ .EQ. 4-1 ) THEN
            ! COLEBROOK-WHITE FORMULA (Fully rough flow i.e. Re >> 1000, section 2.9 DIVAST user man
            DO J = 2, JMAX - 1
            DO I = 2, IMAX - 1
                IF( IACT(I,J) ) THEN  ! varchz = k = roughness height
                    CHZ(I,J) = -CPARAM * LOG10(VARCHZ(I,J) / (12.0*DP(I,J)))  ! C = -18*log10(k/12DP)
                    ! IMPORTANT: DP > k/12 otherwise C will be negative (applies to NFL3 as well)
                END IF
            END DO
            END DO
        END IF
    END SUBROUTINE CHEZY
END MODULE
