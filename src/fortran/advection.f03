MODULE ADVECTION

    IMPLICIT NONE

CONTAINS

    ! ------------------------------------------------------------------------------
    !   CALCULATE X-DIRECTION ADVECTION TERMS
    ! ------------------------------------------------------------------------------
    SUBROUTINE FUFV( IMAX, JMAX, IACT, QX, QY, DP, FX, FY )

        IMPLICIT NONE

        ! INPUT VARIABLES
        INTEGER, INTENT(IN) :: IMAX, JMAX
        LOGICAL(4), DIMENSION(IMAX,JMAX), INTENT(IN) :: IACT
        REAL, DIMENSION(IMAX,JMAX), INTENT(IN) :: QX, QY, DP
        REAL, DIMENSION(IMAX,JMAX), INTENT(OUT) :: FX, FY

        ! LOCAL VARIABLES
        INTEGER :: I, J
        REAL :: U

        ! CALCULATE ADVECTION TERMS
        DO J = 2, JMAX-1
            DO I = 1, IMAX
                IF( IACT(I,J) ) THEN
                    U = QX(I,J) / DP(I,J)
                    FX(I,J) = QX(I,J) * U
                    FY(I,J) = QY(I,J) * U
                ELSE
                    FX(I,J) = 0.0
                    FY(I,J) = 0.0
                END IF
            END DO
        END DO

    END SUBROUTINE FUFV


    ! ------------------------------------------------------------------------------
    !   CALCULATE Y-DIRECTION ADVECTION TERMS
    ! ------------------------------------------------------------------------------
    SUBROUTINE GUGV( IMAX, JMAX, IACT, QX, QY, DP, FX, FY )

        IMPLICIT NONE

        ! INPUT VARIABLES
        INTEGER, INTENT(IN) :: IMAX, JMAX
        LOGICAL(4), DIMENSION(IMAX,JMAX), INTENT(IN) :: IACT
        REAL, DIMENSION(IMAX,JMAX), INTENT(IN) :: QX, QY, DP
        REAL, DIMENSION(IMAX,JMAX), INTENT(OUT) :: FX, FY

        ! LOCAL VARIABLES
        INTEGER :: I, J
        REAL :: V

        ! CALCULATE ADVECTION TERMS
        DO J = 1, JMAX
            DO I = 2, IMAX-1
                IF( IACT(I,J) ) THEN
                    V = QY(I,J) / DP(I,J)
                    FX(I,J) = QX(I,J) * V
                    FY(I,J) = QY(I,J) * V
                ELSE
                    FX(I,J) = 0.0
                    FY(I,J) = 0.0
                END IF
            END DO
        END DO

    END SUBROUTINE GUGV

END MODULE ADVECTION
