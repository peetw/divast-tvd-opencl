subroutine tvd_y(IMAX, JMAX, DTDX, IACT, H, ETL, QXL, QYL)

    IMPLICIT NONE

    INTEGER, INTENT(IN) :: IMAX, JMAX
    REAL, INTENT(IN) :: DTDX
    LOGICAL(4), DIMENSION(IMAX,JMAX),INTENT(IN) :: IACT
    REAL, DIMENSION(IMAX,JMAX), INTENT(IN) :: H
    REAL, DIMENSION(IMAX,JMAX), INTENT(INOUT) :: ETL, QXL, QYL

    ! LOCAL VARIABLES
    INTEGER :: I, J, JM1, JP1
    REAL :: RPLUS, RMINUS, TTMP, DPTMP, CR
    REAL, DIMENSION(:,:), ALLOCATABLE :: DETM1, DQXM1, DQYM1, GPLUS, GMINUS
    ALLOCATE( DETM1(IMAX,JMAX),DQXM1(IMAX,JMAX),DQYM1(IMAX,JMAX),GPLUS(IMAX,JMAX),GMINUS(IMAX,JMAX) )

    ! LOCAL INCREMENTS OF THE UNKNOWN VARIABLES
    DO J = 2, JMAX
        JM1 = J - 1
        DO I = 2, IMAX - 1
            IF( IACT(I,J) .AND. IACT(I,JM1) )  THEN
                DETM1(I,J) = ETL(I,J) - ETL(I,JM1)
                DQXM1(I,J) = QXL(I,J) - QXL(I,JM1)
                DQYM1(I,J) = QYL(I,J) - QYL(I,JM1)
            END IF
        END DO
    END DO

    ! THE SUCCESSIVE GRADIENT RATIO AND G
    DO J = 1, JMAX
        JM1 = J - 1
        JP1 = J + 1
        DO I = 2, IMAX - 1
            IF( IACT(I,J) ) THEN
                IF( J.EQ.1 .OR. J.EQ.JMAX .OR. .NOT.IACT(I,JM1) .OR. .NOT.IACT(I,JP1) ) THEN
                    GMINUS(I,J) = 0.0
                    GPLUS(I,J) = 0.0
                ELSE
                    ! NUMERATOR FOR RPLUS & RMINUS CALCULATIONS
                    TTMP = DETM1(I,J)*DETM1(I,JP1) + DQXM1(I,J)*DQXM1(I,JP1) + DQYM1(I,J)*DQYM1(I,JP1)

                    ! LOCAL COURANT NUMBER
                    DPTMP = H(I,J) + ETL(I,J)
                    CR = DTDX * (ABS(QYL(I,J)/DPTMP) + SQRT(9.81*DPTMP))
                    IF(CR .LT. 0.5) THEN
                        CR = 0.5 * CR * (1.0-CR)
                    ELSE
                        CR = 0.125
                    END IF

                    ! GMINUS
                    IF( J .EQ. 2 )  THEN
                        GMINUS(I,J) = 0.0
                    ELSE
                        RMINUS = TTMP / (1.0E-15 + DETM1(I,J)**2 + DQXM1(I,J)**2 + DQYM1(I,J)**2)
                        GMINUS(I,J) = CR * (1.0 - MAX(0.0, MIN(2.0*RMINUS, 1.0)))
                    END IF

                    ! GPLUS
                    IF( J .EQ. JMAX-1 )  THEN
                        GPLUS(I,J) = 0.0
                    ELSE
                        RPLUS = TTMP / (1.0E-15 + DETM1(I,JP1)**2 + DQXM1(I,JP1)**2 + DQYM1(I,JP1)**2)
                        GPLUS(I,J) = CR * (1.0 - MAX(0.0, MIN(2.0*RPLUS, 1.0)))
                    END IF
                END IF
            END IF
        END DO
    END DO

    ! EXTRA DIFFUSION FLUX
    DO J = 2, JMAX
        JM1 = J - 1
        DO I = 2, IMAX - 1
            IF( IACT(I,JM1) .AND. IACT(I,J) ) THEN
                TTMP = GPLUS(I,JM1) + GMINUS(I,J)
                ETL(I,J) = TTMP * DETM1(I,J)
                QXL(I,J) = TTMP * DQXM1(I,J)
                QYL(I,J) = TTMP * DQYM1(I,J)
            ELSE
                ETL(I,J) = 0.0
                QXL(I,J) = 0.0
                QYL(I,J) = 0.0
            END IF
        END DO
    END DO

    DEALLOCATE( DETM1, DQXM1, DQYM1, GPLUS, GMINUS )
end subroutine




subroutine tvd_post_y(IMAX, JMAX, IWET, IACT, ETL, QXL, QYL, ETU, QXU, QYU)

    implicit none

    INTEGER, INTENT(IN) :: IMAX, JMAX
    LOGICAL(4), DIMENSION(IMAX,JMAX), INTENT(IN) :: IWET, IACT
    REAL, DIMENSION(IMAX,JMAX), INTENT(IN) :: ETL, QXL, QYL
    REAL, DIMENSION(IMAX,JMAX), INTENT(INOUT) :: ETU, QXU, QYU

    INTEGER :: I, J, JP1

    DO J = 2, JMAX - 1
        DO I = 2, IMAX - 1
            IF( IWET(I,J) .AND. IACT(I,J) ) THEN
                JP1 = J + 1
                ETU(I,J) = ETU(I,J) + ETL(I,JP1) - ETL(I,J)
                QXU(I,J) = QXU(I,J) + QXL(I,JP1) - QXL(I,J)
                QYU(I,J) = QYU(I,J) + QYL(I,JP1) - QYL(I,J)
            END IF
        END DO
    END DO
end subroutine
