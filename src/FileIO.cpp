// Class header
#include "FileIO.h"

// System headers
#include <algorithm>
#include <cmath>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

// Third-party headers
#include <CL/cl.hpp>
#include <libconfig.h++>

// Project headers
#include "types.h"
#include "utils.h"

FileIO::FileIO(const std::string &filename)
{
    try {
        // Parse main project file
        cfg = new libconfig::Config;
        cfg->readFile(filename.c_str());
    } catch (const libconfig::FileIOException &fioex) {
        utils::error("I/O error while reading file: " + filename + "\n");
    } catch (const libconfig::ParseException &pex) {
        utils::error("Parse error at " + (std::string)pex.getFile() + ":"
                + std::to_string(pex.getLine()) + " - "
                + (std::string)pex.getError() + "\n");
    }

    // Get base project name
    size_t pos = filename.rfind(".");
    std::string basename = filename.substr(0, pos);
    pos = basename.find_last_of("/\\");
    basename = basename.substr(pos+1);

    // Open domain output file
    const std::string domain_file = "out/" + basename + "_UVE.dat";
    domainFile.open(domain_file.c_str());
    if (!domainFile.is_open()) {
        utils::error("Could not open domain output file: " + domain_file);
    }

    // Get monitoring output filename
    monitFilename = "out/" + basename + "_LST.dat";
}

FileIO::~FileIO()
{
    delete cfg;
}

void FileIO::printDetails()
{
    try {
        // Output project summary
        utils::printHeader("Project details");
        std::cout << "Name:        "
                << cfg->lookup("details.name").c_str() << "\n";
        std::cout << "Description: "
                << cfg->lookup("details.description").c_str() << "\n";
        std::cout << "Author:      "
                << cfg->lookup("details.author").c_str() << "\n";
        std::cout << "Email:       "
                << cfg->lookup("details.email").c_str() << "\n";
    } catch (const libconfig::SettingNotFoundException &nfex) {
        utils::error((std::string)nfex.getPath() + " not found");
    } catch (const libconfig::SettingTypeException &tyex) {
        utils::error((std::string)tyex.getPath()
                + " contains unexpected value type");
    }
}

void FileIO::readHardware(cl_device_type &deviceType, cl_uint &numCores,
        cl_uint &platformIdx)
{
    try {
        // Default values
        deviceType = CL_DEVICE_TYPE_DEFAULT;
        numCores = 0;
        platformIdx = 0;

        if (cfg->exists("open-cl")) {
            int tmp = cfg->lookup("open-cl.platform-idx");
            if (tmp >= 0) {
                platformIdx = tmp;
            } else {
                platformIdx = 0;
                utils::warning("open-cl.platform-idx must be >= 0, using 0");
            }

            const std::string str = cfg->lookup("open-cl.device");
            if (str == "Default") {
                deviceType = CL_DEVICE_TYPE_DEFAULT;
            } else if (str == "CPU") {
                deviceType = CL_DEVICE_TYPE_CPU;
            } else if (str == "GPU") {
                deviceType = CL_DEVICE_TYPE_GPU;
            } else {
                utils::warning("Invalid open-cl.device type, using Default");
                deviceType = CL_DEVICE_TYPE_DEFAULT;
            }

            tmp = cfg->lookup("open-cl.num-cores");
            if (tmp >= 0) {
                numCores = tmp;
            } else {
                numCores = 0;
                utils::warning("open-cl.num-cores must be >= 0, ignoring");
            }
        }
    } catch (const libconfig::SettingNotFoundException &nfex) {
        utils::error((std::string)nfex.getPath() + " not found");
    } catch (const libconfig::SettingTypeException &tyex) {
        utils::error((std::string)tyex.getPath()
                + " contains unexpected value type");
    }
}


void FileIO::readParams(Constants &constants, SOLUTION_SCHEME &solutionScheme,
        bool &alternateDirection, float &timeSim, float &dtOut,
        float &dtMin, float &dtMax, unsigned int &numRows,
        unsigned int &numCols, float &beta, float &cori,
        float &courantFactor)
{
    try {
        std::string str = cfg->lookup("control.solution-scheme");
        if (str == "MacCormack") {
            solutionScheme = MacCormack;
        } else if (str == "TvdMacCormack") {
            solutionScheme = TvdMacCormack;
        } else {
            utils::warning("Invalid control.solution-scheme, using MacCormack");
            solutionScheme = MacCormack;
        }

        alternateDirection = cfg->lookup("control.alternate-direction");

        str = cfg->lookup("control.bed-roughness-scheme").c_str();
        if (str == "Constant") {
            constants.roughnessScheme = Constant;
        } else if (str == "Manning") {
            constants.roughnessScheme = Manning;
        } else if (str == "CwRough") {
            constants.roughnessScheme = CwRough;
        } else if (str == "CwTrans") {
            constants.roughnessScheme = CwTrans;
        } else {
            utils::warning("Invalid control.bed-roughness-scheme, using Constant");
            constants.roughnessScheme = Constant;
        }

        str = cfg->lookup("control.eddy-viscosity-scheme").c_str();
        if (str == "None") {
            constants.eddyScheme = None;
        } else if (str == "Parabolic") {
            constants.eddyScheme = Parabolic;
        } else if (str == "MixingLength") {
            constants.eddyScheme = MixingLength;
        } else {
            utils::warning("Invalid control.eddy-viscosity-scheme, using None");
            constants.eddyScheme = None;
        }

        timeSim = cfg->lookup("control.simulation-length");
        if (timeSim <= 0) {
            utils::error("control.simulation-length must be positive");
        }

        dtOut = cfg->lookup("control.output-interval");
        if (dtOut <= 0) {
            utils::error("control.output-interval must be positive");
        }

        dtMin = cfg->lookup("control.time-step.min");
        if (dtMin <= 0) {
            utils::error("control.time-step.min must be positive");
        }

        dtMax = cfg->lookup("control.time-step.max");
        if (dtMax <= 0) {
            utils::error("control.time-step.max must be positive");
        }

        constants.dx = cfg->lookup("control.grid-cell-size");
        if (dtMax <= 0) {
            utils::error("control.grid-cell-size must be positive");
        }

        int tmp = cfg->lookup("control.num-rows");
        if (tmp >= 3) {
            numRows = tmp;
        } else {
            utils::error("control.num-rows must be >= 3");
        }

        tmp = cfg->lookup("control.num-cols");
        if (tmp >= 3) {
            numCols = tmp;
        } else {
            utils::error("control.num-cols must be >= 3");
        }

        constants.dpMin = cfg->lookup("constants.min-water-depth");
        if (constants.dpMin <= 0) {
            utils::error("constants.min-water-depth must be positive");
        }

        courantFactor = cfg->lookup("constants.courant-factor");
        if (courantFactor <= 0) {
            utils::error("constants.courant-factor must be positive");
        }

        beta = cfg->lookup("constants.momentum-correction");
        if (beta <= 0) {
            utils::error("constants.momentum-correction must be positive");
        }

        if (constants.eddyScheme == Parabolic) {
            constants.eddyCoef = sqrt(9.81) *
                    (float)cfg->lookup("constants.eddy-coefficient");
            if (constants.eddyCoef <= 0) {
                utils::error("constants.eddy-coefficient must be positive");
            }
        }

        const float latitude_ang = cfg->lookup("constants.latitude");
        if (latitude_ang < -90 || latitude_ang > 90) {
            utils::warning("constants.latitude is outside normal range");
        } else {
            const float latitude_rad = latitude_ang * (M_PI/180.0);
            cori = M_PI * sin(latitude_rad) / 21600.0;
        }
    } catch (const libconfig::SettingNotFoundException &nfex) {
        utils::error((std::string)nfex.getPath() + " not found");
    } catch (const libconfig::SettingTypeException &tyex) {
        utils::error((std::string)tyex.getPath()
                + " contains unexpected value type");
    }
}

void FileIO::readBoundaries(std::vector<cl_int> &bcOpen,
        std::vector<cl_uint> &bcWlTime, std::vector<cl_int> &bcWlFree,
        std::vector<cl_uint> &bcQxTime, std::vector<cl_uint> &bcQyTime,
        std::vector<cl_int> &bcQxyFree, std::vector<cl_float> &bcTsTimes,
        std::vector<cl_float> &bcTsValues, NumBC &numBC)
{
    // Initialize number of boundary conditions
    numBC.numBcWlTime = 0;
    numBC.numBcQxTime = 0;
    numBC.numBcQyTime = 0;
    numBC.numBcWlFree = 0;
    numBC.numBcQxyFree = 0;
    numBC.numBcOpen = 0;

    try {
        if (cfg->exists("boundary-conditions")) {
            const int num_rows = cfg->lookup("control.num-rows");
            const int num_cols = cfg->lookup("control.num-cols");
            const int num_bcs = cfg->lookup("boundary-conditions").getLength();
            unsigned int offset = 0;

            for (int i = 0; i < num_bcs; i++) {
                const std::string path = "boundary-conditions.["
                        + std::to_string(i) + "].";

                // Get boundary location
                int row_min = cfg->lookup(path+"row-min");
                if (row_min < 1 || row_min > num_rows) {
                    utils::error("Invalid value for " + path + "row-min");
                } else {
                    row_min--;
                }

                int row_max = cfg->lookup(path+"row-max");
                if (row_max < 1 || row_max > num_rows) {
                    utils::error("Invalid value for " + path + "row-max");
                }

                int col_min = cfg->lookup(path+"col-min");
                if (col_min < 1 || col_min > num_cols) {
                    utils::error("Invalid value for " + path + "col-min");
                } else {
                    col_min--;
                }

                int col_max = cfg->lookup(path+"col-max");
                if (col_max < 1 || col_max > num_cols) {
                    utils::error("Invalid value for " + path + "col-max");
                }

                // Get boundary type
                const std::string type = cfg->lookup(path+"type");

                // Get extra data depending on boundary type
                int row_ref;
                int col_ref;
                int sz_times = 0;
                int sz_values = 0;
                if (type == "Open" || type == "WlFree" || type == "QxyFree") {
                    row_ref = cfg->lookup(path+"row-ref");
                    if (row_ref < -1 || row_ref > 1) {
                        utils::error("Unusual value for " + path + "row-ref");
                    }

                    col_ref = cfg->lookup(path+"col-ref");
                    if (col_ref < -1 || col_ref > 1) {
                        utils::error("Unusual value for " + path + "col-ref");
                    }
                } else {
                    sz_times = cfg->lookup(path+"time-series.times").getLength();
                    sz_values = cfg->lookup(path+"time-series.values").getLength();

                    if (sz_times != sz_values || sz_times < 2) {
                        utils::error("Ivalid data lengths at: " + path
                                + "time-series");
                    }

                    std::vector<cl_float> times;
                    std::vector<cl_float> values;
                    for (int j = 0; j < sz_times; j++) {
                        const std::string path_time = path
                                + "time-series.times.[" + std::to_string(j)
                                + "]";
                        const std::string path_value = path
                                + "time-series.values.[" + std::to_string(j)
                                + "]";
                        const cl_float time = cfg->lookup(path_time);
                        const cl_float value = cfg->lookup(path_value);
                        if (j == 0 && time != 0.0) {
                            utils::error(path_time + " != 0.0");
                        } else if (j == sz_times-1
                                && time < (float)cfg->lookup("control.simulation-length")) {
                            utils::error(path_time
                                    + " < control.simulation-length");
                        }
                        bcTsTimes.push_back(time);
                        bcTsValues.push_back(value);
                    }
                }

                // Store data
                if (type == "Open") {
                    bcOpen.push_back(row_min);
                    bcOpen.push_back(row_max);
                    bcOpen.push_back(col_min);
                    bcOpen.push_back(col_max);
                    bcOpen.push_back(row_ref);
                    bcOpen.push_back(col_ref);
                    numBC.numBcOpen++;
                } else if (type == "WlTime") {
                    bcWlTime.push_back(row_min);
                    bcWlTime.push_back(row_max);
                    bcWlTime.push_back(col_min);
                    bcWlTime.push_back(col_max);
                    bcWlTime.push_back(offset);
                    bcWlTime.push_back(sz_times);
                    numBC.numBcWlTime++;
                } else if (type == "WlFree") {
                    bcWlFree.push_back(row_min);
                    bcWlFree.push_back(row_max);
                    bcWlFree.push_back(col_min);
                    bcWlFree.push_back(col_max);
                    bcWlFree.push_back(row_ref);
                    bcWlFree.push_back(col_ref);
                    numBC.numBcWlFree++;
                } else if (type == "QxTime") {
                    bcQxTime.push_back(row_min);
                    bcQxTime.push_back(row_max);
                    bcQxTime.push_back(col_min);
                    bcQxTime.push_back(col_max);
                    bcQxTime.push_back(offset);
                    bcQxTime.push_back(sz_times);
                    numBC.numBcQxTime++;
                } else if (type == "QyTime") {
                    bcQyTime.push_back(row_min);
                    bcQyTime.push_back(row_max);
                    bcQyTime.push_back(col_min);
                    bcQyTime.push_back(col_max);
                    bcQyTime.push_back(offset);
                    bcQyTime.push_back(sz_times);
                    numBC.numBcQyTime++;
                } else if (type == "QxyFree") {
                    bcQxyFree.push_back(row_min);
                    bcQxyFree.push_back(row_max);
                    bcQxyFree.push_back(col_min);
                    bcQxyFree.push_back(col_max);
                    bcQxyFree.push_back(row_ref);
                    bcQxyFree.push_back(col_ref);
                    numBC.numBcQxyFree++;
                } else {
                    utils::error("Invalid value for " + path + "type");
                }

                // Calculate time series data offset
                offset += sz_times;
            }
        }
    } catch (const libconfig::SettingNotFoundException &nfex) {
        utils::error((std::string)nfex.getPath() + " not found");
    } catch (const libconfig::SettingTypeException &tyex) {
        utils::error((std::string)tyex.getPath()
                + " contains unexpected value type");
    }
}

void FileIO::readMonitPoints(float &dtMonit, std::vector<cl_uint2> &points)
{
    try {
        if (cfg->exists("monitoring")) {
            dtMonit = cfg->lookup("monitoring.output-interval");
            if (dtMonit > (float)cfg->lookup("control.simulation-length")) {
                utils::warning("monitoring.output-interval > control.simulation-length");
            }

            const int num_rows = cfg->lookup("control.num-rows");
            const int num_cols = cfg->lookup("control.num-cols");
            const int num_points = cfg->lookup("monitoring.points").getLength();
            for (int i = 0; i < num_points; i++) {
                const std::string path = "monitoring.points.["
                        + std::to_string(i) + "].";
                int row = cfg->lookup(path+"row");
                int col = cfg->lookup(path+"col");
                if (row <= 0 || row > num_rows || col <= 0 || col > num_cols) {
                    utils::warning("Ignoring invalid cell index at: " + path);
                } else {
                    points.push_back({{(cl_uint)col--, (cl_uint)row--}});
                }
            }
        }
    } catch (const libconfig::SettingNotFoundException &nfex) {
        utils::error((std::string)nfex.getPath() + " not found");
    } catch (const libconfig::SettingTypeException &tyex) {
        utils::error((std::string)tyex.getPath()
                + " contains unexpected value type");
    }

    // Open monitoring output file and write header
    if (points.size()) {
        monitFile.open(monitFilename.c_str());
        if (!monitFile.is_open()) {
            utils::error("Could not open domain output file: " + monitFilename);
        }
        std::string header = " TIME (s)      ";
        char buf[128];
        for (unsigned int i = 1; i <= points.size(); i++) {
            if (i == points.size()) {
                sprintf(buf, "H  %-9.2u ET %-9.2u QX %-9.2u QY %-9.2u TX %-9.2u"
                    " TY %.2u", i, i, i, i, i, i);
            } else {
                sprintf(buf, "H  %-9.2u ET %-9.2u QX %-9.2u QY %-9.2u TX %-9.2u"
                    " TY %-9.2u  ", i, i, i, i, i, i);
            }
            header += buf;
        }
        monitFile << header << std::endl;
    }
}

void FileIO::readVegetation(std::vector<std::string> &veg_types,
        std::vector<cl_float> &veg_data, std::vector<cl_float> &veg_heights,
        std::vector<cl_float> &veg_widths, std::vector<cl_float> &veg_areas)
{
    try {
        if (cfg->exists("vegetation")) {
            unsigned int offset = 0;
            for (int i = 0; i < cfg->lookup("vegetation").getLength(); i++) {
                const std::string path = "vegetation.["
                        + std::to_string(i) + "].";

                veg_types.push_back(cfg->lookup(path+"type"));
                veg_data.push_back(cfg->lookup(path+"density"));
                veg_data.push_back(cfg->lookup(path+"flex-rigid"));
                veg_data.push_back(cfg->lookup(path+"drag-coef"));
                veg_data.push_back((float)cfg->lookup(path+"vogel-exp") * 0.5f);

                const int sz_height = cfg->lookup(path+"height").getLength();
                const int sz_width = cfg->lookup(path+"width").getLength();
                const int sz_area = cfg->lookup(path+"projected-area").getLength();

                if (sz_height != sz_width || sz_height != sz_area
                        || sz_height < 2) {
                    utils::error("Ivalid data lengths at:" + path);
                }

                for (int j = 0; j < sz_height; j++) {
                    const std::string path_height = path + "height.["
                            + std::to_string(j) + "]";
                    const std::string path_area = path + "projected-area.["
                            + std::to_string(j) + "]";
                    const std::string path_width = path + "width.["
                            + std::to_string(j) + "]";
                    const cl_float height = cfg->lookup(path_height);
                    if (j == 0 && height != 0.0) {
                        utils::error(path_height + " != 0.0");
                    }
                    veg_heights.push_back(height);
                    veg_widths.push_back(cfg->lookup(path_width));
                    veg_areas.push_back(cfg->lookup(path_area));
                }
                veg_data.push_back(offset);
                veg_data.push_back(sz_height);
                offset += sz_height;
            }
        }
    } catch (const libconfig::SettingNotFoundException &nfex) {
        utils::error((std::string)nfex.getPath() + " not found");
    } catch (const libconfig::SettingTypeException &tyex) {
        utils::error((std::string)tyex.getPath()
                + " contains unexpected value type");
    }
}

void FileIO::readInitialValues(std::vector<cl_float> &H,
        std::vector<cl_float> &etu, std::vector<cl_float> &qxu,
        std::vector<cl_float> &qyu, std::vector<cl_float> &VARCHZ,
        std::vector<cl_uint> &IWET, std::vector<cl_uint> &IALL,
        std::vector<cl_uint> &iact, std::vector<cl_float> &etmax,
        std::vector<cl_float> &uvmax, const int numVegTypes)
{
    // When creating initial value file, j should increase most rapidly
    // i.e. traverse along rows
    try {
        // Open and check file
        std::string filename = cfg->lookup("initial-values");
        std::ifstream file(filename);
        if (!file.is_open()) {
            utils::error("Unable to open initial value file");
        }

        // Read file header
        std::string line;
        getline(file, line);

        // Control values
        const int num_rows = cfg->lookup("control.num-rows");
        const int num_cols = cfg->lookup("control.num-cols");
        const float dp_min = cfg->lookup("constants.min-water-depth");
        int count = 0;

        // Read data
        while (getline(file, line)) {
            std::istringstream iss(line);
            int i;
            int j;
            iss >> i >> j;

            // Check cell idices are valid
            if (i <= 0 || j <= 0 || i > num_rows || j > num_cols) {
                utils::error("Incorrect I or J index at line  " + filename
                        + ":" + std::to_string(count+2));
            }
            i--;
            j--;
            const unsigned int idx = j + i * num_cols;

            // Redefining iwet to a value > numVegTypes+1 each time
            // ensures that error is thrown if iwet not read in
            int iwet = 99;
            iss >> H[idx] >> etu[idx] >> qxu[idx] >> qyu[idx]
                    >> VARCHZ[idx] >> iwet;

            // Check that roughness coefficient valid
            if (VARCHZ[idx] < 0) {
                utils::error("Invalid roughness value at line " + filename
                        + ":" + std::to_string(count+2));
            }

            // Check vegetation index is valid
            if (iwet < 0 || iwet > numVegTypes+1) {
                utils::error("Invalid vegetation index at line " + filename
                        + ":" + std::to_string(count+2));
            }
            IWET[idx] = iwet;

            // Determing iact
            float dp = H[idx] + etu[idx];
            if (dp > dp_min) {
                iact[idx] = true;
            }

            // Initialize etmax and uvmax
            etmax[idx] = etu[idx];
            if (dp > dp_min) {
                uvmax[idx] = sqrt(qxu[idx]*qxu[idx] + qyu[idx]*qyu[idx]) / dp;
            }

            // Update line count
            count++;
        }

        // Check that correct number of elements were read in
        const int num_elements = num_rows * num_cols;
        if (count != num_elements) {
            utils::error("Incorrect number of 'cells' in file: " + filename);
        }

        // Determine IALL
        for (int i = 0; i < num_rows; i++) {
            for (int j = 0; j < num_cols; j++) {
                const int idx = j + i * num_cols;
                const unsigned int row_prev = std::max(0, idx-num_cols);
                const unsigned int row_next = std::min(num_elements-1, idx+num_cols);
                const unsigned int col_prev = std::max(0, idx-1);
                const unsigned int col_next = std::min(num_elements-1, idx+1);
                if (IWET[idx] || IWET[row_prev] || IWET[row_next] ||
                        IWET[col_prev] || IWET[col_next]) {
                    IALL[idx] = true;
                }
            }
        }
    } catch (const libconfig::SettingNotFoundException &nfex) {
        utils::error((std::string)nfex.getPath() + " not found");
    } catch (const libconfig::SettingTypeException &tyex) {
        utils::error((std::string)tyex.getPath()
                + " contains unexpected value type");
    }
}


void FileIO::writeDomainData(const std::vector<cl_float> &H,
        const std::vector<cl_float> &etu, const std::vector<cl_float> &qxu,
        const std::vector<cl_float> &qyu, const std::vector<cl_float> &taux,
        const std::vector<cl_float> &tauy, const std::vector<cl_uint> &IWET,
        const unsigned int numRows, const unsigned int numCols, const float dx,
        const float dpMin, const float time, const unsigned int numTimeStep,
        const long unsigned int elapsedTime, const float dtOut, float &timeOut)
{
    // Update output timer
    timeOut += dtOut;

    // Timing info
    char buf[128];
    sprintf(buf, " TIME: %.3e sec  TIME STEP: %-10u  DURATION: %.3e sec\n",
            time, numTimeStep, elapsedTime / 1000.f);
    domainFile << buf;

    // Domain info
    sprintf(buf, " ROWS: %-7u  COLS: %-7u  DX: %.3e  DPMIN: %.3e\n", numRows,
            numCols, dx, dpMin);
    domainFile << buf;

    // Column headers
    domainFile << " X (m)       Y (m)       H (m)       E (m)       QX (m2/s)  "
            " QY (m2/s)   TX (N/m2)   TY (N/m2)   IWET\n";

    // Hydrodynamic data
    for (unsigned int i = 0; i < numRows; i++) {
        const float Y = i * dx;
        for (unsigned int j = 0; j < numCols; j++) {
            const float X = j*dx;
            const unsigned int idx = j + i * numCols;
            sprintf(buf, "%11.4e %11.4e %11.4e %11.4e %11.4e %11.4e %11.4e "
                    "%11.4e  %u\n", X, Y, H[idx], etu[idx], qxu[idx], qyu[idx],
                    taux[idx], tauy[idx], IWET[idx]);
            domainFile << buf;
        }
        domainFile << "\n";
    }
    domainFile << std::endl;
}

void FileIO::writeMonitData(const std::vector<cl_float> &H,
        const std::vector<cl_float> &etu, const std::vector<cl_float> &qxu,
        const std::vector<cl_float> &qyu, const std::vector<cl_float> &taux,
        const std::vector<cl_float> &tauy, const std::vector<cl_uint2> &points,
        const unsigned int numCols, const float time, const float dtMonit,
        float &timeMonit)
{
    // Update output timer
    timeMonit += dtMonit;

    // Get simulation time
    char buf[128];
    sprintf(buf, "%11.4e   ", time);
    std::string line = buf;

    // Get data for each monitoring point
    for (unsigned int i = 0; i < points.size(); i++) {
        const unsigned int idx = points[i].s[0] + points[i].s[1] * numCols;
        if (i == points.size()-1) {
            sprintf(buf, "%11.4e  %11.4e  %11.4e  %11.4e  %11.4e  %11.4e",
                    H[idx], etu[idx], qxu[idx], qyu[idx], taux[idx], tauy[idx]);
        } else {
            sprintf(buf, "%11.4e  %11.4e  %11.4e  %11.4e  %11.4e  %11.4e   ",
                    H[idx], etu[idx], qxu[idx], qyu[idx], taux[idx], tauy[idx]);
        }
        line += buf;
    }
    monitFile << line << std::endl;
}

void FileIO::writeMaxValues(const std::vector<cl_float> &H,
        const std::vector<cl_float> &etmax, const std::vector<cl_float> &uvmax,
        const std::vector<cl_float> &taumax, const std::vector<cl_uint> &IWET,
        const unsigned int numRows, const unsigned int numCols, const float dx,
        const float dpMin)
{
    // Header
    domainFile << " MAX VALUES DURING SIMULATION\n";

    // Domain info
    char buf[128];
    sprintf(buf, " ROWS: %-7u  COLS: %-7u  DX: %.3e  DPMIN: %.3e\n", numRows,
            numCols, dx, dpMin);
    domainFile << buf;

    // Column headers
    domainFile << " X (m)       Y (m)       H (m)       E (m)       VMAG (m/s) "
            " TMAG (N/m2) IWET\n";

    // Hydrodynamic data
    for (unsigned int i = 0; i < numRows; i++) {
        const float Y = i * dx;
        for (unsigned int j = 0; j < numCols; j++) {
            const float X = j*dx;
            const unsigned int idx = j + i * numCols;
            sprintf(buf, "%11.4e %11.4e %11.4e %11.4e %11.4e %11.4e  %u\n",X, Y,
                    H[idx], etmax[idx], uvmax[idx], taumax[idx], IWET[idx]);
            domainFile << buf;
        }
        domainFile << "\n";
    }
    domainFile.close();
}
