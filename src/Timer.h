#pragma once

// System headers
#include <chrono>
#include <string>

class Timer {
public:
    Timer();
    void start();
    void stop();
    void pause();
    void resume();
    long unsigned int getDurationMicroseconds();
    long unsigned int getDurationMilliseconds();
    unsigned int getDurationSeconds();
    unsigned int getDurationMinutes();
    unsigned int getDurationHours();
    std::string getDurationFormatted();
    void printDurationFormatted(std::string msg = std::string());
private:
    std::chrono::high_resolution_clock::time_point zero;
    std::chrono::high_resolution_clock::time_point startTime;
    std::chrono::high_resolution_clock::time_point stopTime;
    std::chrono::high_resolution_clock::time_point pauseTime;
    std::chrono::high_resolution_clock::time_point resumeTime;
    std::chrono::duration<long int, std::micro> elapsedTime;
};
