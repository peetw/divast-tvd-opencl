#pragma once
#define __CL_ENABLE_EXCEPTIONS

// System headers
#include <string>

// Third-party headers
#include <CL/cl.hpp>

// Project headers
#include "types.h"

class OpenCl {
public:
    // Constuctors
    OpenCl(const cl_device_type deviceType, const cl_uint numCores,
            cl_uint platformIdx, const unsigned int numRows,
            const unsigned int numCols, const unsigned int numElements);

    // Mutators
    void initConstants(const Constants &constants);
    void initBoundaries(std::vector<cl_int> &bcOpen,
            std::vector<cl_uint> &bcWlTime, std::vector<cl_int> &bcWlFree,
            std::vector<cl_uint> &bcQxTime, std::vector<cl_uint> &bcQyTime,
            std::vector<cl_int> &bcQxyFree, std::vector<cl_float> &bcTsTimes,
            std::vector<cl_float> &bcTsValues);
    void initVegetation(std::vector<cl_float> &vegData,
            std::vector<cl_float> &vegHeights,
            std::vector<cl_float> &vegWidths,
            std::vector<cl_float> &vegAreas);
    void initInitialValues(std::vector<cl_float> &H,
            std::vector<cl_float> &etu,
            std::vector<cl_float> &qxu,
            std::vector<cl_float> &qyu,
            std::vector<cl_float> &VARCHZ,
            std::vector<cl_uint> &IWET,
            std::vector<cl_uint> &IALL,
            std::vector<cl_uint> &iact,
            std::vector<cl_float> &etmax,
            std::vector<cl_float> &uvmax);
    void initBedShearStress();
    void initTempHydro(const Constants &constants, const bool dtConstant,
            const float dt, const float beta, const float cori);
    void buildKernels();
    void setKernelArgs();

    // Accessors
    void printDetails();
    void preHydro(const Constants &constants,
              const std::vector<cl_uint> &IWET,
              std::vector<cl_uint> &iact,
              std::vector<cl_float> &etu,
              const bool dtConstant,
              const float dtMin,
              const float dtMax,
              const float beta,
              const float cori,
              const float courantFactor,
              float &dt);
    void hydroX(const NumBC &numBC, const float time,
            const SOLUTION_SCHEME solutionScheme);
    void hydroY(const NumBC &numBC, const float time,
            const SOLUTION_SCHEME solutionScheme);
    void postHydro();
    void getDomainData(std::vector<cl_float> &etu,
            std::vector<cl_float> &qxu,
            std::vector<cl_float> &qyu,
            std::vector<cl_float> &taux,
            std::vector<cl_float> &tauy);
    void getMaxValues(std::vector<cl_float> &etmax,
            std::vector<cl_float> &uvmax,
            std::vector<cl_float> &taumax);
    void finish();
private:
    // -----------------------------------------------------------------------
    // VARIABLES
    // -----------------------------------------------------------------------

    // --------------------------------
    // Device
    // --------------------------------

    // OpenCL device variables
    cl::Platform platform;
    cl::Device device;
    cl::Context context;
    cl::CommandQueue queue;

    // Kernel launch parameters
    cl::NDRange globalWorkSz;
    cl::NDRange localWorkSz;
    cl::NDRange globalWorkSzReduction;
    cl::NDRange localWorkSzReduction;
    cl::LocalSpaceArg localMemSzReduction;

    // Memory sizes for domain sized arrays
    size_t domainClUIntSz;
    size_t domainClFloatSz;

    // Reduction output elements
    size_t numElementsReduction;

    // --------------------------------
    // Buffers
    // --------------------------------

    // Constants
    cl::Buffer constants_d;
    cl::Buffer vars_d;

    // Boundary conditions
    cl::Buffer bcOpen_d;
    cl::Buffer bcWlTime_d;
    cl::Buffer bcWlFree_d;
    cl::Buffer bcQxTime_d;
    cl::Buffer bcQyTime_d;
    cl::Buffer bcQxyFree_d;
    cl::Buffer bcTsTimes_d;
    cl::Buffer bcTsValues_d;

    // Vegetation data
    cl::Buffer vegData_d;
    cl::Buffer vegHeights_d;
    cl::Buffer vegWidths_d;
    cl::Buffer vegAreas_d;

    // Initial values
    cl::Buffer H_d;
    cl::Buffer etu_d;
    cl::Buffer qxu_d;
    cl::Buffer qyu_d;
    cl::Buffer VARCHZ_d;
    cl::Buffer IWET_d;
    cl::Buffer IALL_d;
    cl::Buffer iact_d;
    cl::Buffer etmax_d;
    cl::Buffer uvmax_d;

    // Bed shear stress
    cl::Buffer taux_d;
    cl::Buffer tauy_d;
    cl::Buffer taumax_d;

    // Temporary hydrodynamic arrays
    cl::Buffer etl_d;
    cl::Buffer qxl_d;
    cl::Buffer qyl_d;
    cl::Buffer etm_d;
    cl::Buffer qxm_d;
    cl::Buffer qym_d;
    cl::Buffer courant_d;   // Dependent on dt_constant

    // --------------------------------
    // Kernels
    // --------------------------------

    // pre_hydo.cl
    cl::Kernel maxCourantNumber;

    // boundary.cl
    cl::Kernel boundaryOpen;
    cl::Kernel boundaryWlTime;
    cl::Kernel boundaryWlFree;
    cl::Kernel boundaryQxTime;
    cl::Kernel boundaryQyTime;
    cl::Kernel boundaryQxyFree;

    // mac_xy.cl
    cl::Kernel macPredDrying;
    cl::Kernel macCorrDrying;
    cl::Kernel macWetDryInterface;

    // mac_x.cl
    cl::Kernel macPredX;
    cl::Kernel macCorrX;

    // mac_y.cl
    cl::Kernel macPredY;
    cl::Kernel macCorrY;

    // tvd_x.cl
    cl::Kernel tvdX;

    // tvd_y.cl
    cl::Kernel tvdY;

    // tvd_xy.cl
    cl::Kernel tvdPost;

    // post_hydro.cl
    cl::Kernel activeCellsMaxVals;
    cl::Kernel bedShearStress;

    // -----------------------------------------------------------------------
    // ACCESSORS
    // -----------------------------------------------------------------------

    // -----------------------------------------------------------------------
    // MUTATORS
    // -----------------------------------------------------------------------
    cl::Kernel buildKernel(const std::string &filename,
            const std::string &funcName);
    void wetting(const float dpMin,
                 const std::vector<cl_uint> &IWET,
                 std::vector<cl_uint> &iact,
                 std::vector<cl_float> &etu);
    void boundaryConditions(const NumBC &numBC, const float time);
};


// Anonymous namespace
namespace
{
std::string formatProfile(const std::string profile);
std::string formatDeviceType(const cl_device_type type_enum);
std::string formatFreq(const cl_uint freq);
std::string formatMemSize(const cl_ulong mem_size);
}
