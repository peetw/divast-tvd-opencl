#pragma once
#define __CL_ENABLE_EXCEPTIONS

// System headers
#include <fstream>
#include <string>
#include <vector>

// Third-party headers
#include <CL/cl.hpp>
#include <libconfig.h++>

// Project headers
#include "types.h"

class FileIO {
public:
    // Constuctors and destructors
    FileIO(const std::string &filename);
    ~FileIO();

    // Mutators

    // Accessors
    void printDetails();
    void readHardware(cl_device_type &deviceType, cl_uint &numCores,
            cl_uint &platformIdx);
    void readParams(Constants &constants, SOLUTION_SCHEME &solutionScheme,
            bool &alternateDirection, float &timeSim, float &dtOut,
            float &dtMin, float &dtMax, unsigned int &numRows,
            unsigned int &numCols, float &beta, float &cori,
            float &courantFactor);
    void readBoundaries(std::vector<cl_int> &bcOpen,
            std::vector<cl_uint> &bcWlTime, std::vector<cl_int> &bcWlFree,
            std::vector<cl_uint> &bcQxTime, std::vector<cl_uint> &bcQyTime,
            std::vector<cl_int> &bcQxyFree, std::vector<cl_float> &bcTsTimes,
            std::vector<cl_float> &bcTsValues, NumBC &numBC);
    void readMonitPoints(float &dtMonit, std::vector<cl_uint2> &points);
    void readVegetation(std::vector<std::string> &vegTypes,
            std::vector<cl_float> &vegData, std::vector<cl_float> &vegHeights,
            std::vector<cl_float> &vegWidths, std::vector<cl_float> &vegAreas);
    void readInitialValues(std::vector<cl_float> &H, std::vector<cl_float> &etu,
            std::vector<cl_float> &qxu, std::vector<cl_float> &qyu,
            std::vector<cl_float> &VARCHZ, std::vector<cl_uint> &IWET,
            std::vector<cl_uint> &IALL, std::vector<cl_uint> &iact,
            std::vector<cl_float> &etmax, std::vector<cl_float> &uvmax,
            const int numVegTypes);

    void writeDomainData(const std::vector<cl_float> &H,
            const std::vector<cl_float> &etu, const std::vector<cl_float> &qxu,
            const std::vector<cl_float> &qyu, const std::vector<cl_float> &taux,
            const std::vector<cl_float> &tauy, const std::vector<cl_uint> &IWET,
            const unsigned int numRows, const unsigned int numCols,
            const float dx, const float dpMin, const float time,
            const unsigned int numTimeStep, const long unsigned int elapsedTime,
            const float dtOut, float &timeOut);
    void writeMonitData(const std::vector<cl_float> &H,
            const std::vector<cl_float> &etu, const std::vector<cl_float> &qxu,
            const std::vector<cl_float> &qyu, const std::vector<cl_float> &taux,
            const std::vector<cl_float> &tauy,
            const std::vector<cl_uint2> &points, const unsigned int numCols,
            const float time, const float dtMonit, float &timeMonit);
    void writeMaxValues(const std::vector<cl_float> &H,
            const std::vector<cl_float> &etmax,
            const std::vector<cl_float> &uvmax,
            const std::vector<cl_float> &taumax,
            const std::vector<cl_uint> &IWET, const unsigned int numRows,
            const unsigned int numCols, const float dx, const float dpMin);
private:
    // Variables
    libconfig::Config *cfg;
    std::ofstream domainFile;
    std::string monitFilename;
    std::ofstream monitFile;

    // Accessors

    // Mutators
};
