// Class header
#include "OpenCl.h"

// System headers
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

// Third-party headers
#include <CL/cl.hpp>

// Project headers
#include "utils.h"

OpenCl::OpenCl(const cl_device_type deviceType, const cl_uint numCores,
        cl_uint platformIdx, const unsigned int numRows,
        const unsigned int numCols, const unsigned int numElements)
{
    try {
        // Select specified platform
        std::vector<cl::Platform> platforms;
        cl::Platform::get(&platforms);
        if (platformIdx >= platforms.size()) {
            utils::warning("Value for platform-idx too large, using 0");
            platformIdx = 0;
        }
        platform = platforms[platformIdx];

        // Find specified device type
        std::vector<cl::Device> devices;
        platform.getDevices(deviceType, &devices);
        device = devices[0];

        // Restrict to specified number of CPU cores
#ifdef CL_VERSION_1_2
        const unsigned int total_cores = device.getInfo<CL_DEVICE_MAX_COMPUTE_UNITS>();
        if (deviceType == CL_DEVICE_TYPE_CPU && numCores > 0
                && numCores < total_cores) {
            const std::string ext = device.getInfo<CL_DEVICE_EXTENSIONS>();
            if (ext.find("cl_ext_device_fission") != std::string::npos) {
                cl_device_partition_property partition_props[] =
                    {CL_DEVICE_PARTITION_EQUALLY, numCores, 0};
                device.createSubDevices(partition_props, &devices);
                device = devices[0];
            } else {
                utils::warning("Selected device does not support device_fission");
            }
        }
#endif

        // Create context and command queue
        cl_context_properties context_props[] = {CL_CONTEXT_PLATFORM,
            (cl_context_properties)(platforms[platformIdx])(), 0};
        context = cl::Context(devices, context_props);
        queue = cl::CommandQueue(context, device);
    } catch(cl::Error &e) {
        utils::clError(e, __FUNCTION__);
    }

    // Domain-sized kernel launch parameters
    globalWorkSz = cl::NDRange(numCols, numRows);
    localWorkSz = cl::NullRange;

    // Memory sizes for domain sized arrays
    domainClUIntSz = numElements * sizeof(cl_uint);
    domainClFloatSz = numElements * sizeof(cl_float);

    // Reduction kernel launch parameters
    const cl_uint num_compute_units = device.getInfo<CL_DEVICE_MAX_COMPUTE_UNITS>();
    const size_t work_group_size = device.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>();
    const cl_uint multiplier = 8;   // 8 seems to be optimal (= max no. of workgroups active per compute unit)
    const size_t num_threads = num_compute_units * work_group_size * multiplier;

    globalWorkSzReduction = cl::NDRange(num_threads);
    localWorkSzReduction = cl::NDRange(work_group_size);
    localMemSzReduction = cl::Local(work_group_size * sizeof(cl_float));

    // Determine number of elements in reduction output buffer
    numElementsReduction = num_compute_units * multiplier;

    // Display selected device and its details
    printDetails();
}

void OpenCl::printDetails()
{
    utils::printHeader("OpenCL platform");
    std::cout << "Name:       " << platform.getInfo<CL_PLATFORM_NAME>() << "\n";
    std::cout << "Vendor:     " << platform.getInfo<CL_PLATFORM_VENDOR>() << "\n";
    std::cout << "Version:    " << platform.getInfo<CL_PLATFORM_VERSION>() << "\n";
    std::cout << "Profile:    " << formatProfile(platform.getInfo<CL_PLATFORM_PROFILE>()) << "\n";
    std::cout << "Extensions: " << platform.getInfo<CL_PLATFORM_EXTENSIONS>() << "\n";

    utils::printHeader("OpenCL device");
    std::cout << "Name:          " << device.getInfo<CL_DEVICE_NAME>() << "\n";
    std::cout << "Type:          " << formatDeviceType(device.getInfo<CL_DEVICE_TYPE>()) << "\n";
    std::cout << "Vendor:        " << device.getInfo<CL_DEVICE_VENDOR>() << "\n";
    std::cout << "Version:       " << device.getInfo<CL_DEVICE_VERSION>() << "\n";
    std::cout << "Profile:       " << formatProfile(device.getInfo<CL_DEVICE_PROFILE>()) << "\n";
    std::cout << "Compute units: " << device.getInfo<CL_DEVICE_MAX_COMPUTE_UNITS>() << "\n";
    std::cout << "Clock freq:    " << formatFreq(device.getInfo<CL_DEVICE_MAX_CLOCK_FREQUENCY>()) << "\n";
    std::cout << "Global mem:    " << formatMemSize(device.getInfo<CL_DEVICE_GLOBAL_MEM_SIZE>()) << "\n";
    std::cout << "Constant mem:  " << formatMemSize(device.getInfo<CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE>()) << "\n";
    std::cout << "Local mem:     " << formatMemSize(device.getInfo<CL_DEVICE_LOCAL_MEM_SIZE>()) << "\n";
    std::cout << "Extensions:    " << device.getInfo<CL_DEVICE_EXTENSIONS>() << "\n";
}

void OpenCl::initConstants(const Constants &constants)
{
    try {
        // Initialize device buffer and transfer data to device
        constants_d = cl::Buffer(context, CL_MEM_READ_ONLY, sizeof(Constants));
        queue.enqueueWriteBuffer(constants_d, CL_TRUE, 0, sizeof(Constants), &constants);
    } catch(cl::Error &e) {
        utils::clError(e, __FUNCTION__);
    }
}

void OpenCl::initBoundaries(std::vector<cl_int> &bcOpen,
        std::vector<cl_uint> &bcWlTime, std::vector<cl_int> &bcWlFree,
        std::vector<cl_uint> &bcQxTime, std::vector<cl_uint> &bcQyTime,
        std::vector<cl_int> &bcQxyFree, std::vector<cl_float> &bcTsTimes,
        std::vector<cl_float> &bcTsValues)
{
    // Array sizes
    const size_t bcOpenSz = sizeof(cl_int)*bcOpen.size();
    const size_t bcWlTimeSz = sizeof(cl_uint)*bcWlTime.size();
    const size_t bcWlFreeSz = sizeof(cl_int)*bcWlFree.size();
    const size_t bcQxTimeSz = sizeof(cl_uint)*bcQxTime.size();
    const size_t bcQyTimeSz = sizeof(cl_uint)*bcQyTime.size();
    const size_t bcQxyFreeSz = sizeof(cl_int)*bcQxyFree.size();
    const size_t bcTsTimesSz = sizeof(cl_float)*bcTsTimes.size();
    const size_t bcTsValuesSz = sizeof(cl_float)*bcTsValues.size();

    try {
        // Initialize device buffers and transfer data to device
        if (bcOpenSz) {
            bcOpen_d = cl::Buffer(context, CL_MEM_READ_ONLY, bcOpenSz);
            queue.enqueueWriteBuffer(bcOpen_d, CL_TRUE, 0, bcOpenSz, &bcOpen[0]);
        }
        if (bcWlTimeSz) {
            bcWlTime_d = cl::Buffer(context, CL_MEM_READ_ONLY, bcWlTimeSz);
            queue.enqueueWriteBuffer(bcWlTime_d, CL_TRUE, 0, bcWlTimeSz, &bcWlTime[0]);
        }
        if (bcWlFreeSz) {
            bcWlFree_d = cl::Buffer(context, CL_MEM_READ_ONLY, bcWlFreeSz);
            queue.enqueueWriteBuffer(bcWlFree_d, CL_TRUE, 0, bcWlFreeSz, &bcWlFree[0]);
        }
        if (bcQxTimeSz) {
            bcQxTime_d = cl::Buffer(context, CL_MEM_READ_ONLY, bcQxTimeSz);
            queue.enqueueWriteBuffer(bcQxTime_d, CL_TRUE, 0, bcQxTimeSz, &bcQxTime[0]);
        }
        if (bcQyTimeSz) {
            bcQyTime_d = cl::Buffer(context, CL_MEM_READ_ONLY, bcQyTimeSz);
            queue.enqueueWriteBuffer(bcQyTime_d, CL_TRUE, 0, bcQyTimeSz, &bcQyTime[0]);
        }
        if (bcQxyFreeSz) {
            bcQxyFree_d = cl::Buffer(context, CL_MEM_READ_ONLY, bcQxyFreeSz);
            queue.enqueueWriteBuffer(bcQxyFree_d, CL_TRUE, 0, bcQxyFreeSz, &bcQxyFree[0]);
        }
        if (bcTsTimesSz) {
            bcTsTimes_d = cl::Buffer(context, CL_MEM_READ_ONLY, bcTsTimesSz);
            queue.enqueueWriteBuffer(bcTsTimes_d, CL_TRUE, 0, bcTsTimesSz, &bcTsTimes[0]);
        }
        if (bcTsValuesSz) {
            bcTsValues_d = cl::Buffer(context, CL_MEM_READ_ONLY, bcTsValuesSz);
            queue.enqueueWriteBuffer(bcTsValues_d, CL_TRUE, 0, bcTsValuesSz, &bcTsValues[0]);
        }
    } catch(cl::Error &e) {
        utils::clError(e, __FUNCTION__);
    }
}

void OpenCl::initVegetation(std::vector<cl_float> &vegData,
        std::vector<cl_float> &vegHeights, std::vector<cl_float> &vegWidths,
        std::vector<cl_float> &vegAreas)
{
    // Array sizes
    const size_t vegDataSz = sizeof(cl_float)*vegData.size();
    const size_t vegHeightsSz = sizeof(cl_float)*vegHeights.size();
    const size_t vegWidthsSz = sizeof(cl_float)*vegWidths.size();
    const size_t vegAreasSz = sizeof(cl_float)*vegAreas.size();

    try {
        if (vegDataSz) {
            // Initialize device buffers
            vegData_d = cl::Buffer(context, CL_MEM_READ_ONLY, vegDataSz);
            vegHeights_d = cl::Buffer(context, CL_MEM_READ_ONLY, vegHeightsSz);
            vegWidths_d = cl::Buffer(context, CL_MEM_READ_ONLY, vegWidthsSz);
            vegAreas_d = cl::Buffer(context, CL_MEM_READ_ONLY, vegAreasSz);

            // Transfer data to device
            queue.enqueueWriteBuffer(vegData_d, CL_FALSE, 0, vegDataSz, &vegData[0]);
            queue.enqueueWriteBuffer(vegHeights_d, CL_FALSE, 0, vegHeightsSz, &vegHeights[0]);
            queue.enqueueWriteBuffer(vegWidths_d, CL_FALSE, 0, vegWidthsSz, &vegWidths[0]);
            queue.enqueueWriteBuffer(vegAreas_d, CL_TRUE, 0, vegAreasSz, &vegAreas[0]);
        } else {
            // Initialize device buffers
            vegData_d = cl::Buffer(context, CL_MEM_READ_ONLY, sizeof(cl_float));
            vegHeights_d = cl::Buffer(context, CL_MEM_READ_ONLY, sizeof(cl_float));
            vegWidths_d = cl::Buffer(context, CL_MEM_READ_ONLY, sizeof(cl_float));
            vegAreas_d = cl::Buffer(context, CL_MEM_READ_ONLY, sizeof(cl_float));
        }
    } catch(cl::Error &e) {
        utils::clError(e, __FUNCTION__);
    }
}

void OpenCl::initInitialValues(std::vector<cl_float> &H,
        std::vector<cl_float> &etu,
        std::vector<cl_float> &qxu,
        std::vector<cl_float> &qyu,
        std::vector<cl_float> &VARCHZ,
        std::vector<cl_uint> &IWET,
        std::vector<cl_uint> &IALL,
        std::vector<cl_uint> &iact,
        std::vector<cl_float> &etmax,
        std::vector<cl_float> &uvmax)
{
    try {
        // Initialize device buffers
        H_d = cl::Buffer(context, CL_MEM_READ_ONLY, domainClFloatSz);
        etu_d = cl::Buffer(context, CL_MEM_READ_WRITE, domainClFloatSz);
        qxu_d = cl::Buffer(context, CL_MEM_READ_WRITE, domainClFloatSz);
        qyu_d = cl::Buffer(context, CL_MEM_READ_WRITE, domainClFloatSz);
        VARCHZ_d = cl::Buffer(context, CL_MEM_READ_ONLY, domainClFloatSz);
        IWET_d = cl::Buffer(context, CL_MEM_READ_ONLY, domainClUIntSz);
        IALL_d = cl::Buffer(context, CL_MEM_READ_ONLY, domainClUIntSz);
        iact_d = cl::Buffer(context, CL_MEM_READ_WRITE, domainClUIntSz);
        etmax_d = cl::Buffer(context, CL_MEM_READ_WRITE, domainClFloatSz);
        uvmax_d = cl::Buffer(context, CL_MEM_READ_WRITE, domainClFloatSz);

        // Transfer data to device
        queue.enqueueWriteBuffer(H_d, CL_FALSE, 0, domainClFloatSz, &H[0]);
        queue.enqueueWriteBuffer(etu_d, CL_FALSE, 0, domainClFloatSz, &etu[0]);
        queue.enqueueWriteBuffer(qxu_d, CL_FALSE, 0, domainClFloatSz, &qxu[0]);
        queue.enqueueWriteBuffer(qyu_d, CL_FALSE, 0, domainClFloatSz, &qyu[0]);
        queue.enqueueWriteBuffer(VARCHZ_d, CL_FALSE, 0, domainClFloatSz, &VARCHZ[0]);
        queue.enqueueWriteBuffer(IWET_d, CL_FALSE, 0, domainClUIntSz, &IWET[0]);
        queue.enqueueWriteBuffer(IALL_d, CL_FALSE, 0, domainClUIntSz, &IALL[0]);
        queue.enqueueWriteBuffer(iact_d, CL_FALSE, 0, domainClUIntSz, &iact[0]);
        queue.enqueueWriteBuffer(etmax_d, CL_FALSE, 0, domainClFloatSz, &etmax[0]);
        queue.enqueueWriteBuffer(uvmax_d, CL_TRUE, 0, domainClFloatSz, &uvmax[0]);
    } catch(cl::Error &e) {
        utils::clError(e, __FUNCTION__);
    }
}

void OpenCl::initBedShearStress()
{
    std::vector<cl_float> zero(globalWorkSz[1] * globalWorkSz[0], 0);
    try {
        // Initialize device buffers
        taux_d = cl::Buffer(context, CL_MEM_WRITE_ONLY, domainClFloatSz);
        tauy_d = cl::Buffer(context, CL_MEM_WRITE_ONLY, domainClFloatSz);
        taumax_d = cl::Buffer(context, CL_MEM_READ_WRITE, domainClFloatSz);

        // Transfer data to device
        queue.enqueueWriteBuffer(taux_d, CL_FALSE, 0, domainClFloatSz, &zero[0]);
        queue.enqueueWriteBuffer(tauy_d, CL_FALSE, 0, domainClFloatSz, &zero[0]);
        queue.enqueueWriteBuffer(taumax_d, CL_TRUE, 0, domainClFloatSz, &zero[0]);
    } catch(cl::Error &e) {
        utils::clError(e, __FUNCTION__);
    }
}

void OpenCl::initTempHydro(const Constants &constants, const bool dtConstant,
        const float dt, const float beta, const float cori)
{
    std::vector<cl_float> zero(globalWorkSz[1] * globalWorkSz[0], 0);
    try {
        // Initialize device buffers
        etl_d = cl::Buffer(context, CL_MEM_READ_WRITE, domainClFloatSz);
        qxl_d = cl::Buffer(context, CL_MEM_READ_WRITE, domainClFloatSz);
        qyl_d = cl::Buffer(context, CL_MEM_READ_WRITE, domainClFloatSz);
        etm_d = cl::Buffer(context, CL_MEM_READ_WRITE, domainClFloatSz);
        qxm_d = cl::Buffer(context, CL_MEM_READ_WRITE, domainClFloatSz);
        qym_d = cl::Buffer(context, CL_MEM_READ_WRITE, domainClFloatSz);

        if (!dtConstant) {
            courant_d = cl::Buffer(context, CL_MEM_WRITE_ONLY,
                    numElementsReduction * sizeof(cl_float));
        }

        // Transfer data to device
        queue.enqueueWriteBuffer(etl_d, CL_FALSE, 0, domainClFloatSz, &zero[0]);
        queue.enqueueWriteBuffer(qxl_d, CL_FALSE, 0, domainClFloatSz, &zero[0]);
        queue.enqueueWriteBuffer(qyl_d, CL_FALSE, 0, domainClFloatSz, &zero[0]);
        queue.enqueueWriteBuffer(etm_d, CL_FALSE, 0, domainClFloatSz, &zero[0]);
        queue.enqueueWriteBuffer(qxm_d, CL_FALSE, 0, domainClFloatSz, &zero[0]);
        queue.enqueueWriteBuffer(qym_d, CL_TRUE, 0, domainClFloatSz, &zero[0]);

        // Initialize buffer to hold variables that can change between timesteps
        vars_d = cl::Buffer(context, CL_MEM_READ_ONLY, sizeof(Vars));
        if (dtConstant) {
            Vars vars;
            vars.dt = dt;
            vars.dtDx = dt / constants.dx;
            vars.dtDxSq = vars.dtDx / constants.dx;
            vars.hDtDxG = 0.5 * vars.dtDx * 9.81;
            vars.dtDxBeta = vars.dtDx * beta;
            vars.dtG = dt * 9.81;
            vars.dtCori = dt * cori;

            queue.enqueueWriteBuffer(vars_d, CL_TRUE, 0, sizeof(Vars), &vars);
        }
    } catch(cl::Error &e) {
        utils::clError(e, __FUNCTION__);
    }
}

void OpenCl::buildKernels()
{
    // pre_hydo.cl
    std::string file = "pre_hydro.cl";
    maxCourantNumber = buildKernel(file, "maxCourantNumber");

    // boundary.cl
    file = "boundary.cl";
    boundaryOpen = buildKernel(file, "boundaryOpen");
    boundaryWlTime = buildKernel(file, "boundaryWlTime");
    boundaryWlFree = buildKernel(file, "boundaryWlFree");
    boundaryQxTime = buildKernel(file, "boundaryQxTime");
    boundaryQyTime = buildKernel(file, "boundaryQyTime");
    boundaryQxyFree = buildKernel(file, "boundaryQxyFree");

    // mac_xy.cl
    file = "mac_xy.cl";
    macPredDrying = buildKernel(file, "macPredDrying");
    macCorrDrying = buildKernel(file, "macCorrDrying");
    macWetDryInterface = buildKernel(file, "macWetDryInterface");

    // mac_x.cl
    file = "mac_x.cl";
    macPredX = buildKernel(file, "macPredX");
    macCorrX = buildKernel(file, "macCorrX");

    // mac_y.cl
    file = "mac_y.cl";
    macPredY = buildKernel(file, "macPredY");
    macCorrY = buildKernel(file, "macCorrY");

    // tvd_x.cl
    file = "tvd_x.cl";
    tvdX = buildKernel(file, "tvdX");

    // tvd_y.cl
    file = "tvd_y.cl";
    tvdY = buildKernel(file, "tvdY");

    // tvd_xy.cl
    file = "tvd_xy.cl";
    tvdPost = buildKernel(file, "tvdPost");

    // post_hydro.cl
    file = "post_hydro.cl";
    activeCellsMaxVals = buildKernel(file, "activeCellsMaxVals");
    bedShearStress = buildKernel(file, "bedShearStress");
}

cl::Kernel OpenCl::buildKernel(const std::string &filename,
        const std::string &funcName)
{
    const std::string directory = "src/kernels/";
    cl::Kernel kernel;
    try {
        // Read source file
        std::ifstream sourceFile(directory + filename);
        if (!sourceFile.is_open()) {
            utils::error("Unable to open file: " + directory + filename);
        }
        std::string sourceCode(std::istreambuf_iterator<char>(sourceFile),
                (std::istreambuf_iterator<char>()));
        cl::Program::Sources source(1, std::make_pair(sourceCode.c_str(),
                sourceCode.length()+1));

        // Make program of the source code in the context
        cl::Program program = cl::Program(context, source);

        // Build program for the specified device
        std::vector<cl::Device> devices;
        devices.push_back(device);
        try {
            program.build(devices, "-I src -I src/kernels");
        } catch(cl::Error &e) {
            utils::error("Failed to build kernel: " + funcName +
                    "\nBuild log:\n" +
                    program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(device));
        }

        // Make kernel
        kernel = cl::Kernel(program, funcName.c_str());
    } catch(cl::Error &e) {
        utils::clError(e, __FUNCTION__);
    }
    return kernel;
}

void OpenCl::setKernelArgs()
{
    try {
        // pre_hydo.cl
        cl_uint num_elements = globalWorkSz[0] * globalWorkSz[1];
        maxCourantNumber.setArg(0, sizeof(cl_uint), &num_elements);
        maxCourantNumber.setArg(1, localMemSzReduction);
        maxCourantNumber.setArg(2, iact_d);
        maxCourantNumber.setArg(3, H_d);
        maxCourantNumber.setArg(4, etu_d);
        maxCourantNumber.setArg(5, qxu_d);
        maxCourantNumber.setArg(6, qyu_d);
        maxCourantNumber.setArg(7, courant_d);

        // boundary.cl
        boundaryWlTime.setArg(0, bcWlTime_d);
        boundaryWlTime.setArg(1, bcTsTimes_d);
        boundaryWlTime.setArg(2, bcTsValues_d);
        // setArg(3, time) done immediately before kernel launch
        boundaryWlTime.setArg(4, sizeof(cl_uint), const_cast<size_t*>(&globalWorkSz[0]));
        boundaryWlTime.setArg(5, etu_d);

        boundaryQxTime.setArg(0, bcQxTime_d);
        boundaryQxTime.setArg(1, bcTsTimes_d);
        boundaryQxTime.setArg(2, bcTsValues_d);
        // setArg(3, time) done immediately before kernel launch
        boundaryQxTime.setArg(4, sizeof(cl_uint), const_cast<size_t*>(&globalWorkSz[0]));
        boundaryQxTime.setArg(5, qxu_d);

        boundaryQyTime.setArg(0, bcQyTime_d);
        boundaryQyTime.setArg(1, bcTsTimes_d);
        boundaryQyTime.setArg(2, bcTsValues_d);
        // setArg(3, time) done immediately before kernel launch
        boundaryQyTime.setArg(4, sizeof(cl_uint), const_cast<size_t*>(&globalWorkSz[0]));
        boundaryQyTime.setArg(5, qyu_d);

        boundaryWlFree.setArg(0, bcWlFree_d);
        boundaryWlFree.setArg(1, sizeof(cl_uint), const_cast<size_t*>(&globalWorkSz[0]));
        boundaryWlFree.setArg(2, etu_d);

        boundaryQxyFree.setArg(0, bcQxyFree_d);
        boundaryQxyFree.setArg(1, sizeof(cl_uint), const_cast<size_t*>(&globalWorkSz[0]));
        boundaryQxyFree.setArg(2, qxu_d);
        boundaryQxyFree.setArg(3, qyu_d);

        boundaryOpen.setArg(0, bcOpen_d);
        boundaryOpen.setArg(1, sizeof(cl_uint), const_cast<size_t*>(&globalWorkSz[0]));
        boundaryOpen.setArg(2, etu_d);
        boundaryOpen.setArg(3, qxu_d);
        boundaryOpen.setArg(4, qyu_d);

        // mac_xy.cl
        macPredDrying.setArg(0, constants_d);
        macPredDrying.setArg(1, IALL_d);
        macPredDrying.setArg(2, H_d);
        macPredDrying.setArg(3, etu_d);
        macPredDrying.setArg(4, qxu_d);
        macPredDrying.setArg(5, qyu_d);
        macPredDrying.setArg(6, iact_d);
        macPredDrying.setArg(7, etl_d);
        macPredDrying.setArg(8, qxl_d);
        macPredDrying.setArg(9, qyl_d);

        macCorrDrying.setArg(0, constants_d);
        macCorrDrying.setArg(1, IWET_d);
        macCorrDrying.setArg(2, IALL_d);
        macCorrDrying.setArg(3, H_d);
        macCorrDrying.setArg(4, etl_d);
        macCorrDrying.setArg(5, qxl_d);
        macCorrDrying.setArg(6, qyl_d);
        macCorrDrying.setArg(7, iact_d);
        macCorrDrying.setArg(8, etm_d);
        macCorrDrying.setArg(9, qxm_d);
        macCorrDrying.setArg(10, qym_d);

        macWetDryInterface.setArg(0, IWET_d);
        macWetDryInterface.setArg(1, iact_d);
        // setArg(2, offset) and setArg(3, q[xy][lm]) set before kernel launch

        // mac_x.cl
        macPredX.setArg(0, constants_d);
        macPredX.setArg(1, vars_d);
        macPredX.setArg(2, vegData_d);
        macPredX.setArg(3, vegHeights_d);
        macPredX.setArg(4, vegAreas_d);
        macPredX.setArg(5, vegWidths_d);
        macPredX.setArg(6, IWET_d);
        macPredX.setArg(7, iact_d);
        macPredX.setArg(8, VARCHZ_d);
        macPredX.setArg(9, H_d);
        macPredX.setArg(10, etl_d);
        macPredX.setArg(11, qxl_d);
        macPredX.setArg(12, qyl_d);
        macPredX.setArg(13, etm_d);
        macPredX.setArg(14, qxm_d);
        macPredX.setArg(15, qym_d);

        macCorrX.setArg(0, constants_d);
        macCorrX.setArg(1, vars_d);
        macCorrX.setArg(2, vegData_d);
        macCorrX.setArg(3, vegHeights_d);
        macCorrX.setArg(4, vegAreas_d);
        macCorrX.setArg(5, vegWidths_d);
        macCorrX.setArg(6, IWET_d);
        macCorrX.setArg(7, iact_d);
        macCorrX.setArg(8, VARCHZ_d);
        macCorrX.setArg(9, H_d);
        macCorrX.setArg(10, etl_d);
        macCorrX.setArg(11, qxl_d);
        macCorrX.setArg(12, qyl_d);
        macCorrX.setArg(13, etm_d);
        macCorrX.setArg(14, qxm_d);
        macCorrX.setArg(15, qym_d);
        macCorrX.setArg(16, etu_d);
        macCorrX.setArg(17, qxu_d);
        macCorrX.setArg(18, qyu_d);

        // mac_y.cl
        macPredY.setArg(0, constants_d);
        macPredY.setArg(1, vars_d);
        macPredY.setArg(2, vegData_d);
        macPredY.setArg(3, vegHeights_d);
        macPredY.setArg(4, vegAreas_d);
        macPredY.setArg(5, vegWidths_d);
        macPredY.setArg(6, IWET_d);
        macPredY.setArg(7, iact_d);
        macPredY.setArg(8, VARCHZ_d);
        macPredY.setArg(9, H_d);
        macPredY.setArg(10, etl_d);
        macPredY.setArg(11, qxl_d);
        macPredY.setArg(12, qyl_d);
        macPredY.setArg(13, etm_d);
        macPredY.setArg(14, qxm_d);
        macPredY.setArg(15, qym_d);

        macCorrY.setArg(0, constants_d);
        macCorrY.setArg(1, vars_d);
        macCorrY.setArg(2, vegData_d);
        macCorrY.setArg(3, vegHeights_d);
        macCorrY.setArg(4, vegAreas_d);
        macCorrY.setArg(5, vegWidths_d);
        macCorrY.setArg(6, IWET_d);
        macCorrY.setArg(7, iact_d);
        macCorrY.setArg(8, VARCHZ_d);
        macCorrY.setArg(9, H_d);
        macCorrY.setArg(10, etl_d);
        macCorrY.setArg(11, qxl_d);
        macCorrY.setArg(12, qyl_d);
        macCorrY.setArg(13, etm_d);
        macCorrY.setArg(14, qxm_d);
        macCorrY.setArg(15, qym_d);
        macCorrY.setArg(16, etu_d);
        macCorrY.setArg(17, qxu_d);
        macCorrY.setArg(18, qyu_d);

        // tvd_x.cl
        tvdX.setArg(0, vars_d);
        tvdX.setArg(1, iact_d);
        tvdX.setArg(2, H_d);
        tvdX.setArg(3, etl_d);
        tvdX.setArg(4, qxl_d);
        tvdX.setArg(5, qyl_d);
        tvdX.setArg(6, etm_d);
        tvdX.setArg(7, qxm_d);
        tvdX.setArg(8, qym_d);

        // tvd_y.cl
        tvdY.setArg(0, vars_d);
        tvdY.setArg(1, iact_d);
        tvdY.setArg(2, H_d);
        tvdY.setArg(3, etl_d);
        tvdY.setArg(4, qxl_d);
        tvdY.setArg(5, qyl_d);
        tvdY.setArg(6, etm_d);
        tvdY.setArg(7, qxm_d);
        tvdY.setArg(8, qym_d);

        // tvd_xy.cl
        tvdPost.setArg(0, IWET_d);
        tvdPost.setArg(1, iact_d);
        tvdPost.setArg(2, etm_d);
        tvdPost.setArg(3, qxm_d);
        tvdPost.setArg(4, qym_d);
        tvdPost.setArg(5, etu_d);
        tvdPost.setArg(6, qxu_d);
        tvdPost.setArg(7, qyu_d);
        // setArg(8, offset) done immediately before kernel launch

        // post_hydro.cl
        activeCellsMaxVals.setArg(0, constants_d);
        activeCellsMaxVals.setArg(1, H_d);
        activeCellsMaxVals.setArg(2, etu_d);
        activeCellsMaxVals.setArg(3, qxu_d);
        activeCellsMaxVals.setArg(4, qyu_d);
        activeCellsMaxVals.setArg(5, iact_d);
        activeCellsMaxVals.setArg(6, etmax_d);
        activeCellsMaxVals.setArg(7, uvmax_d);

        bedShearStress.setArg(0, constants_d);
        bedShearStress.setArg(1, IWET_d);
        bedShearStress.setArg(2, iact_d);
        bedShearStress.setArg(3, H_d);
        bedShearStress.setArg(4, etu_d);
        bedShearStress.setArg(5, qxu_d);
        bedShearStress.setArg(6, qyu_d);
        bedShearStress.setArg(7, VARCHZ_d);
        bedShearStress.setArg(8, vegData_d);
        bedShearStress.setArg(9, vegHeights_d);
        bedShearStress.setArg(10, vegWidths_d);
        bedShearStress.setArg(11, taux_d);
        bedShearStress.setArg(12, tauy_d);
        bedShearStress.setArg(13, taumax_d);
    } catch(cl::Error &e) {
        utils::clError(e, __FUNCTION__);
    }
}


void OpenCl::preHydro(const Constants &constants,
                      const std::vector<cl_uint> &IWET,
                      std::vector<cl_uint> &iact,
                      std::vector<cl_float> &etu,
                      const bool dtConstant,
                      const float dtMin,
                      const float dtMax,
                      const float beta,
                      const float cori,
                      const float courantFactor,
                      float &dt)
{
    try {
        if (!dtConstant) {
            // Calculate maximum Courant number across domain
            queue.enqueueNDRangeKernel(maxCourantNumber, cl::NullRange,
                    globalWorkSzReduction, localWorkSzReduction);

            std::vector<cl_float> courant(numElementsReduction, 0);
            queue.enqueueReadBuffer(courant_d, CL_TRUE, 0,
                    numElementsReduction * sizeof(cl_float), &courant[0]);

            cl_float max_courant = courant[0];
            for (size_t i = 1; i < numElementsReduction; i++) {
                max_courant = (courant[i] > max_courant ? courant[i] : max_courant);
            }

            // Calculate new time step
            dt = courantFactor * constants.dx / (1e-10f + max_courant);
            if (dt < dtMin) {
                dt = dtMin;
            } else if (dt > dtMax) {
                dt = dtMax;
            }

            // Update time step variables
            Vars vars;
            vars.dt = dt;
            vars.dtDx = dt / constants.dx;
            vars.dtDxSq = vars.dtDx / constants.dx;
            vars.hDtDxG = 0.5 * vars.dtDx * 9.81;
            vars.dtDxBeta = vars.dtDx * beta;
            vars.dtG = dt * 9.81;
            vars.dtCori = dt * cori;

            queue.enqueueWriteBuffer(vars_d, CL_FALSE, 0, sizeof(Vars), &vars);
        }

        // Carry out wetting / drying check
        wetting(constants.dpMin, IWET, iact, etu);
    } catch(cl::Error &e) {
        utils::clError(e, __FUNCTION__);
    }
}

void OpenCl::wetting(const float dpMin,
                     const std::vector<cl_uint> &IWET,
                     std::vector<cl_uint> &iact,
                     std::vector<cl_float> &etu)
{
    // Transfer data from device
    queue.enqueueReadBuffer(iact_d, CL_FALSE, 0, domainClFloatSz, &iact[0]);
    queue.enqueueReadBuffer(etu_d, CL_TRUE, 0, domainClFloatSz, &etu[0]);

    // Search for the neighbouring wet cell with highest water level
    // NOTE: This cannot be easily parallized since it reads and writes to
    // not just etu[idx], but also etu[idx+-val]
    for (unsigned int i = 1; i < globalWorkSz[1]-1; i++) {
        for (unsigned int j = 1; j < globalWorkSz[0]-1; j++) {
            const unsigned int idx = j + i * globalWorkSz[0];
            if (IWET[idx] && !iact[idx]) {
                cl_float *et_tmp = &etu[idx];

                unsigned int idx_tmp = idx + 1;
                if (iact[idx_tmp] && etu[idx_tmp] > *et_tmp) {
                    et_tmp = &etu[idx_tmp];
                }

                idx_tmp = idx - 1;
                if (iact[idx_tmp] && etu[idx_tmp] > *et_tmp) {
                    et_tmp = &etu[idx_tmp];
                }

                idx_tmp = idx + globalWorkSz[0];
                if (iact[idx_tmp] && etu[idx_tmp] > *et_tmp) {
                    et_tmp = &etu[idx_tmp];
                }

                idx_tmp = idx - globalWorkSz[0];
                if (iact[idx_tmp] && etu[idx_tmp] > *et_tmp) {
                    et_tmp = &etu[idx_tmp];
                }

                // Increase wl in current cell and decrease in neighbour
                if ((*et_tmp - etu[idx]) > (dpMin * 2.f)) {
                    etu[idx] = etu[idx] + dpMin;
                    *et_tmp = *et_tmp - dpMin;
                }
            }
        }
    }

    // Transfer data to device
    queue.enqueueWriteBuffer(etu_d, CL_FALSE, 0, domainClFloatSz, &etu[0]);
}

void OpenCl::boundaryConditions(const NumBC &numBC, const float time)
{
    try {
        // Time series water level boundaries
        if (numBC.numBcWlTime) {
            boundaryWlTime.setArg(3, time);
            queue.enqueueNDRangeKernel(boundaryWlTime, cl::NullRange,
                    numBC.numBcWlTime, cl::NullRange);
        }

        // Time series x-direction flow rate boundaries
        if (numBC.numBcQxTime) {
            boundaryQxTime.setArg(3, time);
            queue.enqueueNDRangeKernel(boundaryQxTime, cl::NullRange,
                    numBC.numBcQxTime, cl::NullRange);
        }

        // Time series y-direction flow rate boundaries
        if (numBC.numBcQyTime) {
            boundaryQyTime.setArg(3, time);
            queue.enqueueNDRangeKernel(boundaryQyTime, cl::NullRange,
                    numBC.numBcQyTime, cl::NullRange);
        }

        // Free water level boundaries
        if (numBC.numBcWlFree) {
            queue.enqueueNDRangeKernel(boundaryWlFree, cl::NullRange,
                    numBC.numBcWlFree, cl::NullRange);
        }

        // Free flow rate boundaries
        if (numBC.numBcQxyFree) {
            queue.enqueueNDRangeKernel(boundaryQxyFree, cl::NullRange,
                    numBC.numBcQxyFree, cl::NullRange);
        }

        // Open boundaries
        if (numBC.numBcOpen) {
            queue.enqueueNDRangeKernel(boundaryOpen, cl::NullRange,
                    numBC.numBcOpen, cl::NullRange);
        }
    } catch(cl::Error &e) {
        utils::clError(e, __FUNCTION__);
    }
}

void OpenCl::hydroX(const NumBC &numBC, const float time,
        const SOLUTION_SCHEME solutionScheme)
{
    try {
        // ----------------------------
        // Boundary conditions
        // ----------------------------
        boundaryConditions(numBC, time);

        // ----------------------------
        // MacCormack predictor stage
        // ----------------------------

        // Check which cells are active
        queue.enqueueNDRangeKernel(macPredDrying, cl::NullRange, globalWorkSz,
                        localWorkSz);

        // Reset velocities at wet / dry interface
        macWetDryInterface.setArg(2, 1);
        macWetDryInterface.setArg(3, qxl_d);
        queue.enqueueNDRangeKernel(macWetDryInterface, cl::NullRange,
                globalWorkSz, localWorkSz);

        // Main predictor calculations
        queue.enqueueNDRangeKernel(macPredX, cl::NullRange, globalWorkSz,
                                localWorkSz);

        // ----------------------------
        // MacCormack corrector stage
        // ----------------------------

        // Check which cells are active
        queue.enqueueNDRangeKernel(macCorrDrying, cl::NullRange, globalWorkSz,
                        localWorkSz);

        // Reset velocities at wet / dry interface
        macWetDryInterface.setArg(3, qxm_d);
        queue.enqueueNDRangeKernel(macWetDryInterface, cl::NullRange,
                globalWorkSz, localWorkSz);

        // Main corrector calculations
        queue.enqueueNDRangeKernel(macCorrX, cl::NullRange, globalWorkSz,
                        localWorkSz);

        // ----------------------------
        // TVD steps
        // ----------------------------
        if (solutionScheme == TvdMacCormack) {
            // Main calculation
            queue.enqueueNDRangeKernel(tvdX, cl::NullRange, globalWorkSz,
                            localWorkSz);

            // Update variables
            tvdPost.setArg(8, 1);
            queue.enqueueNDRangeKernel(tvdPost, cl::NullRange, globalWorkSz,
                            localWorkSz);
        }
    } catch(const cl::Error &e) {
        utils::clError(e, __FUNCTION__);
    }
}

void OpenCl::hydroY(const NumBC &numBC, const float time,
        const SOLUTION_SCHEME solutionScheme)
{
    try {
        // ----------------------------
        // Boundary conditions
        // ----------------------------
        boundaryConditions(numBC, time);

        // ----------------------------
        // MacCormack predictor stage
        // ----------------------------

        // Check which cells are active
        queue.enqueueNDRangeKernel(macPredDrying, cl::NullRange, globalWorkSz,
                        localWorkSz);

        // Reset velocities at wet / dry interface
        macWetDryInterface.setArg(2, sizeof(cl_uint), const_cast<size_t*>(&globalWorkSz[0]));
        macWetDryInterface.setArg(3, qyl_d);
        queue.enqueueNDRangeKernel(macWetDryInterface, cl::NullRange,
                globalWorkSz, localWorkSz);

        // Main predictor calculations
        queue.enqueueNDRangeKernel(macPredY, cl::NullRange, globalWorkSz,
                                localWorkSz);

        // ----------------------------
        // MacCormack corrector stage
        // ----------------------------

        // Check which cells are active
        queue.enqueueNDRangeKernel(macCorrDrying, cl::NullRange, globalWorkSz,
                        localWorkSz);

        // Reset velocities at wet / dry interface
        macWetDryInterface.setArg(3, qym_d);
        queue.enqueueNDRangeKernel(macWetDryInterface, cl::NullRange,
                globalWorkSz, localWorkSz);

        // Main corrector calculations
        queue.enqueueNDRangeKernel(macCorrY, cl::NullRange, globalWorkSz,
                        localWorkSz);

        // ----------------------------
        // TVD steps
        // ----------------------------
        if (solutionScheme == TvdMacCormack) {
            // Main calculation
            queue.enqueueNDRangeKernel(tvdY, cl::NullRange, globalWorkSz,
                            localWorkSz);

            // Update variables
            tvdPost.setArg(8, sizeof(cl_uint), const_cast<size_t*>(&globalWorkSz[0]));
            queue.enqueueNDRangeKernel(tvdPost, cl::NullRange, globalWorkSz,
                            localWorkSz);
        }
    } catch(const cl::Error &e) {
        utils::clError(e, __FUNCTION__);
    }
}

void OpenCl::postHydro()
{
    try {
        // Determine active cells and update max water elevation and velocity
        queue.enqueueNDRangeKernel(activeCellsMaxVals, cl::NullRange,
                globalWorkSz, localWorkSz);

        // Determine bed shear stress
        queue.enqueueNDRangeKernel(bedShearStress, cl::NullRange,
                globalWorkSz, localWorkSz);
    } catch(cl::Error &e) {
        utils::clError(e, __FUNCTION__);
    }
}

void OpenCl::getDomainData(std::vector<cl_float> &etu,
        std::vector<cl_float> &qxu,
        std::vector<cl_float> &qyu,
        std::vector<cl_float> &taux,
        std::vector<cl_float> &tauy)
{
    try {
        queue.enqueueReadBuffer(etu_d, CL_FALSE, 0, domainClFloatSz, &etu[0]);
        queue.enqueueReadBuffer(qxu_d, CL_FALSE, 0, domainClFloatSz, &qxu[0]);
        queue.enqueueReadBuffer(qyu_d, CL_FALSE, 0, domainClFloatSz, &qyu[0]);
        queue.enqueueReadBuffer(taux_d, CL_FALSE, 0, domainClFloatSz, &taux[0]);
        queue.enqueueReadBuffer(tauy_d, CL_TRUE, 0, domainClFloatSz, &tauy[0]);
    } catch(cl::Error &e) {
        utils::clError(e, __FUNCTION__);
    }
}

void OpenCl::getMaxValues(std::vector<cl_float> &etmax,
        std::vector<cl_float> &uvmax,
        std::vector<cl_float> &taumax)
{
    try {
        queue.enqueueReadBuffer(etmax_d, CL_FALSE, 0, domainClFloatSz, &etmax[0]);
        queue.enqueueReadBuffer(uvmax_d, CL_FALSE, 0, domainClFloatSz, &uvmax[0]);
        queue.enqueueReadBuffer(taumax_d, CL_TRUE, 0, domainClFloatSz, &taumax[0]);
    } catch(cl::Error &e) {
        utils::clError(e, __FUNCTION__);
    }
}

void OpenCl::finish()
{
    try {
        queue.finish();
    } catch(cl::Error &e) {
        utils::clError(e, __FUNCTION__);
    }
}

// Anonymous namespace
namespace
{
std::string formatProfile(const std::string profile)
{
    std::string formatted_profile = "Unknown";
    if (profile == "FULL_PROFILE") {
        formatted_profile = "Full";
    } else if (profile == "EMBEDDED_PROFILE") {
        formatted_profile = "Embedded";
    }
    return formatted_profile;
}

std::string formatDeviceType(const cl_device_type type_enum)
{
    std::string type = "Unknown";
    if (type_enum == CL_DEVICE_TYPE_DEFAULT) {
        type = "Default";
    } else if (type_enum == CL_DEVICE_TYPE_CPU) {
        type = "CPU";
    } else if (type_enum == CL_DEVICE_TYPE_GPU) {
        type = "GPU";
    } else if (type_enum == CL_DEVICE_TYPE_ACCELERATOR) {
        type = "Accelerator";
    }
    return type;
}

std::string formatFreq(const cl_uint freq)
{
    char formatted_freq[64] = "Unkown clock frequency";
    if (freq > 0 && freq < 1000) {
        sprintf(formatted_freq, "%u MHz", freq);
    } else if (freq >= 1000 && freq < 1000*1000) {
        sprintf(formatted_freq, "%.3g GHz", freq / 1000.);
    }
    return formatted_freq;
}

std::string formatMemSize(const cl_ulong mem_size)
{
    char formatted_mem_size[64] = "Unkown memory size";
    if (mem_size > 0 && mem_size < 1024) {
        sprintf(formatted_mem_size, "%lu B", mem_size);
    } else if (mem_size >= 1024 && mem_size < 1024*1024) {
        sprintf(formatted_mem_size, "%.3g kB", mem_size / 1024.);
    } else if (mem_size >= 1024*1024 && mem_size < 1024*1024*1024) {
        sprintf(formatted_mem_size, "%.3g MB", mem_size / (1024.*1024.));
    } else if (mem_size >= 1024*1024*1024) {
        sprintf(formatted_mem_size, "%.3g GB", mem_size / (1024.*1024.*1024.));
    }
    return formatted_mem_size;
}
}   // End anonymous namespace
