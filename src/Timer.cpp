// Class header
#include "Timer.h"

// System headers
#include <chrono>
#include <iostream>
#include <string>

using namespace std::chrono;

Timer::Timer()
{
    // Set all timers to zero
    zero = high_resolution_clock::time_point(duration<long int, std::micro>(0));
    stopTime = zero;
    pauseTime = zero;
    resumeTime = zero;
    elapsedTime = duration<long int, std::micro>(0);
}

void Timer::start()
{
    startTime = high_resolution_clock::now();

    // Reset
    stopTime = zero;
    pauseTime = zero;
    resumeTime = zero;
    elapsedTime = duration<long int, std::micro>(0);
}

void Timer::stop()
{
    stopTime = high_resolution_clock::now();

    if (resumeTime != zero) {
        elapsedTime += duration_cast<microseconds>(stopTime - resumeTime);
    } else if (startTime != zero && pauseTime == zero) {
        elapsedTime = duration_cast<microseconds>(stopTime - startTime);
    }

    // Reset
    startTime = zero;
    pauseTime = zero;
    resumeTime = zero;
}

void Timer::pause()
{
    if (resumeTime != zero) {
        pauseTime = high_resolution_clock::now();
        elapsedTime += duration_cast<microseconds>(pauseTime - resumeTime);
    } else if (startTime != zero && pauseTime == zero) {
        pauseTime = high_resolution_clock::now();
        elapsedTime += duration_cast<microseconds>(pauseTime - startTime);
    }

    // Reset
    resumeTime = zero;
}

void Timer::resume()
{
    if (pauseTime != zero && resumeTime == zero) {
        resumeTime = high_resolution_clock::now();
    }

    // Reset
    pauseTime = zero;
}

long unsigned int Timer::getDurationMicroseconds()
{
    const auto now = high_resolution_clock::now();
    auto elapsed_time = duration_cast<microseconds>(elapsedTime);

    if (startTime != zero && stopTime == zero && !elapsedTime.count()) {
        elapsed_time = duration_cast<microseconds>(now - startTime);
    } else if (resumeTime != zero) {
        elapsed_time += duration_cast<microseconds>(now - resumeTime);
    }

    return elapsed_time.count();
}

long unsigned int Timer::getDurationMilliseconds()
{
    const auto now = high_resolution_clock::now();
    auto elapsed_time = duration_cast<milliseconds>(elapsedTime);

    if (startTime != zero && stopTime == zero && !elapsedTime.count()) {
        elapsed_time = duration_cast<milliseconds>(now - startTime);
    } else if (resumeTime != zero) {
        elapsed_time += duration_cast<milliseconds>(now - resumeTime);
    }

    return elapsed_time.count();
}

unsigned int Timer::getDurationSeconds()
{
    const auto now = high_resolution_clock::now();
    auto elapsed_time = duration_cast<seconds>(elapsedTime);

    if (startTime != zero && stopTime == zero && !elapsedTime.count()) {
        elapsed_time = duration_cast<seconds>(now - startTime);
    } else if (resumeTime != zero) {
        elapsed_time += duration_cast<seconds>(now - resumeTime);
    }

    return elapsed_time.count();
}

unsigned int Timer::getDurationMinutes()
{
    const auto now = high_resolution_clock::now();
    auto elapsed_time = duration_cast<minutes>(elapsedTime);

    if (startTime != zero && stopTime == zero && !elapsedTime.count()) {
        elapsed_time = duration_cast<minutes>(now - startTime);
    } else if (resumeTime != zero) {
        elapsed_time += duration_cast<minutes>(now - resumeTime);
    }

    return elapsed_time.count();
}

unsigned int Timer::getDurationHours()
{
    const auto now = high_resolution_clock::now();
    auto elapsed_time = duration_cast<hours>(elapsedTime);

    if (startTime != zero && stopTime == zero && !elapsedTime.count()) {
        elapsed_time = duration_cast<hours>(now - startTime);
    } else if (resumeTime != zero) {
        elapsed_time += duration_cast<hours>(now - resumeTime);
    }

    return elapsed_time.count();
}

std::string Timer::getDurationFormatted()
{
    const long unsigned int elapsed_usecs = elapsedTime.count();

    const unsigned int sec = 1000*1000;
    const unsigned int min = 60*sec;
    const long unsigned int hr = (long unsigned int)60*min;

    const unsigned int hours = elapsed_usecs / hr;
    const unsigned int mins = (elapsed_usecs - hr*hours) / min;
    const unsigned int secs = (elapsed_usecs - hr*hours - min*mins) / sec;
    const unsigned int msecs = (elapsed_usecs - hr*hours - min*mins - sec*secs)
            / 1000;
    const unsigned int usecs = (elapsed_usecs - hr*hours - min*mins - sec*secs
            - 1000*msecs);

    char str[32];
    sprintf(str, "%.2u:%.2u:%.2u.%.3u.%.3u", hours, mins, secs, msecs, usecs);
    return str;
}

void Timer::printDurationFormatted(std::string msg)
{
    if (!msg.empty()) {
        msg = ", " + msg;
    }
    std::cout << "Elapsed time (h:m:s.ms.us)" << msg << ": "
            << getDurationFormatted() << "\n";
}
