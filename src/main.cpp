#define __CL_ENABLE_EXCEPTIONS

// System headers
#include <cmath>
#include <vector>

// Third-party headers
#include <CL/cl.hpp>

// Project headers
#include "FileIO.h"
#include "OpenCl.h"
#include "Tests.h"
#include "Timer.h"
#include "types.h"
#include "utils.h"

int main(int argc, char* argv[])
{
    // Begin timing main program
    Timer timer_prog;
    timer_prog.start();


    // -----------------------------------------------------------------------
    // Parse command line args
    // -----------------------------------------------------------------------
    std::string proj_file;
    if (utils::cmdLineArgExists(argc, argv, "-t")) {
        unsigned int n_rows = 123;
        unsigned int n_cols = 321;
        if (utils::cmdLineArgExists(argc, argv, "-y")) {
            n_rows = atoi(utils::getCmdLineArg(argc, argv, "-y"));
        }
        if (utils::cmdLineArgExists(argc, argv, "-x")) {
            n_cols = atoi(utils::getCmdLineArg(argc, argv, "-x"));
        }

        utils::printHeader("Running tests (x = " + std::to_string(n_cols) +
                ", y = " + std::to_string(n_rows) + ")");
        Tests tests(CL_DEVICE_TYPE_GPU, 4, 0, n_rows, n_cols);
        tests.run();

        return 0;
    } else if (argc == 2) {
        proj_file = argv[1];
    } else {
        proj_file = "inp/project.cfg";
    }


    // -----------------------------------------------------------------------
    // Read in project file
    // -----------------------------------------------------------------------
    FileIO file_io(proj_file);
    file_io.printDetails();


    // -----------------------------------------------------------------------
    // Get hardware details
    // -----------------------------------------------------------------------
    cl_device_type device_type;
    cl_uint num_cores;
    cl_uint platform_idx;

    file_io.readHardware(device_type, num_cores, platform_idx);


    // -----------------------------------------------------------------------
    // Get simulation control parameters
    // -----------------------------------------------------------------------
    Constants constants;
    SOLUTION_SCHEME solution_scheme;
    bool alternate_direction;
    unsigned int num_rows;
    unsigned int num_cols;
    float time_sim;
    float dt_out;
    float dt_min;
    float dt_max;
    float beta;
    float cori;
    float courant_factor;

    file_io.readParams(constants, solution_scheme, alternate_direction,
            time_sim, dt_out, dt_min, dt_max, num_rows, num_cols, beta, cori,
            courant_factor);

    // Derived values
    const unsigned int num_elements = num_rows * num_cols;
    const unsigned int total_out = floor(time_sim / dt_out) + 1;

    // Check if time step is adaptive (to nearest millisecond)
    float dt = dt_max;
    bool dt_constant = false;
    if (fabs(dt_max - dt_min) < 1e-3f) {
        dt_constant = true;
    }

    // Initialize OpenCL and transfer data to device
    OpenCl opencl(device_type, num_cores, platform_idx, num_rows, num_cols,
            num_elements);
    opencl.initConstants(constants);


    // -----------------------------------------------------------------------
    // Get boundary conditions
    // -----------------------------------------------------------------------
    NumBC num_bc;
    std::vector<cl_int> bc_open;
    std::vector<cl_uint> bc_wl_time;
    std::vector<cl_int> bc_wl_free;
    std::vector<cl_uint> bc_qx_time;
    std::vector<cl_uint> bc_qy_time;
    std::vector<cl_int> bc_qxy_free;
    std::vector<cl_float> bc_ts_times;
    std::vector<cl_float> bc_ts_values;

    file_io.readBoundaries(bc_open, bc_wl_time, bc_wl_free, bc_qx_time,
            bc_qy_time, bc_qxy_free, bc_ts_times, bc_ts_values, num_bc);

    // Transfer data to device
    opencl.initBoundaries(bc_open, bc_wl_time, bc_wl_free, bc_qx_time,
            bc_qy_time, bc_qxy_free, bc_ts_times, bc_ts_values);


    // -----------------------------------------------------------------------
    // Get monitoring points
    // -----------------------------------------------------------------------
    float dt_monit;
    std::vector<cl_uint2> points;

    file_io.readMonitPoints(dt_monit, points);


    // -----------------------------------------------------------------------
    // Get vegetation data
    // -----------------------------------------------------------------------
    std::vector<std::string> veg_types;
    std::vector<cl_float> veg_data;
    std::vector<cl_float> veg_heights;
    std::vector<cl_float> veg_widths;
    std::vector<cl_float> veg_areas;

    file_io.readVegetation(veg_types, veg_data, veg_heights, veg_widths,
            veg_areas);

    // Transfer data to device
    opencl.initVegetation(veg_data, veg_heights, veg_widths, veg_areas);


    // -----------------------------------------------------------------------
    // Get initial values
    // -----------------------------------------------------------------------
    std::vector<cl_float> H(num_elements, 0);
    std::vector<cl_float> etu(num_elements, 0);
    std::vector<cl_float> qxu(num_elements, 0);
    std::vector<cl_float> qyu(num_elements, 0);
    std::vector<cl_float> VARCHZ(num_elements, 0);
    std::vector<cl_uint> IWET(num_elements, 0);
    std::vector<cl_uint> IALL(num_elements, false); // vector<bool> is broken
    std::vector<cl_uint> iact(num_elements, false); // vector<bool> is broken
    std::vector<cl_float> etmax(num_elements, 0);
    std::vector<cl_float> uvmax(num_elements, 0);

    file_io.readInitialValues(H, etu, qxu, qyu, VARCHZ, IWET, IALL, iact, etmax,
            uvmax, veg_types.size());

    // Transfer data to device
    opencl.initInitialValues(H, etu, qxu, qyu, VARCHZ, IWET, IALL, iact, etmax,
            uvmax);


    // -----------------------------------------------------------------------
    // Initialize bed shear stress arrays
    // -----------------------------------------------------------------------
    std::vector<cl_float> taux(num_elements, 0);
    std::vector<cl_float> tauy(num_elements, 0);
    std::vector<cl_float> taumax(num_elements, 0);

    opencl.initBedShearStress();


    // -----------------------------------------------------------------------
    // Initialize remaining temporary hydrodynamic arrays
    // -----------------------------------------------------------------------
    opencl.initTempHydro(constants, dt_constant, dt, beta, cori);


    // -----------------------------------------------------------------------
    // Build OpenCL kernels and set kernel args
    // -----------------------------------------------------------------------
    opencl.buildKernels();
    opencl.setKernelArgs();


    // -----------------------------------------------------------------------
    // Main loop
    // -----------------------------------------------------------------------

    // Initialize time stepping variables
    unsigned int num_time_step = 0;
    float time = 0;
    float time_out = 0;
    float time_monit = 0;
    bool y_first = false;

    // Begin timing calculations
    Timer timer_calcs;
    timer_calcs.start();

    // Run simulation until desired time elapsed
    utils::printHeader("Modelling commenced");
    utils::printProgressHeader();
    while (time < time_sim) {
        // Determine time step and carry out wetting / drying check
        opencl.preHydro(constants, IWET, iact, etu, dt_constant, dt_min, dt_max,
                beta, cori, courant_factor, dt);

        // Update current time and step number
        time += dt;
        num_time_step++;

        // Alternate order of X and Y direction
        if (alternate_direction) {
            y_first = !y_first;
        }

        // Perform main hydrodynamic calculations
        if (y_first) {
            opencl.hydroY(num_bc, time, solution_scheme);
            opencl.hydroX(num_bc, time, solution_scheme);
        } else {
            opencl.hydroX(num_bc, time, solution_scheme);
            opencl.hydroY(num_bc, time, solution_scheme);
        }

        // Update max values and calculate bed shear stress
        opencl.postHydro();

        // Output domain data
        if (time >= time_out) {
            timer_calcs.pause();
            opencl.getDomainData(etu, qxu, qyu, taux, tauy);
            file_io.writeDomainData(H, etu, qxu, qyu, taux, tauy, IWET,
                    num_rows, num_cols, constants.dx, constants.dpMin, time,
                    num_time_step, timer_prog.getDurationMilliseconds(), dt_out,
                    time_out);
            utils::printProgress(total_out, num_time_step, time);
            timer_calcs.resume();
        }

        // Output monitoring point data
        if (points.size() && time >= time_monit) {
            timer_calcs.pause();
            opencl.getDomainData(etu, qxu, qyu, taux, tauy);
            file_io.writeMonitData(H, etu, qxu, qyu, taux, tauy, points,
                    num_cols, time, dt_monit, time_monit);
            timer_calcs.resume();
        }
    }


    // -----------------------------------------------------------------------
    // Post processing
    // -----------------------------------------------------------------------

    // Stop timing calculations
    opencl.finish();
    timer_calcs.stop();

    // Output max water elevation, velocity and bed shear stress data
    opencl.getMaxValues(etmax, uvmax, taumax);
    file_io.writeMaxValues(H, etmax, uvmax, taumax, IWET, num_rows, num_cols,
            constants.dx, constants.dpMin);

    // Display program duration
    timer_prog.stop();
    utils::printHeader("Modelling complete!");
    timer_prog.printDurationFormatted("total");
    timer_calcs.printDurationFormatted("calcs");

    return 0;
}
