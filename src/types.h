#pragma once

typedef enum {MacCormack, TvdMacCormack} SOLUTION_SCHEME;
typedef enum {Constant, Manning, CwTrans, CwRough} ROUGHNESS_SCHEME;
typedef enum {None, Parabolic, MixingLength} EDDY_SCHEME;

// Number of each boundary condition
typedef struct NumBC {
    unsigned int numBcOpen;
    unsigned int numBcWlTime;
    unsigned int numBcWlFree;
    unsigned int numBcQxTime;
    unsigned int numBcQyTime;
    unsigned int numBcQxyFree;
} NumBC;

// Device variables that stay constant
typedef struct Constants {
    ROUGHNESS_SCHEME roughnessScheme;
    EDDY_SCHEME eddyScheme;
    float dx;
    float dpMin;
    float eddyCoef;
} Constants;

// Device variables that might get updated each time-step (depends on dt_const)
typedef struct Vars {
    float dt;
    float dtDx;
    float dtDxSq;
    float hDtDxG;
    float dtDxBeta;
    float dtG;
    float dtCori;
} Vars;
