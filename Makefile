# Project name
PROJECT = divast-tvd-opencl

# Main directories
DIR_BIN = bin
DIR_MOD = mod
DIR_OBJ = obj
DIR_OUT = out
DIR_CPP = src
DIR_F03 = src/fortran

# Directories to create if not already existant
MKDIR := $(DIR_BIN) $(DIR_MOD) $(DIR_OBJ) $(DIR_OUT)

# Binary file
BIN := $(DIR_BIN)/$(PROJECT)

# C++ source files
SRC_CPP := $(wildcard $(DIR_CPP)/*.cpp)
OBJ_CPP := $(patsubst %.cpp,%.o,$(subst $(DIR_CPP),$(DIR_OBJ),$(SRC_CPP)))

# Fortran source files
SRC_F03 := $(wildcard $(DIR_F03)/*.f03)
OBJ_F03 := $(patsubst %.f03,%.o,$(subst $(DIR_F03),$(DIR_OBJ),$(SRC_F03)))

# Dependency files
DEP_CPP = .depend_cpp
DEP_F03 = .depend_f03

# Compilers
FC = gfortran

# Compiler options
FFLAGS := -c -std=f2003 -J $(DIR_MOD) -O3 -march=native -mtune=native -pedantic-errors -Wall -Wextra -Werror
CXXFLAGS = -c -std=c++11 -O3 -march=native -mtune=native -Wall -Werror -Wno-comment
INCLUDES = -I/opt/AMDAPP/include

# Linker options
LDFLAGS = #-Wl,--verbose
LDPATHS = -L/opt/AMDAPP/lib/x86_64
LDLIBS =  -lconfig++ -lOpenCL -lgfortran

# Non-file targets
.PHONY: all debug clean

# Default target
all: $(MKDIR) $(BIN)

# Debug target
debug: FFLAGS := -g $(filter-out -O%, $(FFLAGS))
debug: CXXFLAGS := -g $(filter-out -O%, $(CXXFLAGS))
debug: all

# Create directories
$(MKDIR):
	mkdir -p $@

# Create binary
$(BIN): $(OBJ_CPP) $(OBJ_F03)
	@$(RM) $(BIN)
	$(CXX) $(LDFLAGS) $^ $(LDPATHS) $(LDLIBS) -o $@
	@echo "\n**** Compiled on: "`date`" ****"

# Determine C++ dependices
$(DEP_CPP): $(SRC_CPP)
	@$(CXX) -xc++ $(CXXFLAGS) -MM $^ > $(DEP_CPP)
	@sed -i 's|.*:|$(DIR_OBJ)/&|' $(DEP_CPP)

-include $(DEP_CPP)

# Determine Fortran dependices
$(DEP_F03): $(SRC_F03)
	@makedepf90 $^ > $(DEP_F03)
	@sed -i 's|$(DIR_F03)/\([^ ]*\.o\)|$(DIR_OBJ)/\1|g' $(DEP_F03)

-include $(DEP_F03)

# Compile C++
$(DIR_OBJ)/%.o: $(DIR_CPP)/%.cpp
	$(CXX) $(CXXFLAGS) $(INCLUDES) $< -o $@

# Compile Fortran
$(DIR_OBJ)/%.o: $(DIR_F03)/%.f03
	$(FC) $(FFLAGS) $< -o $@

# Clean project
clean:
	$(RM) $(BIN)
	$(RM) $(DEP_CPP)
	$(RM) $(DEP_F03)
	$(RM) $(OBJ_CPP)
	$(RM) $(OBJ_F03)
